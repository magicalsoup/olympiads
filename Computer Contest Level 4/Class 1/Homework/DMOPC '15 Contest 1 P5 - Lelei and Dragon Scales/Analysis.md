# Analysis

## Subtask 1

I believe its possible to brute-force, and find all the possible squares and check the biggest sum out of all of them.

**Time Complexity:** $`O(W^3 \times H^3 )`$

## Subtask 2 & 3
In order to cut down some of the time spent summing up the squares, we can now use a 2D prefix sum array, which turns our query time in to $`O(1)`$, and the
we can just find all the possible squares.

**Time Complexity:** $`O(W^2 \times H^2)`$

## Subtask 4
For this subtask, we need an insight that there are no negative cells. Meaning that we don't have to check all the ares upto $`N`$, instead, we only check the squares
which have an area of $`N`$, cause more numbers equals higher sum, as there are no negatives. Then we can just loop to the $`min(H, W)`$, and just find the width by
dividing the area by the length, which loops to $`min(H, W)`$. Then, we can also just swap the width and length, and check the other possiblity.

**Time Complexity:** $`O(W \times H \times min(H, W)`$