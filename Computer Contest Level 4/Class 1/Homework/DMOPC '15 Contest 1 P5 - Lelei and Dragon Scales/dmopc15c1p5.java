import java.io.*;
import java.util.StringTokenizer;
public class dmopc15c1p5 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException {
        int W = readInt(), H = readInt(), N = readInt();
        int psa[][] = new int[H+1][W+1];
        for(int i=1; i<=H; i++)
            for(int j=1; j<=W; j++)
                psa[i][j] = readInt();

        for(int i=1; i<=H; i++)
            for(int j=1; j<=W; j++)
                psa[i][j] = psa[i-1][j] + psa[i][j-1] - psa[i-1][j-1] + psa[i][j];

        int max=0;
        for(int i=1; i<=H; i++) {
            for(int j=1; j<=W; j++) {
                for(int length=1; length<=Math.min(W, H); length++) {
                    int width = N / length;
                    if (width * length <= N && j + width-1 <= W && i+length-1 <= H) {
                        int r2 = length + i-1;
                        int c2 = width + j-1;
                        int sum = getSum(psa, i, j, r2, c2);
                        max = Math.max(sum, max);
                    }
                    if(width * length <= N && j+length-1<=W && i+width-1<=H) {
                        int r2 = width+i-1;
                        int c2 = length+j-1;
                        int sum = getSum(psa, i, j, r2, c2);
                        max = Math.max(sum, max);
                    }
                }
            }
        }
        System.out.println(max);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static int getSum(int psa[][], int r1, int c1, int r2, int c2) {
        return psa[r2][c2] - psa[r1-1][c2] - psa[r2][c1-1] + psa[r1-1][c1-1];
    }
    static void print_psa(int H, int W, int psa[][]) {
        for(int i=1; i<=H; i++) {
            for(int j=1; j<=W; j++) {
                System.out.print(psa[i][j] + " ");
            }
            System.out.println();
        }
    }
}