import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
public class dmpg15s5 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt(), M = readInt();
        short dif[][] = new short[N+10][N+10];
        for(int i=0; i<M; i++) {
            int x = readInt()+1, y = readInt()+1, w = readInt(), h = readInt();
            dif[x][y]++;
            dif[x+w][y]--;
            dif[x][y+h]--;
            dif[x+w][y+h]++;
        }
        int cnt=0;
        for(int i=1; i<=N; i++) {
            for(int j=1; j<=N; j++) {
                dif[i][j] += dif[i-1][j] + dif[i][j-1] - dif[i-1][j-1];
                if ((dif[i][j]&1)==1) cnt++;
            }
        }
        System.out.println(cnt);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException{
        return Integer.parseInt(next());
    }
}
