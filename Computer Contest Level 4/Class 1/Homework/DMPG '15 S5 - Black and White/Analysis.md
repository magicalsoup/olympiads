# Analysis

## Subtask 1

For the first subtask, we can brute-force our way through, simply by answering each query by going and filling the 2D grid. Each query will take $`O(N^2)`$ time to answer
and there are $`M`$ queries.

**Time Complexity:** $`O(N^2 \times M)`$


## Subtask 2 & 3
This problem can be solved using a 2D difference array. We can just update $`1`$s for the array, 0 representing white and 1 representing black. Since difference array only takes
$`O(1)`$ time to update, and $`O(N^2)`$ to build, and there are $`M`$ queries, our time complexity is $`O(N^2 + M)`$.

**Note:** Another small insight, if we look at the memory limit, simply using an `int` array will cost too much memory. We need to either use a `short` or boolean` array to help
reduce our memory.

**Time Complexity:** $`O(N^2 + M)`$

