import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
public class oly19practice25 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt(), M = readInt(), K = readInt();
        long dif[][] = new long[M+2][N+2];
        for(int i=0 ;i<K; i++){
            int x = readInt(), y = readInt(), x1 = readInt(), y1 = readInt();
            dif[y][x]+=1;
            dif[y1+1][x1+1]+=1;
            dif[y1+1][x]-=1;
            dif[y][x1+1]-=1;
        }
        for(int k=0; k<2; k++) {
            for (int i = 1; i <= M+1; i++) {
                for (int j = 1; j <= N; j++) {
                    dif[i][j] = dif[i - 1][j] + dif[i][j - 1] + dif[i][j] - dif[i - 1][j - 1];
                }
            }
        }
        int Q = readInt();
        for(int i=0; i<Q; i++) {
            int c1 = readInt(), r1 = readInt(), c2 = readInt(), r2 = readInt();
            long ans = dif[r2][c2] - dif[r1-1][c2] - dif[r2][c1-1] + dif[r1-1][c1-1];
            pw.println(ans);
        }
        pw.close();
    }
    static String next () throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}