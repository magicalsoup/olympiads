# Analysis

This problem problem is just an simple implementation problem. We can first precompute how much damage Fax takes each second, by simply adding $`X`$ in the range $`[T, T+L)`$.

Then for each second, we just find the armor that protects Fax the most and subtract the damage from Fax's total health bar. Then if his health drops to $`0`$, print `DO A BARREL ROLL!`,
else, just print Fax's Health

It takes $`O(N^2)`$  to precompute, or just $`O(N)`$ if we implement something similar to a difference array + prefix sum array, then it takes $`O(N^2)`$ to find the maximum health.

**Time Complexity:** $`O(N^2)`$