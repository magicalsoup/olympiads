import java.util.Scanner;
public class tle17c8p2 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        double H = sc.nextDouble();
        int D = sc.nextInt();
        int E = sc.nextInt();
        defense defenses[] = new defense[D];
        for(int i=0; i<D; i++) {
            double armor = sc.nextDouble(), shield = sc.nextDouble();
            defenses[i] = new defense(shield, armor);
        }
        double damage[] = new double[4*510];
        int longest=0;
        for(int i=0; i<E; i++) {
            int T = sc.nextInt(),  L = sc.nextInt();
            double X = sc.nextDouble();
            for(int j=T; j<T+L; j++)
                damage[j] += X;
            longest = Math.max(longest, T+L);
        }

        for(int i=0; i<=longest; i++) {
            double totdamage=damage[i];
            for(defense def : defenses) {
                double percentage = (100.0 - def.armor) / 100.00;
                double leftoverdamage = (damage[i] - def.shield);
                totdamage = Math.min(totdamage, Math.max(0.0, leftoverdamage * percentage));
            }
            H -= totdamage;
        }
        if(H <= 0) System.out.println("DO A BARREL ROLL!");
        else System.out.printf("%.2f\n", H);
        sc.close();
    }
    static class defense {
        double shield, armor;
        public defense(double shield, double armor) {
            this.shield=shield;
            this.armor=armor;
        }
    }
}
