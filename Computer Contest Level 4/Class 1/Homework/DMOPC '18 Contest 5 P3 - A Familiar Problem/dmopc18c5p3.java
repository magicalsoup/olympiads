import java.util.Scanner;
public class dmopc18c5p3 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        long M = sc.nextLong();
        long psa[] = new long[N+1];
        for(int i=1; i<=N; i++) {
            psa[i] = sc.nextLong();
            psa[i] += psa[i-1];
        }
        int l=1, r=1;
        int max=0;
        while(l <= N && r <= N) {
            if(psa[r] - psa[l-1] < M)
                r++;
            else if(psa[r]-psa[l-1] >= M)
                l++;
            max = Math.max(max, r-l);
        }
        System.out.println(max);
        sc.close();
    }
}
