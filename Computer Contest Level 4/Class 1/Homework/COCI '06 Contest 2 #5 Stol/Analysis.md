# Analysis

This problem might seem very hard, but is actually really simple. Lets assume the apartement is reduced to just 2 columns, now what is the answer for this? 
We can first do a horizontal 2D prefix sum array, only horizontal, so we keep track of how many blocked cells each column share, this way we can get the 
number of blcoked column in that row in $`O(1)`$ time, by calling `psa[row][column2] - psa[row][column1]`. If the previous expression is equal to 0,
then that means we can place a table horizontally in those 2 columns. Then we can go down through the rows, and until we can't go do anymore. In essence, we are 
searching for all possibilties of rows. This takes about $`O(N^2)`$, and looping through the whole thing once is $`O(N)`$.

**Time Complexity:** $`O(C^2 \times R)`$