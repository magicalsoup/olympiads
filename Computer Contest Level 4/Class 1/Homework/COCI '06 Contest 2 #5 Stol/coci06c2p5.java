import java.util.Scanner;
public class coci06c2p5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int R = sc.nextInt(), C = sc.nextInt();
        int g[][] = new int[R+1][C+1];
        int psa[][] = new int[R+1][C+1];
        for(int i=1; i<=R; i++){
            String s = sc.next();
            for(int j=1; j<=C; j++) {
                g[i][j] = s.charAt(j-1);
            }
        }
        for(int i=1; i<=R; i++){
            for(int j=1; j<=C; j++){
                if(g[i][j] == 'X')
                    psa[i][j] += psa[i][j-1] + 1;
                else
                    psa[i][j] += psa[i][j-1];
            }
        }
        int ans=0;
        for(int c1=1; c1<=C; c1++) {
            for(int c2=c1; c2<=C; c2++) {
                int length=0, maxLen=0;
                for(int i=1; i<=R; i++) {
                    if(psa[i][c2] - psa[i][c1-1] == 0) {
                        length++;
                        maxLen = Math.max(maxLen, length);
                    }
                    else length=0;
                }
                int width=c2-c1+1;
                int perimeter = 2*(maxLen+width);
                if(maxLen > 0)
                    ans = Math.max(ans, perimeter);
            }
        }
        System.out.println(ans-1);
    }
}
