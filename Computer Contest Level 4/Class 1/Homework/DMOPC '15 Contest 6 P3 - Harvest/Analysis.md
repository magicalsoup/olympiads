# Analysis

## Subtask 1
For the first subtask, we can create an 2D array and subtract places that were raided. Then we can use a 2 pointer approach to find the minimum $`W`$

**Time Complexity:** $`O(N^2 \times M)`$

## Subtask 2 & 3
For the next 2 subtasks, lets use an difference array to keep track the number of potatoes in each row. Which makes our update time $`O(1)`$, then we turn the difference array into
a prefix sum array. Then we use a two-pointer approach to find the minimum $`W`$ needed.

**Time Complexity:** $`O(N + M)`$