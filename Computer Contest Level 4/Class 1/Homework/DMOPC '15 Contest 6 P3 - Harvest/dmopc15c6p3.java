import java.util.Scanner;
public class dmopc15c6p3 {
    public static void main(String[]args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(), m = sc.nextInt();
        long k = sc.nextLong();
        long dif[] = new long[n+2];
        dif[0]=0;
        for(int i = 1; i <= m; i++) {
            int a = sc.nextInt(), b = sc.nextInt();
            update(dif, a, b, -1);
        }
        long rows[] = new long[n+1];
        for(int i=1; i<=n; i++) {
            dif[i] += dif[i-1];
            rows[i] = dif[i] + m;
            rows[i] += rows[i-1];
        }
        long w = n+1; int l = 1, r = 1;
        while(l <= n && r <= n) {
            if(rows[r] - rows[l-1] >= k) {
                w = Math.min(w, r - l+1);
                l++;
            }
            else
                r++;
        }
        System.out.println(w == n+1? -1: w);
        sc.close();
    }
    static void update(long dif[], int l, int r, int x) {
        dif[l] += x;
        dif[r + 1] -= x;
    }
}