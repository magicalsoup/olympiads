# Difference Array & Prefix Sum Array

## Prefix Sum Array
- `Prefix Sum Array`: Given an array of numbers A, we replace each element with the sum of itself and all the elements preceding it.
` Example
- $`A`$ = `[3, 5, 4, 1, 2]`
- $`P(A)`$ = `[3, 8, 12, 13, 15]`
- **Time Complexity**: $`O(N)`$, Linear time
```cpp
void prefix_sum_array(int c, int* A, int n, int* P) {
    P[0] = c;
    for(int i=0; i<n; i++)
        P[i+1] = P[i] + A[i];
}
```

## 2D Prefix Sum Array
- PSA from 1D to 2D
- Query is to find a rectangle sum
    - E.g. $`Sum((2, 4), (5,7))`$, where $`(2,4)`$ is the top left corner of a sub-matrix.
- Can we still use prefix sum array?
    - Create new Array $`S`$.
    - For each row in $`A`$, create its' cumulative sum is $`S`$
    - In $`S`$, in-place, create cumulative sum for each column
    - Now $`S(i, j) = Sum((0, 0), (i, j))`$
    
- | 1 | 2 | 2 | 4 | 1 |
  |:--|:--|:--|:--|:--|
  |1|4|1|5|2|
  |2|3|3|2|4|
  |4|1|5|4|6|
  |6|3|2|1|3|

- Now we accumulate each row: $`\rightarrow`$
- | 1 | 2 | 2 | 4 | 1 |
  |:--|:--|:--|:--|:--|
  |1|5|6|11|13|
  |2|5|8|10|14|
  |4|5|10|14|20|
  |6|9|11|12|15|

- Then we accumulate each column: $`\downarrow`$
- | 1 | 2 | 2 | 4 | 1 |
  |:--|:--|:--|:--|:--|
  |1|5|6|11|13|
  |3|10|14|23|27|
  |7|15|24|37|47|
  |13|24|35|49|62|


- Another way to calculate prefix sum array
```java
  psa[i][j] = psa[i-1][j] + psa[i][j-1] - psa[i-1][j-1] + a[i][j];
```

### 2D Sum Query
- Sum of $`C`$ is the sum from top left corner to $`C`$             
- Lets remove sum of $`B`$
- Let's remove sum of $`D`$
- Sum of $`A`$ is removed twice, lets add $`A`$ back                
- $`S = C - B - D + A`$     
- A: `psa[r1-1][c1-1]`
- B: `psa[r1-1][c2]`
- C: `psa[r2][c2]`
- D: `psa[r2][c1-1]`
```
S = psa[r2][c2] - psa[r1-1][c2] - psa[r2][c1-1] + psa[r1-1][c1-1]
```

## 3D Prefix Sum Array
- Same idea to calculate `psa[i][j][k]`
    - Accumulate sum over i
    - Accumulate sum over j
    - Accumulate sum over k
- How abount 3D sum query
    - Sum of $`(i1, j1, k1)`$ to $`(i2, j2, k2)`$ (inclusive)
```    
= psa[i2][j2][k2] - psa[i1-1][j2][k2] - psa[i2][j1-1][k2] - psa[i2][j2][k1-1] 
+ psa[i1-1][j1-1][k2] + psa[i1-1][i2][k1-1] + psa[i2][j1-1][k1-1] - psa[i1-1][j1-1][k1-1]
```
- **Time Complexity:** $`O(2 \times N^2)`$

## Difference Array
- `Difference Array`: Given an array of numbers, we can construct a new array by replacing each element by the difference between itself and teh previous element, except for the first elemement, which we simply ignore.
- Example:
- $`A`$ = `[3, 5, 4, 1, 2]`
- $`D(A)`$ = `[5-3, 4-5, 1-4, 2-1] = [2, -1, -3, 1]`
- $`D(A)`$ = `[3], 2, -1, -3, 1]`

## 2D Prefix Sum Array
- To construct the 2d difference array, simply update everything, then run 2d prefix sum array, then just add the difference array with the original array.

### Updating A value
```java
dif[r1][c1] += val;
dif[r2+1][c1] -= val;
dif[r1][c2+1] -= val;
dif[r2+1][c2+1] += val;
```
- **Time Complexity:** $`O(1)`$


