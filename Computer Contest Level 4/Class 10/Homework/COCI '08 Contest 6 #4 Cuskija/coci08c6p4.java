import java.util.Scanner;
import java.util.LinkedList;
public class coci08c6p4 {
    public static void main(String[]args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        LinkedList<Integer> nums[] = new LinkedList[3];
        for(int i=0; i<3; i++) nums[i] = new LinkedList<>();
        int a[] = new int[N];
        for(int i=0; i<N; i++) {
            a[i] = sc.nextInt();
            nums[a[i] % 3].add(i);
        }
        if((nums[0].size() > nums[1].size() + nums[2].size()+1 )|| (nums[0].size() == 0 && nums[1].size() >= 1 && nums[2].size() >= 1)) {
            System.out.println("impossible");
            return;
        }
        while(!nums[1].isEmpty()) {
            if(nums[0].size() > nums[1].size()) {
                System.out.print(a[nums[0].pollFirst()] + " ");
                System.out.print(a[nums[1].pollLast()] + " ");
            }
            else System.out.print(a[nums[1].pollLast()] + " ");
        }
        while(!nums[2].isEmpty()) {
            if(!nums[0].isEmpty()) {
                System.out.print(a[nums[0].pollLast()] + " ");
                System.out.print(a[nums[2].pollFirst()] + " ");
            }
            else System.out.print(a[nums[2].pollLast()] + " ");
        }
        if(!nums[0].isEmpty()) System.out.print(a[nums[0].pollLast()]);
        System.out.println();
        sc.close();
    }
}
