# Analysis

For this problem, we only care about the number's modulus. After modding, each number will be either $`0, 1, 2`$. Two $`0`$ beside each other or a $`1`$ and $`2`$ together will
form a number that is divisble by three, so we need to separate them.

First we check if its possible to pair up those numbers. If we have $`2`$ or more zeroes than ones and twos, we cannot arrange a seqeunce where $`2`$ zeroes will not be beside each
other, so its impossible. Similarily, if we have no zeroes and we have at least one and at least a two, its not possible as well.

Once we know its possible, we can simply put a one or a two in between zeroes, and that will be our answer.

**Time Complexity:** $`O(N)`$