import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;
public class cco10p1 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int D = sc.nextInt(); // number of dogs in the neighbourhood
        int wait[] = new int[D+1]; // the ith dog waits for wait_i seconds before barking
        for(int i=1; i<=D; i++)
            wait[i] = sc.nextInt();
        ArrayList<ArrayList<Integer>> adj = new ArrayList<>();
        int F = sc.nextInt();
        for(int i=0; i<=F; i++) adj.add(new ArrayList<>());
        for(int i=0; i<F; i++) {
            int u = sc.nextInt(), v = sc.nextInt();
            adj.get(u).add(v);
        }
        int T = sc.nextInt();
        int bark[] = new int[T+1],  vis[] = new int[D+1];
        Arrays.fill(vis, -1);
        vis[1] = 1;
        for(int i=0; i<=T; i++) {
            for(int j=1; j<=D; j++)
                vis[j]--;
            for(int j=1; j<=D; j++) {
                if (vis[j] == 0) {
                    bark[j]++;
                    for (int e : adj.get(j)) {
                        if (vis[e] < 0) {
                            vis[e] = wait[e];
                        }
                    }
                }
            }
        }
        for(int i=1; i<=D; i++)
            System.out.println(bark[i]);
        sc.close();
    }
}
