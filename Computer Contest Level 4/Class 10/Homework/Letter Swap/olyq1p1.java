import java.util.Scanner;
import java.util.LinkedList;
public class olyq1p1 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        String a = sc.next(), b = sc.next();
        int A[] = new int[N+1], B[] = new int[N+1];
        long BIT[] = new long[N+1];
        LinkedList<LinkedList<Integer>> pos = new LinkedList<LinkedList<Integer>>();
        for(int i=0; i<26; i++) pos.add(new LinkedList<Integer>());
        for(int i=1; i<=N; i++) {
            A[i] = a.charAt(i-1) - 'A';
            B[i] = b.charAt(i-1) - 'A';
            pos.get(B[i]).add(i);
        }
        long ans=0;
        for(int i=1; i<=N; i++) {
            int t = pos.get(A[i]).pollFirst();
            ans += query(N - t + 1, BIT);
            update(N-t+1, 1, BIT);
        }
        System.out.println(ans);
        sc.close();
    }
    static void update(int x, int val, long BIT[]) {
        for(int i=x; i < BIT.length; i+=(i&-i))
            BIT[i] += val;
    }
    static long query(int x, long BIT[]) {
        long sum=0;
        for(int i=x; i>0; i-=(i&-i))
            sum += BIT[i];
        return sum;
    }
}
