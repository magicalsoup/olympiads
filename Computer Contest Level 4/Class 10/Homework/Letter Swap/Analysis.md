# Analysis

This problem is simply counting the number of inversion that string $`B`$ has from string $`A`$. Keep track of the positions of each letter, and use a BIT to count the inversions.

**Time Complexity:** $`O(N \log N)`$