import java.util.Scanner;
import java.util.HashSet;
public class seed4 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        long xk = sc.nextLong(), yk = sc.nextLong(), zk = sc.nextLong();
        int N = sc.nextInt();
        HashSet<Point> st = new HashSet<>();
        for(int i=0; i<N; i++) {
            long x = sc.nextLong(), y = sc.nextLong(), z = sc.nextLong();
            x = x - xk; y = y-yk; z = z - zk;
            long gcd = Math.abs(gcd(x, gcd(y, z)));
            st.add(new Point(x/gcd, y/gcd, z/gcd));
        }
        System.out.println(st.size());
        sc.close();
    }
    static long gcd(long p, long q){
        if(q == 0) return p;
        return gcd(q, p%q);
    }
    static class Point {
        long x, y, z;
        final int MOD = 1608893173;
        public Point(long x, long y, long z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        @Override
        public int hashCode() {
            int prime = 2357;
            return (int) ((x % MOD * prime % MOD) % MOD + (y * prime % MOD * prime % MOD) % MOD +
                    (z % MOD * prime % MOD & prime % MOD * prime % MOD) % MOD) % MOD;
        }

        @Override
        public boolean equals(Object o) {
            return o.hashCode() == hashCode();
        }
    }
}
