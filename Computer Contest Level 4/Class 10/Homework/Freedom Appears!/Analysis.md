# Analysis

We can find the slope of the different points from the starting point, and just eliminate any duplicate slopes. To do this, we can find the slopes and throw them into a set,
and the size of the set will be our answer.

To find the slope, simply subract $`x_i,y_i, z_i`$ from $`x_k, y_k, z_k`$, then by using euclidian gcd, divide them by their gcd to get the simplest form of their slope.

In c++, you cause use a `pair<int, pair<int, int>>` to quickly get the answer, in java you would need to implment your own triplet set and object.

**Time Complexity:** $`O(N \log N)`$