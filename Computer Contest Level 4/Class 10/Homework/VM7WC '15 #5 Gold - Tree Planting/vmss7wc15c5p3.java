import java.io.*;
public class vmss7wc15c5p3 {
    static Reader in = new Reader(System.in);
    static final int MOD = 1000000007;
    public static void main(String[]args) throws IOException{
        long BIT[][] = new long[4000][4000];
        int N = in.readInt();
        long tot=0;
        for(int i=0; i<N; i++) {
            int com = in.readInt();
            if(com == 1) {
                int r = in.readInt()+1, c = in.readInt()+1, t = in.readInt();
                update(r+c-1, r, t, BIT);
            }
            if(com == 2) {
                int r = in.readInt()+1, c = in.readInt()+1, x = in.readInt();
                tot += query(r+c-1, r, BIT) % MOD - query(r+c-1, r-x-1,BIT) % MOD;
                tot %= MOD;
            }
        }
        System.out.println(tot);
    }
    static class Reader {
        final private static int BUFFER_SIZE = 1 << 16;
        private static DataInputStream din = new DataInputStream(System.in);
        private static byte[] buffer = new byte[BUFFER_SIZE];
        private static int bufferPointer = 0, bytesRead = 0;
        public Reader(InputStream stream) {
            din = new DataInputStream(stream);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }
        public Reader(String fileName) throws IOException {
            din = new DataInputStream(new FileInputStream(fileName));
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }
        public  String readLine() throws IOException {
            byte[] buf = new byte[64]; // line length
            int cnt = 0, c;
            while ((c = Read()) != -1) {
                if (c == '\n')
                    break;
                buf[cnt++] = (byte) c;
            }
            return new String(buf, 0, cnt);
        }
        public  String read() throws IOException{
            byte[] ret = new byte[1024];
            int idx = 0;
            byte c = Read();
            while (c <= ' ') {
                c = Read();
            }
            do {
                ret[idx++] = c;
                c = Read();
            } while (c != -1 && c != ' ' && c != '\n' && c != '\r');
            return new String(ret, 0, idx);
        }
        public int readInt() throws IOException {
            int ret = 0;
            byte c = Read();
            while (c <= ' ')
                c = Read();
            boolean neg = (c == '-');
            if (neg)
                c = Read();
            do {
                ret = ret * 10 + c - '0';
            } while ((c = Read()) >= '0' && c <= '9');

            if (neg)
                return -ret;
            return ret;
        }
        public  long readLong() throws IOException {
            long ret = 0;
            byte c = Read();
            while (c <= ' ')
                c = Read();
            boolean neg = (c == '-');
            if (neg)
                c = Read();
            do {
                ret = ret * 10 + c - '0';
            } while ((c = Read()) >= '0' && c <= '9');
            if (neg)
                return -ret;
            return ret;
        }
        public  double readDouble() throws IOException {
            double ret = 0, div = 1;
            byte c = Read();
            while (c <= ' ')
                c = Read();
            boolean neg = (c == '-');
            if (neg)
                c = Read();

            do {
                ret = ret * 10 + c - '0';
            } while ((c = Read()) >= '0' && c <= '9');

            if (c == '.') {
                while ((c = Read()) >= '0' && c <= '9') {
                    ret += (c - '0') / (div *= 10);
                }
            }

            if (neg)
                return -ret;
            return ret;
        }
        private  void fillBuffer() throws IOException {
            bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
            if (bytesRead == -1)
                buffer[0] = -1;
        }
        private  byte Read() throws IOException {
            if (bufferPointer == bytesRead)
                fillBuffer();
            return buffer[bufferPointer++];
        }
        public void close() throws IOException {
            if (din == null)
                return;
            din.close();
        }
    }
    static void update(int r, int c, long val, long BIT[][]) {
        for(int j=c; j<BIT.length; j+=j&-j)
            BIT[r][j] += val;
    }
    static long query(int r, int c, long BIT[][]) {
        long sum=0;
        for(int j=c; j>0; j-=j&-j)
            sum += BIT[r][j];
        return sum;
    }
}