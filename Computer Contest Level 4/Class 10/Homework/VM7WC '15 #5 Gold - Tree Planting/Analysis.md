# Analysis

Since we are supporting two types of query, one for updating a specific square and one for sum of a diagonal, we can use a BIT. 

To query the diagonal, simply call

```java
query(r+c-1, r, BIT) - query(r+c-1, r-x-1,BIT);
```

To update a specific square, call
```java
update(r+c-1, r, t, BIT);
```

Also make sure to add one to the input to avoid BIT 0-index problems

**Time Complexity:** $`O(N \log (4 \times 10^6))`$
 