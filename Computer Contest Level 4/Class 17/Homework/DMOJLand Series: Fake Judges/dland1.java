import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class olyrim1 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt(), Q = readInt();
        long a[] = new long[N+1];
        for(int i=1; i<=N; i++)
            a[i] = readInt();
        int dif[] = new int[N+2];
        for(int i=0; i<Q; i++) {
            int l = readInt(), r = readInt();
            dif[l]++;
            dif[r+1]--;
        }
        ArrayList<pair> list = new ArrayList<>();
        for(int i=1; i<=N; i++) {
            dif[i] += dif[i - 1];
            list.add(new pair(i, dif[i]));
        }
        Collections.sort(list);
        Arrays.sort(a);
        long sum=0;
        for(int i=1; i<=N; i++) {
            sum += a[i]*list.get(i-1).val;
        }
        System.out.println(sum);
    }
    static class pair implements Comparable<pair>{
        int idx, val;
        public pair(int idx, int val) {
            this.idx=idx;
            this.val=val;
        }
        @Override
        public int compareTo(pair o) {
            return o.val - val;
        }
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}