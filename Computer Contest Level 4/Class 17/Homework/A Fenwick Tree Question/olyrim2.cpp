#include <bits/stdc++.h>
using namespace std;
#define INF 0x3f3f3f3f
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
#define For(i, a, b) for(int i=a; i<b; i++)
#define FOR(i, a, b) for(int i=a; i<=b; i++)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007;
const int MAXN = 500005, MAXQ = 100005;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}

int BIT[32][100005], a[100005], N, Q;
void upd(int idx, int x, int val) {
    for(int i=x; i<100005; i+=i&-i)
        BIT[idx][i] += val;
}
int sum(int idx, int x) {
    int sum=0;
    for(int i=x; i>0; i-=i&-i)
        sum += BIT[idx][i];
    return sum;
}
ll choose(ll n) { return n*(n-1)/2; }
int main() {
    scanf("%d %d", &N, &Q);
    for(int i=1; i<=N; i++) {
        scanf("%d", &a[i]); int x=a[i];
        For(j, 0, 31) {
            if(((x>>j) & 1)) {
                upd(j, i, 1);
            }
        }
    }
    while(Q--) {
        int cmd; scanf("%d", &cmd);
        if(cmd == 1) {
            int p, x; scanf("%d %d", &p, &x);
            int tmp=a[p];
            For(i, 0, 31) { if((tmp>>i) & 1) upd(i, p, -1); }
            For(i, 0, 31) { if((x>>i) & 1) upd(i, p, 1);}
            a[p]=x;
        }
        if(cmd == 2) { // OR
            ll ans=0; int l, r; scanf("%d %d", &l, &r);
            For(i,0, 31) {
                ll numOfBits = r-l+1, numOfZeroes = (r-l+1)-(sum(i, r) - sum(i, l-1));
                ans += (choose(numOfBits) - choose(numOfZeroes))*(1<<i);
            }
            printf("%lld\n", ans);
        }
        if(cmd == 3) { // And
            ll ans=0; int l, r; scanf("%d %d", &l, &r);
            For(i, 0, 31) {
                ll numofOnes = sum(i, r) - sum(i, l - 1);
                ans += choose(numofOnes)*(1<<i);
            }
            printf("%lld\n", ans);
        }
        if(cmd == 4) { // Xor
            ll ans=0; int l, r; scanf("%d %d", &l, &r);
            For(i, 0, 31) {
                ll numOnes = sum(i, r) - sum(i, l-1), numZeroes = (r-l+1)-(sum(i, r) - sum(i, l-1));
                ans += numOnes * numZeroes * (1<<i);
            }
            printf("%lld\n", ans);
        }

    }
}