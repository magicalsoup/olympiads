#include <bits/stdc++.h>
using namespace std;
#define f first
#define s second
typedef long long ll;
typedef pair<int, int> pii;

ll BIT[5010][5010];
void update(int r, int c, int val) {
	for(int i=r; i<5010; i+=i&-i)
		for(int j=c; j<5010; j+=j&-j)
			BIT[i][j] += val;
}
ll query(int r, int c) {
	ll sum=0;
	for(int i=r; i>0; i-=i&-i)
		for(int j=c; j>0; j-=j&-j)
			sum += BIT[i][j];
	return sum;
}
int N, Q;
int main() {
	cin>>N>>Q;
	while(Q--) {
		char com;
	    cin>>com;
		if(com == 'U') {
			int r1, c1, r2, c2, val;
			cin>>r1>>c1>>r2>>c2>>val;
			r1+=2; c1+=2; r2+=2; c2+=2;
			update(r1, c1, val);
			update(r2+1, c1, -val);
			update(r1, c2+1, -val);
			update(r2+1, c2+1, val);
		}
		if(com == 'Q') {
			int	r1, c1, r2, c2;
			cin>>r1>>c1>>r2>>c2;
			r1+=2; c1+=2; r2+=2; c2+=2; ll tot=0;
			for(int i=r1; i<=r2; i++) {
			    for(int j=c1; j<=c2; j++)
			        tot += query(i, j);
			}
			printf("%lld\n", tot);
		}
	}
}