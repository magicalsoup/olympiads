import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;

public class dutyofcall {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    static int N, R, val[] = new int[500005], cnt1=0, cnt2=0, q[] = new int[500005], f=0, r=-1;
    static ArrayList<Integer> adj[] = new ArrayList[500005];
    static boolean in[] = new boolean[500005];
    public static void main(String[]args) throws IOException{
        int N = readInt(), R = readInt();
        for(int i=0; i<=N; i++) adj[i] = new ArrayList<>();
        for(int i=1; i<=N; i++) {
            int k = readInt();
            for(int j=1; j<=k; j++) {
                int x = readInt(); in[x]=true;
                adj[i].add(x); adj[x].add(i);
            }
        }
        q[++r] = R; val[R] = -1;
        while(f <= r) {
            int u = q[f++];
            for(int v : adj[u]) {
                if(val[v]==0) {val[v] = -val[u]; q[++r] = v;}
            }
        }
        for(int i=1; i<=N; i++) {
            if(val[i] < 0 && !in[i]) cnt1++;
            if(val[i] > 0 && !in[i]) cnt2++;
        }
        System.out.println(cnt1 + " " + cnt2);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static String readLine() throws IOException {
        return br.readLine().trim();
    }
}