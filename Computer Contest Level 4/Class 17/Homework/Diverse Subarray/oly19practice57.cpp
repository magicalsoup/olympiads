using namespace std;
#define INF 0x3f3f3f3f
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
#define For(i, a, b) for(int i=a; i<b; i++)
#define FOR(i, a, b) for(int i=a; i<=b; i++)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
const int MM = 1000005, LOG = 18;
int N, Q, len[200010], pre[2*MM], st[LOG+10][200010];
void build() {
    for(int i=0; i<=N; i++)
        st[0][i] = len[i];
    for(int i=1; i<=LOG; i++) {
        for(int j=1; j+(1<<i)-1<=N; j++)
            st[i][j] = max(st[i-1][j], st[i-1][j+(1<<(i-1))]);
    }
}
int query(int l, int r) {
    if(l > r) return 0;
    int k = log2(r - l + 1);
    return max(st[k][l], st[k][r-(1<<k)+1]);
}
int main() {
    scanf("%d %d", &N, &Q); memset(pre, 0, sizeof(pre));
    for(int i=1, x; i<=N; i++) {
        scanf("%d", &x); x += 1000000;
        len[i] = min(len[i-1] + 1, i-pre[x]); pre[x] = i;
    }
    build();
    while(Q--) {
        int l, r; scanf("%d %d", &l, &r);
        if(len[r] >= r-l+1) printf("%d\n", r-l+1);
        else {
            int lo=l, hi=r, lft=r+1;
            while(lo <= hi) {
                int mid = lo+(hi - lo)/2;
                if(mid - len[mid] + 1 <= l) {lo = mid+1;}
                else { lft = mid; hi = mid-1; }
            }
            int ans = max(lft - l, query(lft, r));
            printf("%d\n", ans);
        }
    }
}