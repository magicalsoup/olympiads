import java.util.StringTokenizer;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
public class oly18novp1 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int n = readInt();
        segment a[] = new segment[n];
        for(int i=0; i<n; i++)
            a[i] = new segment(readInt(), readInt());

        Arrays.sort(a);
        int cnt=0;
        int last=-1;
        for(int i=0; i<n; i++) {
            if(a[i].start >= last) {
                last = a[i].end;
                cnt++;
            }
        }
        System.out.println(cnt);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static class segment implements Comparable<segment>{
        int start, end;
        public segment(int start, int end) {
            this.start=start;
            this.end=end;
        }
        @Override
        public int compareTo(segment other){
            return end - other.end;
        }

    }
}
