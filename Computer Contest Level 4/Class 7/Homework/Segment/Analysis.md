# Analysis

This problem is the same as the task scheduling problem mentioned before. We will use a greedy approach. We first sort the segments by their end time, then we will loop through,
greedily taking the nearest possible segments.

**Time Complexity:** $`O(N \log N)`$