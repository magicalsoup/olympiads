import java.util.Arrays;
import java.util.Scanner;
public class ceoi17p2 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double a[] = new double[n];
        double b[] = new double[n];
        for(int i=0; i<n; i++) {
            a[i] = sc.nextDouble();
            b[i] = sc.nextDouble();
        }
        Arrays.sort(a);
        Arrays.sort(b);
        double atot=0.0, btot=0.0, best=0;
        double cnt=0;
        for(int i=n-1, j=n-1; cnt<2*n;) {
            if(atot<=btot && i>=0) {
                atot += a[i];
                i--;
            }
            else if(btot < atot && j>= 0){
                btot += b[j];
                j--;
            }
            cnt++;
            best = Math.max(best, Math.min(atot, btot) - cnt);
        }
        System.out.printf("%.4f\n", best);
        sc.close();
    }
}
