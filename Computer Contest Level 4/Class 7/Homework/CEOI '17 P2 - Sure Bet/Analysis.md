# Analysis

This problem at a glance seems like you need to brute force all the combinations, but looking at the constraints, it's definetly not possible.

Thus, we need to make an observation. Since we want the *guranteed* amount of money we can get, that means we will get the minimum of the $`2`$ bets, if we bet on both of them.

Then, we can think of the problem this way. Suppose you have $`2 \times N`$ choices, what is the guranteed amount of money you can win? Since we can only take the minimum, we will
take a greedy approach. We will first sort the both arrays from greatest to least. Then, we want to maintain a $`atot`$ and $`btot`$, which represents the amount of total money you get
for each of the $`2`$ outcomes. We want to have the minimum difference between those two totals, so we have the maximum profit. Thus, we will add a bet to the lesser total, so
we can have the least difference. 

After each bet, we also consider stopping, and take the maximum between $`bestScore`$ and $`\min(atot, btot) - cnt`$, where $`cnt`$ is the amount of bets you have placed, since each
bet costs $`1`$ euro.

**Time Complexity:** $`O(N \log N)`$