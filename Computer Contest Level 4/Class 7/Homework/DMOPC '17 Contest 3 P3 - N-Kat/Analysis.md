# Analysis

This question was kinda hell to implement. Also, I'll mention **Kirito** and **r3mark** for providing the proof on why this solution works.

## Subtask 1

For $`20\%`$ of points, we can write two nested loops, both of which iterate over all $`O(2^N)`$ non-empty subsets. We can check in $`O(N)`$ time that the two subsets are distinct, and also determine the difference between the two subsets' values.

**Time Complexity:** $`O(2^{2N} \times N)`$

## Subtask 2

Call the value of a subset $`S`$, $`val(S)`$, the sum of the values of the elements in a subset $`S`$. For the remaining $`80\%`$ of points, we can generate all non-empty subsets as well as their values in $`O(N \times 2^N)`$. Then, we can sort these by their values in $`O(N \times 2^N)`$ time. The answer is then the pair of adjacent subsets in this sorted list which are disjoint and have the smallest difference.

To generate the subsets, we can use bitmask. For each bit, if its set, then we add the sum, if not, we don't add it. Thus we make all $`2^N`$ subsets. For each subset, we will
also add a pair value $`i`$, the original index, so we can find out the indexes that make up this sum later after we sort the array. After we sort it, we can mind the minimum
by using $`&`$ (and) to check if they are disjoint, and find the minimum difference. Once we find the $`2`$ subsets that have the minimal differnce, we can check their $`i`$, if
the $`j^{th}`$ bit is set, then that means the subset used this index, and we will output our answer.

**Note:** Do not use string, using integers is **much** faster. Also, you may need some optimizations and fast i/o to pass all test cases.



### Proof
To prove that this indeed works, consider the two subsets $`A`$ and $`B`$ such that $`A ∩ B=∅`$, $`val(A)≤val(B)`$ (and $`A`$ is before $`B`$ in the sorted list), and $`val(B)−val(A)`$ is minimized. To break ties, choose $`val(A)`$ minimal. To break further ties, choose $`B`$ such that $`B`$ appears as early as possible in the sorted list.

Assume for the sake of contradiction that $`A`$ and $`B`$ are not adjacent in the sorted list. Let subset $`C`$ be the subset directly after $`A`$ in the sorted list. By our assumption, $`B \ne C`$.

**Case 1: $`A ⊂ C`$**

Since all values of individual elements are positive, then $`val(A)<val(C)`$. Note that since $`A ∩ B = ∅, C \subset C`$. Also, since $` C`$ is between $`A`$ and $`B`$ in the sorted list,$`val(B)≥val(C)`$, so $`B \not\subset C`$. Now let $`B′=B∖(B∩C)`$ and $`C′=C∖(B∩C)`$. Note that $`val(B′)−val(C′)=val(B)−val(C)`$ and $`B′∩C′=∅`$. But $`val(B′)−val(C′)=val(B)−val(C)<val(B)−val(A)`$, which is a contradiction to $`val(B)−val(A)`$ minimal.

**Case 2: $` A \not\subset C `$**

Since $`val(A)≤val(C)`$, $`C \not\subset A`$. If $`A∩C=∅`$, then since $`val(C)≤val(B)`$ and $`C`$ appears before $`B`$, this is a contradiction to $`B`$ appearing the earliest (while the other conditions are satisfied, which they are for $`A`$ and $`C`$). Otherwise, let $`A′=A∖(A∩C)`$ and $`C′=C∖(A∩C)`$. But then $`val(A′)<val(A)`$ and $`val(C′)−val(A′)=val(C)−val(A)≤val(B)−val(A)`$, which is a contradiction to $`val(A)`$ being minimized while $`val(B)−val(A)`$ is minimized.

So in both these cases, there is a contradiction. So $`A`$ and $`B`$ are adjacent in the sorted list.

**Time Complexity:** $`O(N \times 2^N)`$