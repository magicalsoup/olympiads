import java.util.Scanner;
import java.util.Arrays;
public class dmopc17c3p3 {
    public static void main(String[]args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        pair dp[] = new pair[(1<<N)];
        int s[] = new int[N];
        for(int i=0; i<N; i++) s[i] = sc.nextInt();

        for(int i=0; i<(1<<N); i++) {
            int sum=0;
            for(int j=0; j<N; j++) {
                if((i&(1<<j)) == 0){
                    sum += s[j];
                }
            }
            dp[i] = new pair(sum, i);
        }
        Arrays.sort(dp);
        int min = 0x3f3f3f3f;
        int idx=0;
        for(int i=0; i<(1<<N)-1; i++) {
            if(((dp[i+1].s & dp[i].s) != 0) && Math.abs(dp[i].f - dp[i+1].f) < min) {
                min = Math.abs(dp[i].f - dp[i + 1].f);
                idx = i;
            }
        }
        for(int j=0; j<N; j++)
            if((dp[idx].s & (1<<j)) == 0)
                System.out.print((j+1)+" ");
        System.out.println();
        for(int j=0; j<N; j++)
            if((dp[idx+1].s & (1<<j)) == 0)
                System.out.print((j+1)+" ");
        //sc.close();
    }
    static class pair implements Comparable<pair>{
        int f, s;
        public pair(int f, int s) {
            this.f=f;
            this.s=s;
        }
        @Override
        public int compareTo(pair other) {
            return f - other.f;
        }
    }
}
