# Analysis

First we need to make an insight: If a tile has an odd width, length or both, we need to make it even. 

Lets say the tile is a $`3 \times 4`$ board, all we need to do is subtract one from the width, to make a $`2 \times 4`$ board. It goes the same for the length. As such, we just need to
tile a column or row of $`1 \times 1`$ tiles to make the side length even.

If both side lengths or odd, then we need to subtract both, which we need to tile  $`height + width - 1`$ number of $`1 \times 1`$ tiles.  

Then we can just continue to divide the length and width by $`2`$, until either of them becomes $`0`$, which we will return $`0`$.

**Time Complexity:** $`\log (N \times M)`$