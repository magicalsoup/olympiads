import java.util.Scanner;
public class dwite10c1p3 {
    static int ans=0;
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        for(int t=1; t<=5; t++) {
            int n = sc.nextInt(), m = sc.nextInt();
            System.out.println(recur(m, n));
        }
        sc.close();
    }
    static int recur(int height, int width){
        if(height == 0 || width == 0) return 0;
        if(height % 2 == 0 && width % 2 == 0) return recur(height / 2, width / 2);
        if(height % 2 == 1 && width % 2 == 0) return width + recur(height / 2, width / 2);
        if(height % 2 == 0 && width % 2 == 1) return height + recur(height / 2, width / 2);
        return height + width - 1 + recur(height / 2, width / 2);
    }
}
