# Analysis

This problem has many different ways to solve this, including an easy implemented iterative dp solution. However, for simplicity, I will talk about the graphy theory solution.

The naive implementation would be to dfs every single node, and find the sum of the subtree. However, we can store previous answers, and update them along the tree.

Since there are $`N-1`$ edges, we know that the graph is a tree, and that its an directed graph. Now we will root node $`1`$ as the root, as he has no superior. Then we will recursion
down the tree, the sum of the current subtree is the sum of all the subtrees beneath it, and thus, we will only require one dfs. 

**Time Complexity:** $`O(N + N-1) \rightarrow O(N)`$  