# Class 7

## Task Scheduling Problem
- Given a set $`T`$ of $`n`$ tasks, each having: 
    - A start time, $`s_i`$
    - A finish time, $`f_i`$
- Goal: Perform all the tasks using a minimum number of "machines".

### Solutions
- Sort by start time
- Overlap idea (with difference array, coordinate compression, min machine is the the most overlap)
- Keep a map data structure, binary search to find lowest time needed
- **Time Complexity:** $`O(N \log N)`$

## Harder Scheduling Problem
- Giving a set $`T`$ of $`n`$ tasks, each having
    - A start time, $`s_i`$
    - A finish time, $`f_i`$
- Goal: Perform the maximum number of tasks using $`1`$ machine.

### Solutions
- Sort by end time, greedy.
- Find the maximum overlapping end time, that will be the minimum number of machines needed.
- Use difference array to mark, then run prefix sum
- If the numbers are too big, use coordinate compression
- **Time Complexity:** $`O(N \log N)`$

## Even harder Scheduling Problem
- Giving a set $`T`$ of $`n`$ tasks, each having
    - A start time, $`s_i`$
    - A finish time, $`f_i`$
    - A profit, $`p_i`$
- Goal: Choose the task so that none overlap and gives the maximum profit

### Solutions
- Sort by end time, but with dp this time instead of greedy
- define $`dp[i]`$ as the maximum profit by choosing the tasks from $`1`$ to $`i`$.
- We can binary search for the task that we can do if we choose the current one, thus we arrive with a dp transition.
    - $`A(0) = 0`$ 
    - $`A(i) = \max \{A(i-1), g_i + A(H(i))\},  i \in \{1, \cdots, n\}`$
- **Time Complexity:** $`O(N \log N)`$

