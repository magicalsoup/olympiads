# Analysis

For $`50\%`$ of the points, we can brute force and find all the possible pairs, by iterating 2 nested for loops.

**Time Complexity:** $`O(N^2)`$.

For the full points, we need to implement a 2-pointer approach. First we sort the array, then we keep a left and right pointer, where one starts at the start and the other at the end of the array.
Since the array is sorted, that means if $`a[l] + a[r] \le m`$, that means all the elements between $`l`$ and $`r`$ are also pairs that match the constraints.
And everytime $`a[l] + a[r] \le m`$, we increase $`l`$ by one and add $`r - l`$ to the answer, and if its bigger than $`m`$, we decrease $`r`$ by 1. Keep in mind
that the number of pairs can be large, so we need a 64-bit data type to store the answer.

**Time Complexity:** $`O(N \log_2(N) + N)`$