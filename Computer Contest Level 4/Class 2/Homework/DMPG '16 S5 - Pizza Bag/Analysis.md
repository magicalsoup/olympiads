# Analysis

First of all, we need to make some changes to how we store the values, in other words, we need to solve the problem on array, not a circle. Since $`K \le N`$, and
when we pick the slices, they need to be touching each other, we can simply extend the array by twice its original size, this way, when we choose from the $`N^{th}`$
slice, we will just loop around back to the $`1^{st}`$ slice.

-----
For $`15\%`$ of the points, since $`N`$ is rather small, we can implement a prefix sum array, and do 2 nested for loops for all the ranges, and get the sum
in $`O(1)`$ using a prefix sum array.

**Time Complexity:** $`O((2N)^2)`$

------

For another $`15\%`$ of the points, we can again use a prefix sum array, but since $`N`$ can be as big as $`10^5`$ here, we  cannot do the same approach as before.
Except, since all values are strictly positive, that means we want the range as big as possible, and we can thus move through the array once, only calculating
the sum that has a range of $`K`$.

**Time Complexity:** $`O(2N)`$

------

For the last 3 subtasks, the first 2 may be solved with unorthodox methods, which I will not cover. The general solution is to implement a minimum monotonic deque
and a prefix sum array. We keep a minimum monotonic deque to keep track of the minimum value, or the value which we do not want in our slices. Its very simple to
implement. First make the prefix sum array. Then set an $`r`$ (similar to [a classic problem](https://dmoj.ca/problem/dmopc15c6p5), as before). Then add an element that has a value of `0`. This is
important, since we want the values to not keep in our maximum to be negative, as anything positive is good.

Now we do the minimum monotonic deque, the element which we are putting in is $`psa[r]`$. Why? because $`psa[r]`$ is going to be subtracted later on, (if it stays the minimum), its technically the $`psa[l-1]`$ part of 
$`psa[r] - psa[l-1]`$ part of calculating the sum with prefix sum array. And we basically do a normal mono deque, if the added element does not make
the deque in increasing order, pop from the tail until it does. Then add the element in. Then check if the top element in the deque is still part of the 
range $`r - k`$, if its not, pop it out of the deque, Then we just update the maximum, `ans = max(ans, psa[r] - dq.peekFirst().val)`. And print out our answer!

**Time Complexity:** $`O(2N)`$