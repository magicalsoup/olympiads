import java.util.ArrayDeque;
import java.util.Scanner;
import java.util.Deque;
public class dmpg16s5 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), K = sc.nextInt();
        long psa[] = new long[2*N+1];
        for(int i=1; i<=N; i++) {
            long x = sc.nextLong();
            psa[i] = x;
            psa[i+N] = x;
        }
        for(int i=1; i<2*N; i++)
            psa[i] += psa[i-1];
            
        Deque<pair> dq = new ArrayDeque<>();
        long max=0;
        dq.add(new pair(0, 0));
        for(int r=1; r<2*N; r++) {
            while(!dq.isEmpty() && dq.peekLast().val >= psa[r])
                dq.pollLast();
            dq.add(new pair(r, psa[r]));
            if(dq.peekFirst().idx < r-K)
                dq.pollFirst();
            max = Math.max(max, psa[r] - dq.peekFirst().val);
        }
        System.out.println(max);
        sc.close();
    }
    static class pair {
        int idx; long val;
        public pair(int idx, long val) {
            this.idx=idx;
            this.val=val;
        }
    }
}
