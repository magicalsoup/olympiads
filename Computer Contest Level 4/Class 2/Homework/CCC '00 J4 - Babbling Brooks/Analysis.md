# Analysis

This problem is just an implmentation problem. Keep an arraylist, or a vector, or a data structure that can delete, insert, add, set stuff rather quickly and easily. 

When the command is `99`, set/update the value of the mainstream and `index`, then add a new stream to your data structure, at `index+1`.

When the command is `88`, update the `index` stream, and remove the `index+1` stream. 

Finally, just print out each of the streams in your data strucutre, don't forget to round the numbers.

**Time Complexity:** $`O(N)`$