# Analysis

This problem should be pretty straight forward, simply scan in the 2 arrays. Then just accumulate the sums of each team, when they equal, update the index. Finally, print out the answer.

**Time Complexity:** $`O(N)`$.