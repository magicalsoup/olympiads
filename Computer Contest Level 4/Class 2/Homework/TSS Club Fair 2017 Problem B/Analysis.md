# Analysis

For $`50\%`$ of the points, its sufficent enough for each query, to check all the houses using the distance formula: $`\sqrt{(y2-y1)^2 + (x2-x1)^2}`$ and see if its within the radius, a brute force solution.

**Time Complexity:** $`O(NQ)`$

Interesting enough, another way that I used to get $`50\% - 60\%`$ of the points, was using an frequnecy array + prefix sum array. If you look at the constraints, 
the maximum distance away from the center is really just $`r`$, $`\sqrt{(10^6)^2 + (10^6)^2} = 1414213.56`$. So all you had to do was create an array about 
$`2 \times 10^6`$ size, then rounding the distance of the houses from the center, and adding 1 do every house of that radius. (`freq[radius]++`). Then, just prefix sum array
the frequency array, and `freq[radius]` will be the answer. However, this did not work for the full points, as by rounding the distance, I'm miscalculating some of
the values, and a Map would just be $`O(N^2)`$, so there was no one to further optimize this solution.

**Time Complexity:** $`O(N + Q)`$

For $`100\%`$ of the points, you needed to implement an upper_bound/lower_bound. First sort the array, then, if the house that exceeds the radius is at index $`i`$, that means
all of the houses below it, or $`i-1`$ houses are within the radius. With this, we can just use binary search to find the first house that exceeded the radius limit.
This means, we sort the array in $`O(N \log_2(N))`$ time, and answer each query in $`O(\log_2(N))`$, time.

**Time Complexity:** $`O(N \log_2(N) + Q \log_2(N))`$
