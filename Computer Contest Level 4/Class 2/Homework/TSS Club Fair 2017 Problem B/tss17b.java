import java.util.Arrays;
import java.util.Scanner;
import java.util.HashMap;
public class tss17b {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), Q = sc.nextInt();
        double distance[] = new double[N];
        for(int i=0; i<N; i++) {
            long x = sc.nextLong(), y = sc.nextLong();
            double dist = (Math.sqrt(x*x + y*y));
            distance[i] = dist;
        }
        Arrays.sort(distance);
        for(int i=0; i<Q; i++) {
            int r = sc.nextInt();
            System.out.println(upper_bound(distance, r)==-1? 0:upper_bound(distance, r));
        }
        sc.close();
    }
    static int upper_bound(double a[], double key) {
        int lo = 0, hi = a.length - 1;
        while(lo <= hi) {
            int mid = (lo + hi) / 2;
            if(a[mid] == key) lo = mid + 1;
            if(a[mid] > key) hi = mid - 1;
            else lo = mid + 1;
        }
        return lo;
    }
}