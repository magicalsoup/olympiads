# Analysis

At first glance, it seems impossible. Even with the knowledge of mono-deque, $`N`$ can be as big as 3e6, theres no way brute-forcing mono-deque is going to work.
However, after a bit of debugging, we can find some insights. For this question, we can use a 2-pointer approach. Lets first fix the right pointer, and not count
all the subarray. Lets first print the matching left pointers with a brute force solution:
```java
import java.util.Scanner;
import java.util.Collections;
public class dmopc15c6p5 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(); int K = sc.nextInt();
        int a[] = new int[N+1];
        for(int i=1; i<=N; i++)
            a[i] = sc.nextInt();

        for(int r=1; r<=N; r++) {
            for(int l=1; l<=r; l++) {
                int max=0;
                int min=0x3f3f3f3f;
                for (int i = l; i <= r; i++) {
                    max = Math.max(max, a[i]);
                    min = Math.min(min, a[i]);
                }
                if(max - min <= K) {
                    System.out.println(l + " " + r);
                    break;
                }
            }
        }
        sc.close();
    }
}
```

With the program above, if we run the sample case, we got left pointer values of `1 2 2 2 5`. You can try some more cases, but we can see that the left pointers
are always increasing. This means, that if a $`L`$ to $`R`$ subarray is valid, then all subarrays within it are also valid (see this [question](https://dmoj.ca/problem/oly18decp4) for a similar problem). So now its the case of determing the left pointer
for each R pointer, and then we can just add $`r - l + 1`$ to our answer.

Now, how do we find this $`L`$? We can use mono-deque!

With mono-deque, we can keep track of the maximum and minimum in the ranges $`L`$ to $`R`$, and easily find $`L`$. All we have to do is keep an increasing deque for the minimum,
and a decreasing deque for the maximum. If the element that is going to be inserted is not in the order of those deques, we pop them out, then we insert the element
into the respective deque. Then we loop through from the top of each deque, and find the $`L`$ that we need, poping out any elements that are not in the ranges $`[L, R]`$.

**Time Complexity:** $`O(N)`$ (mono-deque), or $`O(N \log(N))`$ (PriorityQueue)