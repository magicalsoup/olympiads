import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
public class dmopc15c6p5 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt(), K = readInt();
        int a[] = new int[N+1];
        for(int i=1; i<=N; i++)
            a[i] = readInt();
        Deque<pair> max = new ArrayDeque<pair>();
        Deque<pair> min = new ArrayDeque<pair>();
        int l=1;
        long ans=0;
        for(int r=1; r<=N; r++) {
            while(!max.isEmpty() && max.peekLast().val <= a[r])
                max.pollLast();
            max.add(new pair(r, a[r]));
            while(!min.isEmpty() && min.peekLast().val >= a[r])
                min.pollLast();
            min.add(new pair(r, a[r]));
            while(!min.isEmpty() && !max.isEmpty() && max.peekFirst().val - min.peekFirst().val > K) {
                l = Math.min(max.peekFirst().idx+1, min.peekFirst().idx+1);
                while(max.peekFirst().idx < l)
                    max.pollFirst();
                while(min.peekFirst().idx < l)
                    min.pollFirst();
            }
            ans += r - l + 1;
        }
        System.out.println(ans);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static class pair {
        int idx, val;
        public pair(int idx, int val) {
            this.idx=idx;
            this.val=val;
        }
        @Override
        public String toString() {
            return "index " + idx + " value " + val;
        }
    }
}
