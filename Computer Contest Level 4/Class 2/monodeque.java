import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;
public class monodeque2 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), W = sc.nextInt();
        int a[] = new int[N+1];
        for(int i=1; i<=N; i++)
            a[i] = sc.nextInt();
        Deque<pair> dq = new ArrayDeque<pair>();
        int ans=0;
        for(int r=1; r<=N; r++) { 
            if(dq.isEmpty() || a[r] <= dq.peekLast().val) // if the deque is empty or adding the element will still make the deque decreasing
                dq.add(new pair(r, a[r])); // add the new element
            else {
                while(!dq.isEmpty() && dq.peekLast().val < a[r]) // while adding this element will make this monotonic deque not decreasing
                    dq.pollLast(); // pop the tail element from the deque, we will do this until the monotonic will be decreasing when adding this new element
                dq.add(new pair(r, a[r])); // add the new element
            }
            if(!dq.isEmpty() && dq.peekFirst().idx <= r-W)  // if the head element is no longer in the range
                dq.pollFirst(); // pop it out of the deque
            if(r >= W) ans += dq.peekFirst(); // if we have passed the first range, we can start checking and adding up the answer
        }
        System.out.println(ans);
        sc.close();
    }
    static class pair { // class to store the index and value for mono deque
        int idx, val;
        public pair(int idx, int val) {
            this.idx=idx;
            this.val=val;
        }

        @Override
        public String toString() {
            return "index " + idx + " value " + val;
        }
    }
}
