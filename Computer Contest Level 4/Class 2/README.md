# Class 2

## Mono Deque
- Meaning that everything in the Deque is in decreasing or increasing order

## Common Problem With Sliding Window
- Find the minimum or maximum between a window
- **Time Complexity:** $`O(N)`$
- Simply keep a decreasing deque. The front of the deque is the maximum/minimum element in the window
- For every insert, if after inserting the deque is still decreasing, insert it. If not, pop until it is decreasing.
- For every window, check if the front of the deque is still within the window, if not, pop it out of the deque.
- See this problem for pratice: https://leetcode.com/problems/sliding-window-maximum/
- The algorithm is implmented below

```java
public int[] maxSlidingWindow(int[] a, int k) {

        Deque<pair> dq = new ArrayDeque<pair>(); // initialize dq, we are keeping an decreasing deque
        ArrayList<Integer> al = new ArrayList<Integer>(); // add the maximums into this arraylist
        for(int r=0; r<a.length; r++) { // set an R index
            if(dq.isEmpty() || a[r] <= dq.peekLast().val) // if the deque is emtpy or if the element is smaller or equal to the tail element
                dq.add(new pair(r, a[r])); // insert the element into he deque
                
            else { // if the element is bigger than the tail element
                while(!dq.isEmpty() && dq.peekLast().val < a[r]) // while the elements in the deque are smalelr than the element
                    dq.pollLast(); // pop them from the deque
                dq.add(new pair(r, a[r])); // add the new element into the deque
            }
            if(!dq.isEmpty() && dq.peekFirst().idx <= r-k)  // if the first element is no longer in the range of the window
                dq.pollFirst(); // pop the first element from the deque
            if(r>=k-1) al.add(dq.peekFirst().val);  // if we have passed the the first range, we can start finding the maximums
        }
        int ans[] = new int[al.size()]; 
        for(int i=0; i<al.size(); i++)
            ans[i] = al.get(i);
        return ans;
    }
    static class pair { // create a class to store the index and value of an element
        int idx, val;
        public pair(int idx, int val) {
            this.idx=idx;
            this.val=val;
        }
    }
```