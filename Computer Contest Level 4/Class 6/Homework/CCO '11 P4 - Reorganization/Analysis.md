# Analysis

This problem has a relatively simple solution. 

We can simply just greedy + simulate the reorganization process.

We first create a sorted map, to keep track of how many people each person is supervising. Then, for each current rank we read in, we will binary search for the lowest rank
possible in the map, (as per the rules of the reorganization process in the problem statement), if we cannot find anybody, this means that there is no once capable of supervising
the current person, as such we should output `NO`, as its not possbile to reorganize the company. If we do find somebody, (lets call them $`k`$), we will assign one person to
person $`k`$ in our map. If $`k`$ is now supervising $`2`$ people, remove $`k`$ from the map, as he cannot supervise anymore people. Then we put the current person into the map,
and he is supervising $`0`$ people. At the end, if we are able to find a $`k`$ for everyone, then it's possible to reorganize the company, and as such, you should output `YES`.

**Time Complexity:** $`O(N \log N)`$