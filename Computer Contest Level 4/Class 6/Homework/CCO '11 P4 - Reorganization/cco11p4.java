import java.util.TreeMap;
import java.util.Scanner;
public class cco11p4 {
    public static void main(String[]args) {
        Scanner sc = new Scanner(System.in);
        TreeMap<Integer, Integer> tm = new TreeMap<>();
        int n = sc.nextInt(), r = sc.nextInt();
        tm.put(r, 0);
        for(int i=1; i<n; i++) {
            r = sc.nextInt();
            Integer key = tm.floorKey(r);
            if(key == null) {
                System.out.println("NO");
                System.exit(0);
            }
            tm.put(key, tm.get(key) + 1);
            if(tm.get(key) == 2) {
                tm.remove(key);
            }
            tm.put(r, 0);
        }
        System.out.println("YES");
        sc.close();
    }
}
