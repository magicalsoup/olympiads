import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Deque;
import java.util.ArrayDeque;
public class hopscotch2 {
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static Reader in = new Reader(System.in);
    public static void main(String[]args) throws IOException{
        int N = in.readInt(), K = in.readInt();
        int pre[] = new int[N+1];
        long a[] = new long[N+1];
        for(int i=1; i<=N; i++)
            a[i] = in.readLong();
        long dp[] = new long[N+2];
        Deque<pair> dq = new ArrayDeque<>();
        dq.add(new pair(0, 0L));
        for(int r=0; r<=N; r++) {
            dp[r] = dq.peekFirst().val + a[r];
            pre[r] = dq.peekFirst().idx;
            while(!dq.isEmpty() && dq.peekLast().val >= dp[r])
                dq.pollLast();
            dq.add(new pair(r, dp[r]));
            if(dq.peekFirst().idx <= r-K) dq.pollFirst();
        }
        dp[N+1] = dq.peekFirst().val;
        pw.println(dp[N+1]);
        int cnt=0;
        int ans[] = new int[N+1];
        for(int i=dq.peekFirst().idx; i!=0; i=pre[i], cnt++)
            ans[cnt] =  i;
        for(int i=cnt-1; i>=0; i--)
            pw.print(ans[i] + " ");
        pw.close();
    }
    static class Reader {
        final private static int BUFFER_SIZE = 1 << 16;
        private static DataInputStream din = new DataInputStream(System.in);
        private static byte[] buffer = new byte[BUFFER_SIZE];
        private static int bufferPointer = 0, bytesRead = 0;
        public Reader(InputStream stream) {
            din = new DataInputStream(stream);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }
        public Reader(String fileName) throws IOException {
            din = new DataInputStream(new FileInputStream(fileName));
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }
        public  String readLine() throws IOException {
            byte[] buf = new byte[64]; // line length
            int cnt = 0, c;
            while ((c = Read()) != -1) {
                if (c == '\n')
                    break;
                buf[cnt++] = (byte) c;
            }
            return new String(buf, 0, cnt);
        }
        public  String read() throws IOException{
            byte[] ret = new byte[1024];
            int idx = 0;
            byte c = Read();
            while (c <= ' ') {
                c = Read();
            }
            do {
                ret[idx++] = c;
                c = Read();
            } while (c != -1 && c != ' ' && c != '\n' && c != '\r');
            return new String(ret, 0, idx);
        }
        public int readInt() throws IOException {
            int ret = 0;
            byte c = Read();
            while (c <= ' ')
                c = Read();
            boolean neg = (c == '-');
            if (neg)
                c = Read();
            do {
                ret = ret * 10 + c - '0';
            } while ((c = Read()) >= '0' && c <= '9');

            if (neg)
                return -ret;
            return ret;
        }
        public  long readLong() throws IOException {
            long ret = 0;
            byte c = Read();
            while (c <= ' ')
                c = Read();
            boolean neg = (c == '-');
            if (neg)
                c = Read();
            do {
                ret = ret * 10 + c - '0';
            } while ((c = Read()) >= '0' && c <= '9');
            if (neg)
                return -ret;
            return ret;
        }
        public  double readDouble() throws IOException {
            double ret = 0, div = 1;
            byte c = Read();
            while (c <= ' ')
                c = Read();
            boolean neg = (c == '-');
            if (neg)
                c = Read();

            do {
                ret = ret * 10 + c - '0';
            } while ((c = Read()) >= '0' && c <= '9');

            if (c == '.') {
                while ((c = Read()) >= '0' && c <= '9') {
                    ret += (c - '0') / (div *= 10);
                }
            }

            if (neg)
                return -ret;
            return ret;
        }
        private  void fillBuffer() throws IOException {
            bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
            if (bytesRead == -1)
                buffer[0] = -1;
        }
        private  byte Read() throws IOException {
            if (bufferPointer == bytesRead)
                fillBuffer();
            return buffer[bufferPointer++];
        }
        public void close() throws IOException {
            if (din == null)
                return;
            din.close();
        }
    }
    static class pair {
        long val; int idx;
        public pair(int idx, long val) {
            this.idx=idx;
            this.val=val;
        }
    }
}
