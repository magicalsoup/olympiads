# Analysis

For this problem, we are going to combine some dp and data structures! First we define our dp transition. Lets say $`dp[i]`$ contains the minimum cost from getting from stone $`0`$ to
stone $`i`$. To get to stone $`i`$, $`dp[i]`$ must be from any stone from $`[i-K, i]`$, thus $`dp[i]`$ will be the minimum from that range. Now, how can we get this minimum? The
answer is min queue. We will use a mono deque to keep track of $`dp[i]`$. $`dp[i] = dq.top().val + a[i]`$, this represent the minimum cost to get to the current stone, and inorder
to step on the current stone, we also have to cool it, thus adding the cost ($`a[i]`$). Then, we will treat this as a value and add it into our mono deque, and we will pop out other
dp elements from the mono deque if necessary. (If you don't understand, first learn how to implement a normal mono deque). Be sure to pop the front of the deque when necessary as well.
After all the stones have been processed, we care about $`dp[N+1]`$, as we need to **get over to the other side**, rather than just landing on the last stone. $`dp[N+1]`$ is simply
the minimum element from the mono deque (jumping from stone $`i`$, such that $`~i-k \le i \le N`$). With this, we can output $`dp[N+1]`$, and that will be the minimum power bruce needs
to get to the other side. 

With the hard part done, we can now implement on how to get the indexes. Surprisingly, its very simple. We will keep a parent array, basically $`par[v] = i`$, that means $`v`$ came
from $`i`$. Then we just walk backwards, starting from the minimum elements index, and that will be the indexes in **reverse order**, the rest is left as an exercise to the reader.

**Time Complexity:** $`O(N)`$