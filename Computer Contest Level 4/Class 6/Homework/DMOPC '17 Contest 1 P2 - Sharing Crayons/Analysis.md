# Analysis

This is just the editorial, cause I think its more trustable than my own analysis, author is **Kirito** (Roger Feng).

## Subtask 1
For the first subtask, we can loop through all subarrays and find their sum, and count how many are divisible by $`M`$.

**Time Complexity:** $`O(N^3)`$

## Subtask 2
For the second subtask, we can iterate through all subarrays and find their sum using a prefix sum array, and count how many are divisible by $`M`$.

**Time Complexity:** $`O(N^2)`$

## Subtask 3
Let $`P_i=\sum_{j=1}^{i}{A_i}`$ (basically prefix sum array).

For the third subtask, notice that the sum of a subarray $`A[l,r]`$ will be divisible by $`M`$ if $`P_r \mod M ≡ P_{l−1} \mod M`$. Thus we can store a counter array $`C`$ of size $`M`$ such that the $`i^{th}`$ entry is the number of prefix sums that are congruent to $`i \mod M`$. Thus we can just loop through $`A`$, and add $`C[A_i]`$ to our answer, and then increment it by one.

**Time Complexity:** $`O(N)`$

## Subtask 4
For the fourth subtask, we can replace the counter array with a map data structure, as an array of size $`10^9`$ will MLE.

**Time Complexity:** $`O(N)`$