# Analysis

We realize that the person in line should never wait if there is a microwave available, or else it will just lengthen the line. Thus, we can implement a greedy solution.

First we sort the people by arrival time. Then we simulate the process of guiding people. For each microwave, we will keep 2 values, a used value and a endtime value, denoting
if the microwave is being used and the time where the person will be done using it. 

We loop through the microwaves to see if its endtime is sooner than the current person's start time, if it is, then that must mean this microwave is unoccupied, and we will set
this microwave's used value to false. Then, we will try to find a microwave for `TT1103`. The only way for `TT1103` to use a microwave inbetween people if a microwave's endtime has at least
$`T`$ minutes of difference between the current person's start time. Thus, we loop through the microwaves once more and check for such a microwave, and find the minimum time out of
all possible ones. Then we will proceed to find a microwave for the current person, loop through the microwaves once more to find unused microwaves, assign the current person to
the unsed microwave with the maximum endtime, as this means we can assign `TT1103` to unused microwaves with a smaller time to wait. If we cannot find an unsed microwave, find the 
microwave with the earliest endtime, and assign this person to that microwave. Once a microwave is assigned, set the used value to true.

Lastly, if we couldn't squeeze `TT1103` to microwave his food inbetween any of the times while people are microwaving, this means he must wait till everyone has gone, and then microwave his food.
As such, we will find the earliest end time of all the microwaves after everyone has been processed, and assign `TT1103` to that microwave.

Alternatively, we can use binary search to quickly assign microwaves to people.

**Time Complexity:** $`O(M \log M)`$ (binary search) or $`O(M \times 4N \log M) \rightarrow O(MN \log M)`$ (pure implementation).