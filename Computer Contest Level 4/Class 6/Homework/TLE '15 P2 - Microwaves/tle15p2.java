import java.util.Arrays;
import java.util.Scanner;
public class tle15p2 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), M = sc.nextInt(), T = sc.nextInt();
        pair people[] = new pair[M];
        microwave micro[] = new microwave[N]; // false for not used, true for used
        for(int i=0; i<N; i++) micro[i] = new microwave(0L, false);
        for(int i=0; i<M; i++)
            people[i] = new pair(sc.nextInt(), sc.nextInt());
        Arrays.sort(people);
        long ans=-1;
        for(int i=0; i<M; i++) {
            boolean found=false;
            for(int j=0; j<N; j++) {
                if (micro[j].endtime <= people[i].start) {
                    micro[j].used = false;
                }
            }
            for(int j=0; j<N; j++) { // finding a microwave for TT1103
                if (people[i].start - micro[j].endtime >= T ) { // if we found one
                    ans = ans==-1? micro[j].endtime : Math.min(ans, micro[j].endtime);
                }
            }
            long max = -1;
            int fidx = -1;
            for(int j=0; j<N; j++) {
                if(!micro[j].used && micro[j].endtime > max) { // if we find a microwave that isn't used
                    max = micro[j].endtime;
                    fidx = j;
                    found=true;
                }
            }
            if(fidx != -1) {
                micro[fidx].used = true;
                micro[fidx].endtime = people[i].start + people[i].time;
            }
            if(!found) { // if this person didn't find a microwave
                long min = 0x3f3f3f3f3f3fL;
                int idx=0;
                for(int j=0; j<N; j++) {
                    if (micro[j].endtime < min) {
                        min = micro[j].endtime; // get the soonest end time
                        idx = j;
                    }
                }
                micro[idx].endtime = min + people[i].time; // the new endtime
            }
        }
        if(ans == -1) { // if TT1103 needs to wait till everyone has gone
            long min = 0x3f3f3f3f3f3f3fL;
            for(int j=0; j<N; j++)
                min = Math.min(min, micro[j].endtime);
            ans = min;
        }
        System.out.println(ans);
        sc.close();
    }
    static class microwave {
        long endtime; boolean used;
        public microwave(long endtime, boolean used) {
            this.endtime = endtime;
            this.used=used;
        }
    }
    static class pair implements Comparable<pair>{
        int start, time;
        public pair(int start, int time){
            this.start=start;
            this.time=time;
        }
        @Override
        public int compareTo(pair other) {
            return start - other.start;
        }
    }
}
