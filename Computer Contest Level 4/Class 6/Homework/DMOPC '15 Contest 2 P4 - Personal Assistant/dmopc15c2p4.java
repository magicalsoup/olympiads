import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.StringTokenizer;
public class dmopc15c2p4 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String []args) throws IOException{
        int N = readInt();
        anime animes[] = new anime[N];
        for(int i=0; i<N; i++) {
            long r = readLong();
            long l = readLong();
            long h = readLong();
            animes[i] = new anime(r, r + l - 1, h);
        }
        Arrays.sort(animes);
        long dp[] = new long[N+1];
        for(int i=0; i<N; i++) {
            int idx = lower_bound(animes[i].start, animes)-1;
            if(i > 0) dp[i] = dp[i-1];
            if(idx < 0) // if there isn't any anime we can watch
                dp[i] = Math.max(dp[i], animes[i].h);
            else
                dp[i] = Math.max(dp[i], dp[idx] + animes[i].h);

        }
        System.out.println(dp[N-1]);
    }
    static int lower_bound(long key, anime arr[]) {
        int low=0;
        int high = arr.length;
        while(low < high){
            final int mid = low + (high - low) / 2;
            if(key > arr[mid].endtime)
                low = mid+1;
            else
                high = mid;
        }
        return low;
    }
    static class anime implements Comparable<anime>{
        long start, endtime, h;
        public anime(long start, long endtime, long h){
            this.start=start;
            this.endtime=endtime;
            this.h=h;
        }
        @Override
        public int compareTo(anime other){
            return Long.compare(endtime, other.endtime);
        }
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static long readLong() throws IOException {
        return Long.parseLong(next());
    }
}
