# Analysis

This is a variation of the classical knapsack problem.

## Subtasks 1 & 2
For subtasks $`1`$ and $`2`$, let $`dp[i]`$ represent the maximum possible happiness value we can obtain if we only consider the $`i^{th}`$ minute and onward. Iterate backwards through the array, for each minute with an anime, we can either skip it or watch it. If anime $`j`$ is released on the $`i^{th}`$ minute, we have the transition formula $`dp[i]=max(dp[i+1],H[j]+dp[i+L[j]])`$.

## Subtask 3
For subtask $`3`$, we need to realize that only the minutes with animes matter. The happiness value we can obtain from watching an anime can be found by greedily binary searching for the next anime we can watch if we watch this one. Implementation is left to the reader as an exercise.

**Time Complexity:** $`O(N \log N)`$

