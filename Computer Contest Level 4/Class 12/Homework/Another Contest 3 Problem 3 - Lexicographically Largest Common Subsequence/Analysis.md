# Analysis

Lemma: The subseqeunce must be in reverse alphabetical order.

Proof: By contradiction - if the string is not, take the first occurrence of an index $`i`$ where $`s_i<s_{i+1}`$. Deleting character $`s_i`$ results in a common subsequence that is lexicographically larger.

The solution follows directly from this - if a `z` is present in the answer, it must be the first letter, so find the first `z` in every string, and delete the prefix of each string up to and including that `z`. Repeat until some string doesn't contain a `z`. Repeat this process again with `y`, and loop through the letters in reverse alphabetic order.

**Time Complexity:** $`O(26N)`$, where $`N`$ is the total length of all the strings.