import java.util.ArrayList;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class acc3p3 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt(), cnt=0;
        String s[] = new String[N];
        int start[] = new int[N], tmp[] = new int[N];
        for(int i=0; i<N; i++)
            s[i] = next();
        for(int i=25; i>=0; ) {
            boolean flag=true;
            for(int j=0; j<s.length; j++) {
                int idx=-1;
                for(int k=start[j]; k<s[j].length(); k++) {
                    if ((char) (i + 'a') == s[j].charAt(k)) {
                        idx = k;
                        break;
                    }
                }
                if(idx == -1) {flag=false; break;}
                tmp[j] = idx+1;
            }
            if(flag) {
                System.out.print((char)(i+'a'));
                cnt++;
                for(int j=0; j<N; j++)
                    start[j] = tmp[j];
            }
            else i--;
        }
        if(cnt==0) System.out.println(-1);
        else System.out.println();
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}