import java.util.HashMap;
import java.util.Scanner;
public class ceoi19pp1 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        HashMap<Integer, Integer> mp = new HashMap<>();
        int h = sc.nextInt(), v = sc.nextInt();
        int y[] = new int[h], x[] = new int[v];
        for(int i=0; i<h; i++) y[i] = sc.nextInt();
        for(int i=0; i<v; i++) x[i] = sc.nextInt();
        for(int i=0; i<h; i++) {
            for(int j=i+1; j<h; j++) {
                int diff=y[j]-y[i];
                mp.put(diff, mp.getOrDefault(diff, 0) + 1);
            }
        }
        int ans=0;
        for(int i=0; i<v; i++) {
            for(int j=i+1; j<v; j++) {
                int diff = x[j]-x[i];
                if(mp.containsKey(diff)) ans += mp.get(diff);
            }
        }
        System.out.println(ans);
        sc.close();
    }
}