# Analysis

Since a square must have all equal sides, we can keep track of the frequency of each difference between each of the horizontal lines. Then see which difference of vertical lines
has the same length and add the frequency to our answer. Due to the small input size, this is possible.

**Time Complexity:** $`O(hv)`$