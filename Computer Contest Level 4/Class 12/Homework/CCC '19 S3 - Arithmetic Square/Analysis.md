# Analysis

Subtasks can be solved with logic and implementation, and I will not be talking about the solutions of them here, as the full solution basically is a harder version of the subtasks.

We must first realize that we can place an abritrially value on a square marked with `X`, and calculate the other values after. Thus, we first try to find the center piece, as it
affects the most squares. If the center piece is marked as `X`, set it to $`0`$ and calculate the 4 adajacent squares around it. We can next fill the center of the rows and columns squares
with $`0`$s, if they haven't been marked already, and calculate the rest of the values.

**Time Complexity:** $`O(1)`$