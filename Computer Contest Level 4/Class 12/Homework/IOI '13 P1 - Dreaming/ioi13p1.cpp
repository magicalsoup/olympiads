#include <bits/stdc++.h>
using namespace std;
#define v first
#define w second
#define INF 1L<<60;
#define NINF -INF
#define mp make_pair 
typedef long long ll;
typedef long double ld;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MAXN = 100005;
int dis1[MAXN], dis2[MAXN]; 
bool vis[MAXN], vis2[MAXN]; // vis1 for going down, vis2 for going back up
vector<pii> adj[MAXN];
vector<int> radii, res;
// bfs from A to B, B to C, B to C is the diameter, from C, move up, try to keep both side the same length, get the center, after that, the larger side is the radius
pii last, last2, par[MAXN];
void bfs1(int u) { // works
  queue<pii> q; q.push(make_pair(u, 0));
  int maxdepth=0; vis[u]=true; last=make_pair(u, 0);
  dis1[u]=0;
  while(!q.empty()) {
    pii u = q.front(); q.pop();
    for(pii e : adj[u.v]) {
     // if(e.v == u.v) continue;
      if(!vis[e.v]) {
        vis[e.v]=true;
        dis1[e.v] = dis1[u.v] + e.w;
        if(dis1[e.v] > maxdepth) {
          maxdepth = dis1[e.v];
          last = e;
        }
        q.push(e);
      }
    }
  }
 //cout<<"maxdepth " <<maxdepth<<endl;
 // cout<<"farthest node "<<last.v<<endl;
}
void bfs2() { // works
  queue<pii> q; q.push(make_pair(last.v, 0));
  int maxdepthrev=0; vis2[last.v]=true; 
  last2 = make_pair(last.v, 0); dis2[last2.v]=0;
  par[last.v]= make_pair(-1, last.w);
  while(!q.empty()) {
    pii u = q.front(); q.pop();
    for(pii e: adj[u.v]) {
      if(!vis2[e.v]) {
        vis2[e.v] = true;
        dis2[e.v] = dis2[u.v] + e.w;
        if(dis2[e.v] > maxdepthrev) {
          maxdepthrev = dis2[e.v];
          last2 = e;
        }
        par[e.v] = make_pair(u.v, e.w);
        if(e.v == last.v) return; 
        q.push(e);
      }
    }
  }
  // cout<<"maxdepthrev " <<maxdepthrev<<endl;
  // cout<<"farthest node "<<last2.v<<endl;
}
int getRadius(int u, int N) {
  bfs1(u); bfs2();
 // cout<<"starting node "<<last2.v<<endl;
  int radius=INT_MAX, left=0, right=dis2[last2.v];
  res.push_back(right); int dia=right;
  radius=min(radius, max(left, right));
  for(pii i=par[last2.v]; i.v!=-1; i=par[i.v]) {
    //cout<<"edge2 "<<i.v<<endl;
    if(par[i.v].v == i.v) break;
    left += i.w; right -= i.w;
   // cout<<"left "<<left<<" right "<<right<<endl;
    radius=min(radius, max(left, right));
  }
  //cout<<"diameter "<<diameter<<" radius "<< radius<<endl;
  if(radius == INT_MAX) return dia;
  return radius;
}
int getDiameter() {
  int dia=0;
  for(int i : res)
    dia = max(dia, i);
  return dia;  
}
int travelTime(int N, int M, int L, int A[], int B[], int T[]) {
	for(int i=0; i<M; i++) {
		int u = A[i], v = B[i], w = T[i];
		adj[u].push_back(mp(v, w));
		adj[v].push_back(mp(u, w));
	}
	for(int e=0; e<N; e++)  {
		if(vis[e]) continue;
		radii.push_back(getRadius(e, N));
	}
	int diameter = getDiameter();
	sort(radii.begin(), radii.end(), greater<int>());
  //for(int i : radii)
    //cout<<"radius "<<i<<endl;
	if(radii.size() > 1) diameter = max(diameter, radii[0] + radii[1] + L);
	if(radii.size() > 2) diameter = max(diameter, radii[1] + radii[2] + L + L);
	return diameter;	
}
int main() {
  int N = 12, M = 8, L = 2;
  int A[8] = {0, 8, 2, 5, 5, 1, 1, 10};
  int B[8] = {8, 2, 7, 11, 1, 3, 9, 6};
  int T[8] = {4, 2, 4, 3, 7, 1, 5, 3};
  cout<<travelTime(N, M, L, A, B, T)<<endl;
}