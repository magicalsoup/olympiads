# Analysis

We must notice that each component, or the whole graph is a tree. ALso, the longest distance between any node is at most the radius of the tree + $`2L`$. Thus we can
get the radius of each component, using 2 BFS. Then we can store the radii and sort them in increasing order. There are 2 cases:

- If there is 1 radius, the answer is the maximum diameter of the components
- If there is more than $`1`$ radii, the answer is $`\max(\text{diameter }, radii[0] + radii[1] + L)`$
- If there is more than $`2`$ radii, the answer is $`\max(\text{diameter }, radii[1] + radii[2] + L + L)`$

Finally, return the answer.

**Time Complexity:** $`O(V + E + V \log V)`$, where $`V`$ is the total number vertices and $`E`$ is the total number of edges.