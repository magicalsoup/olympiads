import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class dmopc18c1p4 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt(); long M = readLong(); long K = readLong(); int X = readInt();
        pair f[] = new pair[N+1]; long psa[] = new long[N+1]; int ans[] = new int[N+1];
        for(int i=1; i<=N; i++)
            f[i] = new pair(readInt(), i);
        Arrays.sort(f, 1, N+1);
        for(int i=1; i<=N; i++) {
            psa[i] = f[i].val + psa[i - 1];
        }
        int lft=0, rit=N;
        while(psa[lft] < K) lft++;
        while(psa[N] - psa[rit] < K) rit--;
        if(N - rit > X || X > lft) {
            System.out.println(-1);
            return;
        }
        int cnt=0;
        for(int i=X; i<=N; i++) {
            long t = psa[i] - psa[i-X];
            if(t >= K) {
                for(int j=i-X+1; j<=i; j++) ans[f[j].id] = ++cnt;
                for(int j=1; j<=N; j++) {
                    if(ans[j] == 0) System.out.print(++cnt + " ");
                    else System.out.print(ans[j] + " ");
                }
                return;
            }
        }
        System.out.println(-1);
    }
    static class pair implements Comparable<pair>{
        long val; int id;
        public pair(long val, int id) {
            this.val=val;
            this.id=id;
        }
        @Override
        public int compareTo(pair o) {
            if(o.val == val) return Integer.compare(id, o.id);
            return Long.compare(val, o.val);
        }
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static long readLong() throws IOException {
        return Long.parseLong(next());
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}