## Anaylsis
This problem can be solved in numerous ways.

### Sparse Table
Simply calculate/preprocess a sparse table of maximum and minimum values, and query them later.

**Time Complexity:** $`O(N \log N + Q\log N)`$

### Segment Tree
Use a segtree and find maximum and minium values.

**Time Complexity:** $`O(N \log N + Q \log N)`$
