import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class ncco3d2p1 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    static int tree1[], tree2[], A[];
    public static void main(String[]args) throws IOException{
        int N = readInt(), Q = readInt();
        int exp = (int)(Math.ceil(Math.log(N)/Math.log(2))), sz = 2*(1<<exp)+1;
        A = new int[N+1]; tree1 = new int[sz]; tree2 = new int[sz];
        for(int i=0; i<N; i++)
            A[i] = readInt();
        build1(1, 0, N-1);
        build2(1, 0, N-1);
        for(int t=1; t<=Q; t++) {
            int a = readInt()-1, b = readInt()-1;
            int max = max(N, a, b), min = min(N, a, b);
            //System.out.println(max + " " + min);
            int range = max - min;
            System.out.println(range);
        }
    }
    static void build1(int idx, int start, int end) {
        // leaf node will have a single element
        if(start == end) tree1[idx] = A[start];
        else {
            int mid = (start + end) / 2;
            // recurse on left child
            build1(2*idx, start, mid);
            // recurse on right child
            build1(2*idx+1, mid+1, end);
            // Internal node will have the sum of both its children
            tree1[idx] = Math.min(tree1[2*idx], tree1[2*idx+1]);
        }
    }
    static void build2(int idx, int start, int end) {
        // leaf node will have a single element
        if(start == end) tree2[idx] = A[start];
        else {
            int mid = (start + end) / 2;
            // recurse on left child
            build2(2*idx, start, mid);
            // recurse on right child
            build2(2*idx+1, mid+1, end);
            // Internal node will have the sum of both its children
            tree2[idx] = Math.max(tree2[2*idx], tree2[2*idx+1]);
        }
    }
    static int min(int n, int start, int end) {
        return min(0, n-1, start, end, 1);
    }
    static int min(int st, int ed, int l, int r, int idx) {
        // if segment of this node is part of given range, then return the min of this element
        if(l <= st && ed <= r) return tree1[idx];
        // If segment of this node is outside the given range
        if(ed < l || r < st) return Integer.MAX_VALUE;
        // If a part of segment overlaps with the given range
        int mid = (st + ed) / 2;
        return Math.min(min(st, mid, l, r, 2*idx), min(mid+1, ed, l, r, 2*idx+1));
    }
    static int max(int n, int start, int end) {
        return max(0, n-1, start, end, 1);
    }
    static int max(int st, int ed, int l, int r, int idx) {
        // if segment of this node is part of given range, then return the min of this element
        if(l <= st && ed <= r) return tree2[idx];
        // If segment of this node is outside the given range
        if(ed < l || r < st) return Integer.MIN_VALUE;
        // If a part of segment overlaps with the given range
        int mid = (st + ed) / 2;
        return Math.max(max(st, mid, l, r, 2*idx), max(mid+1, ed, l, r, 2*idx+1));
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}