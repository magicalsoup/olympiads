#include <bits/stdc++.h>
using namespace std;

int N, Q; int sparse[20][50000], sparse2[20][50000], a[50000];
void build() {
	for(int i=1; i<=N; i++) 
		sparse[0][i] = sparse2[0][i]= a[i]; 
	for(int i=1; i<18; i++) {
	    for(int j=1; j<=N; j++) {
			sparse[i][j] = min(sparse[i-1][j], sparse[i-1][j+(1<<(i-1))]);
			sparse2[i][j] = max(sparse2[i-1][j], sparse2[i-1][j+(1<<(i-1))]);
		}
	}
}
int main() {
	scanf("%d %d", &N, &Q);
	for(int i=1; i<=N; i++)
		scanf("%d", &a[i]);
	build();
	for(int i=1, a, b; i<=Q; i++) {
		scanf("%d %d", &a, &b);
		int K = log2(b - a + 1);
		int mn = min(sparse[K][a], sparse[K][b - (1<<K) + 1]);
		int mx = max(sparse2[K][a], sparse2[K][b - (1<<K) + 1]);
		int ans = mx - mn;
		printf("%d\n", ans);
	}	
}