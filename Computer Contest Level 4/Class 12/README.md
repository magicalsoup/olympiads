# Graph Theory!

## How to find diameter of a tree
- BFS twice, from A to B, then B to C, the distance from B to C is the diameter
- Use parent array

## Radius Of A tree
- Find diameter, then move up from point C using parent array, the radius is the minimum value of the maximums between the right and left halfs while moving up the diameter
- BFS works if the graph is a tree

## Center Of A Tree
- The center is when the graph's left or right side is the radius.

