import java.util.*;
public class coci16c1p3 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        String str[] = new String[N+1];
        int a[] = new int[N+1];
        int indegree[] = new int[26];
        for(int i=1; i<=N; i++)
            str[i] = sc.next();
        for(int i=1; i<=N; i++)
            a[i] = sc.nextInt();

        ArrayList<ArrayList<Integer>> adj = new ArrayList<ArrayList<Integer>>();
        for(int i=0; i<26; i++) adj.add(new ArrayList<Integer>());
        for(int i=1; i<N; i++) {
            int cur = a[i], len = str[cur].length();
            int nxt = a[i+1], len2 = str[nxt].length();
            for(int k=0; k<Math.min(len, len2); k++) {
                if (str[cur].charAt(k) != str[nxt].charAt(k)) {
                    adj.get(str[cur].charAt(k) - 'a').add(str[nxt].charAt(k) - 'a');
                    indegree[str[nxt].charAt(k) - 'a']++;
                    break;
                }
                else {
                    if(k == Math.min(len, len2)-1 && len2 < len) {
                        System.out.println("NE");
                        return;
                    }
                }
            }
        }
        toposort(indegree, N, adj, str);
        sc.close();
    }
    static void toposort(int indegree[], int N, ArrayList<ArrayList<Integer>> adj, String str[]) {
        int cnt=0;
        Queue<Integer> q = new LinkedList<>();
        int top_num[] = new int[26];
        for(int v=0; v<26; v++)
            if(indegree[v] == 0)
                q.add(v);
        while(!q.isEmpty()) {
            int v = q.poll();
            top_num[v] = cnt++;
            for(int w : adj.get(v)) {
                if(--indegree[w] == 0)
                    q.add(w);
            }
        }
        if(cnt != 26) {
            System.out.println("NE");
        }
        else {
            String key="";
            for(int i=0; i<26; i++)
                key += (char)(top_num[i] + 'a')+"";

            System.out.println("DA");
            System.out.println(key);
        }
    }
    static void encrypt(String str[], String encryption) {
        String array[] = new String[str.length-1];
        for(int i=0; i<str.length-1; i++) {
            String res="";
            for(int j=0; j<str[i+1].length(); j++) {
                res += encryption.charAt(str[i+1].charAt(j)-'a');
            }
            array[i] = res;
        }
        String copy[] = array;
        Arrays.sort(copy);
        for(String i: copy)
            System.out.println(i);

        System.out.println();
        for(String i: array)
            System.out.println(i);
    }
}