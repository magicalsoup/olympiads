# Analysis

For this problem, we can use topological sort. The key point is that, when listed in order, if $`a[i]`$ has a character that is different than a[i+1], this means that $`a[i]`$'s character
must go before $`a[i+1]`$'s character. Which we can now put in the form of a direct edge, and thus, a DAG (direct acyclic graph) is created. Then, we can topological sort, and print out
the alphabet in the order of the topological sort, and that would be our answer. Now there is 2 ways that its not possible to find a encryption key. If the graph has a cycle,
then its not possible. The second case is when $`a[i]`$ is a prefix of $`a[i+1]`$, and $`a[i+1]`$ goes before $`a[i]`$, then this is also not possible. This can be proven given a simple case
such as 

```
2
a
ab
2 1
```

In this case, there is no encrpytion key that allows `ab` to go before `a`.

**Time Complexity:** $`O(V + E)`$, or  $`O(26 + 26^2)`$ (at the most).