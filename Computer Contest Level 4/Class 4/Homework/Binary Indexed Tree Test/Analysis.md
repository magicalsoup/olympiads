# Analysis

This problem is simply implementing a binary indexed tree, or BIT. In short there are several functions to a binary indexed tree, which I explained another folder. 

For the `Q` operation, we just need to perform another BIT on the frequency of each number.
Thus, by summing $`BIT2[1] + BIT2[2] + \cdots + BIT2[V]`$, where $`BIT2[i]`$ represents the number of times $`i`$ is in the array, we get the answer for numbers that are smaller or equal to $`v`$ in the array.
The other operatiosn are standard BIT operations. A key part of BIT is that when updating, it `adds`, so if we are going to change $`4`$ to $`6`$, we call `update(4, 2, BIT)`, instead of `update(4, 6, BIT)`, since we are not adding $`6`$, we are adding $`2`$ to get from $`4`$ to $`6`$. Similarily, when making numbers a smaller we have to add a negative number, so to change a number from $`5`$ to $`3`$, we call `update(5, -2, BIT)`. To get the sum of a range, we call our BIT similarily to a Prefix sum array, so we call `query(r, l-1, BIT)` to get our answer, if $`[l, r]`$ is our range.

**Time Complexity:** $`O(M \log N)`$

