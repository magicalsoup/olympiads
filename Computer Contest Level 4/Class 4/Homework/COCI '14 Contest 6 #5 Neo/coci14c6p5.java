import java.io.*;
import java.util.Stack;
public class coci14c6p5 {
    static Reader br = new Reader(System.in);
    public static void main(String[]args) throws IOException{
        int R = br.readInt(), S = br.readInt();
        int a[][] = new int[R+1][S+1];
        for(int i=0; i<R; i++)
            for(int j=0; j<S; j++)
                a[i][j] = br.readInt();
        int ans=0;
        int heights[] = new int[S];
        for(int i=0; i<R-1; i++) {
            for(int j=0; j<S-1; j++){
                if(a[i][j] + a[i+1][j+1] <= a[i][j+1] + a[i+1][j])
                    heights[j]++;
                else
                    heights[j]=0;
            }
            ans = Math.max(ans, biggestRectangle(heights));
        }
        System.out.println(ans);
    }
    static int biggestRectangle(int heights[]) {
        int max_area = 0;
        int idx = 0;
        int N = heights.length;
        Stack<Integer> st = new Stack<>();
        while (idx < N) {
            if (st.isEmpty() || heights[idx] >= heights[st.peek()]) {
                st.push(idx);
                idx++;
            } else {
                int pop_idx = st.pop();
                int w = st.isEmpty() ? idx+1 : idx - st.peek();
                int h = heights[pop_idx]+1;
                int area = h*w;
                max_area = Math.max(max_area, area);
            }
        }
        while (!st.isEmpty()) {
            int pop_idx = st.pop();
            int w = st.isEmpty() ? idx+1 : idx - st.peek();
            int h = heights[pop_idx]+1;
            int area = w*h;
            max_area = Math.max(max_area, area);
        }
        return max_area;
    }
    static class Reader {
        final private static int BUFFER_SIZE = 1 << 16;
        private static DataInputStream din = new DataInputStream(System.in);
        private static byte[] buffer = new byte[BUFFER_SIZE];
        private static int bufferPointer = 0, bytesRead = 0;
        public Reader(InputStream stream) {
            din = new DataInputStream(stream);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }
        public Reader(String fileName) throws IOException {
            din = new DataInputStream(new FileInputStream(fileName));
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }
        public  String readLine() throws IOException {
            byte[] buf = new byte[64]; // line length
            int cnt = 0, c;
            while ((c = Read()) != -1) {
                if (c == '\n')
                    break;
                buf[cnt++] = (byte) c;
            }
            return new String(buf, 0, cnt);
        }
        public  String read() throws IOException{
            byte[] ret = new byte[1024];
            int idx = 0;
            byte c = Read();
            while (c <= ' ') {
                c = Read();
            }
            do {
                ret[idx++] = c;
                c = Read();
            } while (c != -1 && c != ' ' && c != '\n' && c != '\r');
            return new String(ret, 0, idx);
        }
        public int readInt() throws IOException {
            int ret = 0;
            byte c = Read();
            while (c <= ' ')
                c = Read();
            boolean neg = (c == '-');
            if (neg)
                c = Read();
            do {
                ret = ret * 10 + c - '0';
            } while ((c = Read()) >= '0' && c <= '9');

            if (neg)
                return -ret;
            return ret;
        }
        public  long readLong() throws IOException {
            long ret = 0;
            byte c = Read();
            while (c <= ' ')
                c = Read();
            boolean neg = (c == '-');
            if (neg)
                c = Read();
            do {
                ret = ret * 10 + c - '0';
            } while ((c = Read()) >= '0' && c <= '9');
            if (neg)
                return -ret;
            return ret;
        }
        public  double readDouble() throws IOException {
            double ret = 0, div = 1;
            byte c = Read();
            while (c <= ' ')
                c = Read();
            boolean neg = (c == '-');
            if (neg)
                c = Read();

            do {
                ret = ret * 10 + c - '0';
            } while ((c = Read()) >= '0' && c <= '9');

            if (c == '.') {
                while ((c = Read()) >= '0' && c <= '9') {
                    ret += (c - '0') / (div *= 10);
                }
            }

            if (neg)
                return -ret;
            return ret;
        }
        private  void fillBuffer() throws IOException {
            bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
            if (bytesRead == -1)
                buffer[0] = -1;
        }
        private  byte Read() throws IOException {
            if (bufferPointer == bytesRead)
                fillBuffer();
            return buffer[bufferPointer++];
        }
        public void close() throws IOException {
            if (din == null)
                return;
            din.close();
        }
    }
}