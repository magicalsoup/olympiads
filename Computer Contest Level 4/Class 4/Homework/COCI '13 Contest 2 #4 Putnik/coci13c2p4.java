import java.io.InputStreamReader;
import java.util.Scanner;
import java.io.BufferedReader;
import java.util.StringTokenizer;
import java.io.IOException;
public class coci13c2p4 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt();
        int adj[][] = new int[N+2][N+2];
        for(int i=1; i<=N; i++) {
            for(int j=1; j<=N; j++) {
                adj[i][j] = readInt();
            }
        }
        int dp[][] = new int[N+2][N+2];
        for(int i=1; i<=N; i++)
            for(int j=1; j<=N; j++)
                dp[i][j] = 0x3f3f3f3f;

        dp[1][1]=0;
        for(int left=1; left<=N; left++) {
            for(int right=1; right<=N; right++) {
                int curCity = Math.max(left, right) + 1;
                dp[left][curCity] = Math.min(dp[left][curCity], dp[left][right] + adj[right][curCity]);
                dp[curCity][right] = Math.min(dp[curCity][right], dp[left][right] + adj[left][curCity]);
            }
        }
        int ans=0x3f3f3f3f;
        for(int i=1; i<=N; i++) {
            ans = Math.min(ans, Math.min(dp[i][N], dp[N][i]));
        }
        System.out.println(ans);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static void print_dp(int dp[][]) {
        for(int i=1; i<dp.length-1; i++) {
            for (int j = 1; j < dp.length-1; j++)
                System.out.print(dp[i][j]==0x3f3f3f3f? "I" + " " : dp[i][j] + " ");
            System.out.println();
        }
    }
}