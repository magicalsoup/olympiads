# Analysis

So we are given apparently a NP-hard problem, so what is the catch here? The catch is obiviously the restriction, and we can use dynamic programming to solve it. Say we put city number 
$`1`$ in our current visited city list (not saying we have to visit city $`1`$ first), we can place city $`2`$ either on the right or left side of city one, as it followes the rule.
When we move on to city $`3`$, we can then put it on the right or left side of any of the variations we had until now.

```math
1 \rightarrow 2 \rightarrow 3 \\

2 \rightarrow 1 \rightarrow 3 \\

3 \rightarrow 1 \rightarrow 2 \\

3 \rightarrow 2 \rightarrow 1 \\
```

The key point here is that we can place the city either on the most left or right of our current path each time. That means, at either end of the path, if we want to place 
city $`i`$, on one of the side of the route is city $`i-1`$. Now we can build our dp transition.

let $`dp[1][1] = 0`$, as there is no cost in the path $`1`$.

lets call the current city the maximum between the left and right index, or the city that we want to place, and since $`i-1`$ is guranteed to be either one of the city either on the 
left or right, we can get the maximum of the 2, which gives us $`i-1`$, and we can add $`1`$ to it to get us the current city, the transition is as follows. 

```cpp
int curCity = max(left, right) + 1;
dp[left][curCity] = min(dp[left][curCity], dp[left][right] + adj[right][curCity]);
dp[curCity][right] = min(dp[curCity)[right], dp[left][right + adj[left][curCity]);
```

The transition is basically placing a city on the left, and then adding the cost of the route, and finding the minimum. The process is repeated by placing a city on the right.

To get the answer, we will have to loop through all dp values that go from the $`c_i`$ (city $`i`$) to the end, and vice versa.

```py
for city in range(N):
    ans = max(ans, min(dp[i][N], dp[N][i]))
```

Total complexity, given the dp size and transtion is $`O(N^2)`$

**Time Complexity:** $`O(N^2)`$