import java.util.Scanner;
public class mwc15c3p3 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), Q = sc.nextInt();
        char g[][] = new char[N+1][N+1];
        for(int i=1; i<=N; i++) {
            for (int j=1; j<=N; j++) {
                g[i][j] = sc.next().charAt(0);
            }
        }
        for(int i=0; i<Q; i++) {
            String word = sc.next();
            boolean flag = findWord(g, N, word);
            System.out.println(flag? "good puzzle!" : "bad puzzle!");
        }
        sc.close();
    }
    static boolean findWord(char g[][], int N, String word) {
        for(int i=1; i<=N; i++) {
            for(int j=1; j<=N; j++) {
                if(word.charAt(0) == g[i][j]) {
                    boolean found = find(i, j, g, 0, word);
                    if(found) return true;
                }
            }
        }
        return false;
    }
    static boolean find(int r, int c, char g[][], int lev, String word) {
        int len = word.length();
        if(len == lev) return true;
        if(r < 1 || c < 1 || r >= g.length|| c >= g.length) return false;
        else if(g[r][c] == word.charAt(lev)) {
            char temp = g[r][c]; g[r][c] = '#';
            boolean flag = find(r+1, c,  g, lev + 1, word) |
                    find(r-1, c, g, lev + 1, word) |
                    find(r, c+1, g, lev+1, word) |
                    find(r, c-1, g, lev+1, word) |
                    find(r+1, c+1, g, lev+1, word) |
                    find(r-1, c-1, g, lev+1, word) |
                    find(r+1, c-1, g, lev+1, word) |
                    find(r-1, c+1, g, lev+1, word);
            g[r][c] = temp;
            return flag;
        }
        return false;
    }
}
