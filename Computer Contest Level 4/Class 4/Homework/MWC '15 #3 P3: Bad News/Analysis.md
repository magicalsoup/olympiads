# Analysis

This is very similar to the question Boggle from ECOO. This a purely implementation + recursion problem. Given a grid, all we need to do is see if we can find the word, starting
at a letter and going in all 8 directions. 

The hard part is just the recurion part in finding the word (no duh).

To do that, it is very simple. We can use dfs or bfs to easily find the word. Even simpler might be just writing pure recursion code. I'll explain a recursion solution here:
for our parameters, we will keep 5 values:

```cpp
bool find(int r, int c, char g[][], int lev, string word);
```

$`r`$ and $`c`$ represent the current position in the grid, $`g`$ is the grid, $`lev`$ is the "depth" that we go into the recursion tree, basically the length of our searach, and $`word`$
is the word that we need to find.

The base case is that if $`\text{lev} = len(\text{word})`$, then we have found our word, this would be explained very soon.
Then we check if the current $`r`$ and $`c`$ are within bounds of the grid, as we will be using them, if the are outside the grid, that means the square is not part of the grid, and we should
stop searching further, and we should return false or end this recursion branch.

Then we check if $`g[r][c]`$ is equal to the $`word[lev]`$. If this is true, this means we are going down the correct path of words/or maybe not, and we should keep searching from this word.
we then mark the $`g[r][c]`$ as visited, very important if we want to stop infinite recursion loops. and do recursion on the 8 adjacent squares beside the current square we are on,
and add $`1`$ to the current lev. Then we reset the square, as we are done searching and we won't have loops, and return if any of the adajcent squares are true, then true, otherwise false.

And after all of that, we need to return false since $`g[r][c] != word[lev]`$, that means we shouldn't keep searching in the branch of letters anymore.

The total complexity of the recursion + finding the word is $`O(N^4)`$, and we take $`O(Q)`$ to answer all the queries.

**Time Complexity:** $`O(N^4 \times Q)`$

