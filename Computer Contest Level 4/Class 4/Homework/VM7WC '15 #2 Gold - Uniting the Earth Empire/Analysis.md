# Analysis

This is literally the same problem as [COCI Task Patrik](https://dmoj.ca/problem/coi06p1), so I will not be giving a detailed explanation here. (I have in my explanation of Patrik)
The solution is to use a mono deque, and solve subproblems of people when adding a person into the deque, more specfically, when we pop, that means this person cannot see anyone anymore
and we will just add the number of people he could see to the answer, and if we meet people that are the same height, we modifiy it a little as they can still potientally see more people.

**Time Complexity:** $`O(N)`$