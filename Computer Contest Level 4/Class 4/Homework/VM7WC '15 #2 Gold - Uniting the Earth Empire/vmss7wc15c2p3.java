import java.util.Scanner;
import java.util.Stack;

public class vmss7wc15c2p3 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        Stack<pair> st = new Stack<>();
        long a[] = new long[N+1];
        for(int i=1; i<=N; i++)
            a[i] = sc.nextLong();
        long ans=0;
        for(int i=1; i<=N; i++) {
            while(!st.isEmpty() && st.peek().height < a[i])
                ans += st.pop().precede;
            int cnt=0;
            while(!st.isEmpty() && st.peek().height == a[i]) {
                ans += st.peek().precede;
                cnt = st.peek().precede;
                st.pop();
            }
            if(!st.isEmpty()) ans++;
            st.push(new pair(a[i], cnt + 1));
        }
        System.out.println(ans);
        sc.close();
    }
    static class pair {
        long height; int precede;
        public pair(long height, int precede) {
            this.height=height;
            this.precede=precede;
        }
    }
}