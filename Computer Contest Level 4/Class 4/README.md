# Class 4

- Topological sort? 
- Dynamic Programming

## Topological Sort

- Basically like BFS
- A topological sort is an ordering of vertices in a directed acyclic graph, such that if there is a path from $`v_i`$ to $`v_j`$, then $`v_j`$ appears after $`v_i`$ in the ordering.
- Each vertex in the graph has an indegree, or how many incoming edges this vertex has.
- If the number of vertex in the sorted list does not equal the number of vertices in the graph, this means there is a cycle.

### Algorithm
- Add any vertex that doesn't have any incoming edges
- Then for each vertex that that this vertex is connected to, we can remove that connection or edge, and check again to see how many verticess with indegree of 0
- Repeat this process until all vertexes have been processed

```cpp
vector<int> adj[MAXN];
int indegree[MAXN];
void toposort() {
    queue<int> q;
    int cnt=0; // keep count of number of vertices
    int order[MAXN]l
    for(int v=0; v<NUM_OF_VERTICES; v++)
        if(indegree[v] == 0)  // if this vertex has no incoming edges
            q.push(v); // push it into the queue
            
    while(!q.empty()) { 
        int cur = q.front(); // get the current vertex and pop from queue
        q.pop(); 
        order[cur] = cnt++; // map the order of the vertex, and add one to the total cnt of vertices
        for(int e: adj[cur]) // for every vertex connected to this vertex
            if(--indegree[e] == 0) // if erasing that connection/edge leaves them with no incoming edges
                q.push(e); // add it into the queue
    }
    if(cnt != NUM_OF_VERTICES) // if there is a cycle
        cout<<"There is a cycle"<<endl;
    else {
        for(int i=0; i<NUM_OF_VERTICES; i++)
            cout<<order[i]<<endl;
    }
}
```