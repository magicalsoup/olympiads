# Computer Contest Level 4

## Topics:
1. Data structure
    - Prefix Sum / Difference Array (Multi-dimensional)
    - Stack (Mono Stack)
    - Deque (Mono Deque)
    - Binary Tree
    - Binary Index Tree 
    - Segment tree
    - Sparse Table