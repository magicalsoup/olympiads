import java.util.*;
public class oly19practice28 {
    static ArrayList<ArrayList<Edge>> adj = new ArrayList<ArrayList<Edge>>();
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), M = sc.nextInt(), L = sc.nextInt();
        for(int i=0; i<=N; i++) adj.add(new ArrayList<>());
        for(int i=0; i<M; i++) {
            int u = sc.nextInt(), v = sc.nextInt(), w = sc.nextInt();
            adj.get(u).add(new Edge(v, w));
            adj.get(v).add(new Edge(u, w));
        }
        int ans = bs(0, 1000000000, N, L);
        System.out.println(ans == 0x3f3f3f3f? -1 : ans);
        sc.close();
    }
    static int bs(int lo, int hi, int N, int L) {
        int ans=0x3f3f3f3f;
        while(lo <= hi) {
            int mid = lo + (hi-lo)/2;
            if(check(mid, L, N)) {
                ans = Math.min(ans, mid);
                hi = mid - 1;
            }
            else lo = mid + 1;
        }
        return ans;
    }
    static boolean check(int mid, int L, int N) {
        PriorityQueue<Edge> pq = new PriorityQueue<>();
        pq.add(new Edge(1, 0));
        int dis[] = new int[N+1];
        Arrays.fill(dis, 0x3f3f3f3f);
        dis[1] = 0;
        while(!pq.isEmpty()) {
            Edge cur = pq.poll();
            int weight = cur.w <= mid? 0 : 1;
            if(weight > dis[cur.v]) continue;
            for(Edge e : adj.get(cur.v)) {
                int w2 = e.w <= mid? 0 : 1;
                if(dis[e.v] > dis[cur.v] + w2) {
                    dis[e.v] = dis[cur.v] + w2;
                    pq.add(new Edge(e.v, dis[e.v]));
                }
            }
        }
        return dis[N] <= L;
    }
    static class Edge implements Comparable<Edge>{
        int v, w;
        public Edge(int v, int w){
            this.v=v;
            this.w=w;
        }
        @Override
        public int compareTo(Edge other) {
            if (w == other.w)
                return v - other.v;
            return w - other.w;
        }
    }
}
