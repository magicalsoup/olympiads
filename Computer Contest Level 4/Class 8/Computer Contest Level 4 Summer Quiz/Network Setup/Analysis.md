# Analysis

We can simply binary search for the answer, we can search for the longest remaining cable. 

To check if it works, we just need make all edges that are less than or equal to the binary searched value $`0`$, other wise $`1`$. Then we just need to see if the path from 
$`1`$ to $`N`$ has a cost of $`L`$ or less, if yes, then this is possible, and we will go smaller, if not, we increase the length of the longest remaining cable. We can use a simple
single source shortest path algorithm to find out the total cost from $`1`$ to $`N`$.

**Time Complexity:** $`O(M \log N \times \log N ) \rightarrow O(M \log(N)^2)`$