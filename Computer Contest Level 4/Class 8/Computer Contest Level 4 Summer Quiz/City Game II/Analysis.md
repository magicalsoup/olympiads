# Analysis

Lets first try to solve the easier problem first, given a 1D array of $`N`$ numbers, what is the maximum/minimum in each interval of $`K`$ numbers? We see that this can be easily
solved with min deque, now lets get back to the harder problem.

We can precompute and run min/max queue horizontally and vertically, then the answer is simply $`\min(ans, maxH[r][c] - minH[r][c])`$, where $`N \le r \le H`$ and $`N \le c \le W`$.

**Time Complexity:** $`O(H \times W)`$.