import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class oly19practice37 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int H = readInt(), W = readInt(), N = readInt();
        int h[][] = new int[H+1][W+1];
        int maxH[][] = new int[H+1][W+1];
        int minH[][] = new int[H+1][W+1];
        for(int i=1; i<=H; i++)
            for(int j=1; j<=W; j++)
                h[i][j] = readInt();
        precompute(N, H, W, h, maxH, minH);
        int ans=2*0x3f3f3f3f;
        for(int i=N; i<=H; i++) {
            for(int j=N; j<=W; j++) {
                ans = Math.min(ans, maxH[i][j] - minH[i][j]);
            }
        }
        System.out.println(ans);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static void precompute(int N, int H, int W, int h[][], int maxH[][], int minH[][]) {
        // do min/max deque horizontally
        for(int i=1; i<=H; i++) {
            Deque<pair> dq1 = new ArrayDeque<>();
            Deque<pair> dq2 = new ArrayDeque<>();
            for(int r=1; r<=W; r++) {
                while(!dq1.isEmpty() && dq1.peekLast().val >= h[i][r])
                    dq1.pollLast();
                while(!dq2.isEmpty() && dq2.peekLast().val <= h[i][r])
                    dq2.pollLast();
                dq1.add(new pair(r, h[i][r]));
                dq2.add(new pair(r, h[i][r]));
                if(dq1.peekFirst().idx <= r - N) dq1.pollFirst();
                if(dq2.peekFirst().idx <= r - N) dq2.pollFirst();
                minH[i][r] = dq1.peekFirst().val;
                maxH[i][r] = dq2.peekFirst().val;
            }
        }
        for(int j=1; j<=W; j++) {
            Deque<pair> dq1 = new ArrayDeque<>();
            Deque<pair> dq2 = new ArrayDeque<>();
            for (int r=1; r<=H; r++) {
                while (!dq1.isEmpty() && dq1.peekLast().val >= minH[r][j])
                    dq1.pollLast();
                while (!dq2.isEmpty() && dq2.peekLast().val <= maxH[r][j])
                    dq2.pollLast();
                dq1.add(new pair(r, minH[r][j]));
                dq2.add(new pair(r, maxH[r][j]));
                if (dq1.peekFirst().idx <= r - N) dq1.pollFirst();
                if (dq2.peekFirst().idx <= r - N) dq2.pollFirst();
                minH[r][j] = dq1.peekFirst().val;
                maxH[r][j] = dq2.peekFirst().val;
            }
        }
    }
    static class pair {
        int idx, val;
        public pair(int idx, int val){
            this.idx=idx;
            this.val=val;
        }
    }
}
