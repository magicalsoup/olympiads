# Analysis

We can simply read the numbers as a string, and this problem just simplifies to finding the sum of the products of all the possible pairs of digits. We can then just
loop through and brute-force our way to find the answer.

**Time Complexity:** $`O(D_1 \times D_2)`$, where $`D_1`$ is the number of digits in the first number and $`D_2`$ is the number of digits in the second number.