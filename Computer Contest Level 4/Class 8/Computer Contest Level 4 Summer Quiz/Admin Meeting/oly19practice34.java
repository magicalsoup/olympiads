import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
public class oly19practice34 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), M = sc.nextInt(), T = sc.nextInt();
        ArrayList<ArrayList<Edge>> adj = new ArrayList<ArrayList<Edge>>();
        ArrayList<ArrayList<Edge>> adj2 = new ArrayList<ArrayList<Edge>>();
        for(int i=0; i<=N; i++) {
            adj.add(new ArrayList<>());
            adj2.add(new ArrayList<>());
        }
        int dis[] = new int[N+1], dis2[] = new int[N+1];
        for(int i=0; i<M; i++) {
            int u = sc.nextInt(), v = sc.nextInt(), w = sc.nextInt();
            adj.get(u).add(new Edge(v, w));
            adj2.get(v).add(new Edge(u, w));
        }
        dijkstras(T, adj, dis); dijkstras(T, adj2, dis2);
        int ans=0;
        for(int i=1; i<=N; i++) {
            if(dis[i] != 0x3f3f3f3f && dis2[i] != 0x3f3f3f3f)
                ans = Math.max(ans, dis[i] + dis2[i]);
        }
        System.out.println(ans);
        sc.close();
    }
    static void dijkstras(int st, ArrayList<ArrayList<Edge>> adj, int dis[]) {
        Arrays.fill(dis, 0x3f3f3f3f);
        dis[st] = 0;
        PriorityQueue<Edge> pq = new PriorityQueue<>();
        pq.add(new Edge(st, 0));
        while(!pq.isEmpty()) {
            Edge cur = pq.poll();
            if(cur.w > dis[cur.v]) continue;
            for(Edge e: adj.get(cur.v)) {
                if(dis[e.v] > dis[cur.v] + e.w){
                    dis[e.v] = dis[cur.v] + e.w;
                    pq.add(new Edge(e.v, dis[e.v]));
                }
            }
        }
    }
    static class Edge implements Comparable<Edge>{
        int v, w;
        public Edge(int v, int w){
            this.v=v;
            this.w=w;
        }
        @Override
        public int compareTo(Edge other){
            return w - other.w;
        }
    }
}
