# Analysis

This problem is a very classic problem. To begin, the graph is a directed graph, and the value of $`N`$ is quite small.

The naive way would be to dijkstras every single node, resulting in a $`O(N \times E \log V)`$ time complexity. 

However, since it is a directed graph, we only have to perform dijkstras twice. First we will create $`2`$ grahps, the original and a reverse of the original.

Then we dijkstras from $`T`$ twice, one using the original graph and one using the reverse, storing the answers in $`dis1[]`$ and $`dis2[]`$ respectively.

Then we can simply loop through the $`N`$ nodes, and find the maximum of $`dis1[vertex] + dis2[vertex]`$.

**Time Complexity:** $`O(E \log V)`$ (Priority Queue Dijsktras)