# Analysis

The are several ways to approach this problem, I will be talking about the most straightforward way.

Since $`N`$ is quite big, we cannot do a brute-force $`O(N^2)`$ solution.

The solution is to sort the array, and binary search for the farthest indexed champion which the current champion an defeat. This is a $`O(N \log N)`$ solution. However, a lot of
people were stuck on the part on how to calculate the friend part in constant time. 

We can pre-compute an array of $`friends[]`$, were the $`i^{th}`$ index of the $`friends[]`$ array will keep the number of champions that has lower strength value
than the $`i^{th}`$ champion. This way, when calculating the number of champions the current champion can defeat, all we need to do is binary search, then subtract $`friends[i]`$,
where $`i`$ is the current champion.

**Time Complexity:** $`O(N \log N)`$