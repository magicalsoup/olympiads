import java.util.Arrays;
import java.util.Scanner;
public class champions {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(), k = sc.nextInt();
        int friend[] = new int[n];
        int strength[] = new int[n];
        int original[] = new int[n];
        for(int i=0; i<n; i++)
            strength[i] = original[i] = sc.nextInt();

        Arrays.sort(strength);
        for(int i=0; i<k; i++) {
            int a = sc.nextInt()-1, b = sc.nextInt()-1;
            if(original[a] > original[b])
                friend[a]++;
            if(original[b] > original[a])
                friend[b]++;
        }
        for(int i=0; i<n; i++) {
            int idx = lower_bound(0, n, strength, original[i]);
            idx -= friend[i];
            System.out.print(idx + " ");
        }
        sc.close();
    }
    static int lower_bound(int lo, int hi, int a[], int key) {
        while(lo < hi) {
            int mid = lo + (hi - lo) / 2;
            if(key > a[mid])
                lo = mid+1;
            else hi = mid;
        }
        return lo;
    }
}
