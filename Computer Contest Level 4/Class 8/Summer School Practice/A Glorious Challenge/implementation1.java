import java.util.Scanner;
public class implementation1 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a[] = new int[n];
        for(int i=0; i<n; i++)
            a[i] = sc.nextInt();
        int max1=0, min1=0x3f3f3f3f, max2=0, min2=0x3f3f3f3f;
        for(int i=0; i<n; i++) {
            max1 = Math.max(max1, a[i]+i);
            max2 = Math.max(max2, a[i]-i);
            min1 = Math.min(min1, a[i]+i);
            min2 = Math.min(min2, a[i]-i);
        }
        System.out.println(Math.max(max1-min1, max2-min2));
        sc.close();
    }
}
