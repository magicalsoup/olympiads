# Analysis

The given questions asks us to find the maximum value of $`|A_i - A_j| + |i-j|`$,

A simple solution will be to iterating in two for loops and get the answer, but since constraints are too large and time limit is strict, it will not pass.

So, let us try to do this in other way. First of all lets make the four equations by removing the mod , Following four equations will be formed, and we need to find the maximum value of any of these equation,that will be our answer :

- $`A_i - A_j + i - j = (A_i+i) - (A_j+j) \cdots (i)`$
- $`A_i - A_j - i + j = (A_i-i) - (A_j-j) \cdots (ii)`$
- $`-A_i + A_j + i - j = -(A_i-i) + (A_j-j) \cdots (iii)`$
- $`-A_i + A_j - i + j = -(A_i+i) + (A_j+j) \cdots (iv)`$


A simple observation is that the equation $`(i)`$ and $`(iv)`$ are identical, similarly equations $`(ii)`$ and $`(iii)`$ are identical.

Now, how do we find the maximum value of the given equations ? One approach is to form two arrays ,
- $` \text{First Array[]} `$, it will store $`(A_i+i), 0 \le i \lt n`$
- $` \text{Second Array[]}`$, it will store values of $`(A_i-i), 0 \le i \lt n`$

Now, our task is easy we just need to find the maximum difference b/w two values in the above created two arrays.

For that, we find maximum value and minimum value in $` \text{First Array}`$, and store their difference;
- $`ans1 = (\text{maximum value in First Array} - \text{minimum value in First Array});`$

Similarly, we need to find the maximum value and minimum value in $` \text{Second Array} `$, and store their difference;
- $`ans2 = (\text{maximum value in Second Array} - \text{minimum value in Second Array});`$

**Desired Answer** = $`\max(ans1,ans2);`$


Another way to approach this is simply keeping $`4`$ values, and storing the $`4`$ equations

- $`max1 = \max(max1, a[i]+i)`$
- $`max2 = \max(max2, a[i]-i)`$
- $`min1 = \min(min1, a[i]+i)`$
- $`min2 = \min(min2, a[i]-i)`$

Then our answer would be simply $`\max(max1-min1, max2-min2)`$.

**Time Complexity:** $`O(N)`$