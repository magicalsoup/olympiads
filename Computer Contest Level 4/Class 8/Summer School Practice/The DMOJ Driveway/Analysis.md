# Analysis

Ironically, the implementation of this question is to use a a double ended queue, where we can pop from both sides. 

When a command is for someone to be pushed into the driveway, just add that person to the end of the deque. 

When a command is for someone to be popped out of the driveway, check to see if the person is at the end of the deque. If they are, then pop them out. If they're not, check if they
are at the front of deque, and if $`M \ge 1`$, then pop them out of the front. Treat duplicates as another person.

**Time Complexity:** $`O(T)`$