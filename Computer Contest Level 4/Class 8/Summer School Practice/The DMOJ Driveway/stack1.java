import java.util.LinkedList;
import java.util.Scanner;
public class stack1 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt(), M = sc.nextInt();
        LinkedList<String> q = new LinkedList<>();
        while(T-- > 0) {
            String name = sc.next();
            String com = sc.next();
            if(com.equals("in")) q.add(name);
            else {
                if(!q.isEmpty()) {
                    if(q.peekLast().equals(name)) q.pollLast();
                    else if(q.peekFirst().equals(name) && M > 0) {
                        M--;
                        q.pollFirst();
                    }
                }
            }
        }
        for(String i : q)
            System.out.println(i);
        sc.close();
    }
}
