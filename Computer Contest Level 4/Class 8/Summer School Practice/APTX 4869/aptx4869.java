import java.util.Scanner;
import java.util.Stack;
import java.util.TreeMap;
public class aptx4869 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        String s = sc.next();
        System.out.println(countOfAtoms(s));
        sc.close();
    }
    public static String countOfAtoms(String formula) {
        Stack<TreeMap<String, Integer>> stack = new Stack<>();
        TreeMap<String, Integer> curMap = new TreeMap<>();
        int n = formula.length();
        int i = 0;
        char[] c = formula.toCharArray();
        while (i < n) {
            if (c[i] == '(') {
                stack.push(curMap);
                curMap = new TreeMap<>();
                i++;
            }
            else if (c[i] == ')') {
                TreeMap<String, Integer> tmp = curMap;
                curMap = stack.pop(); i++;
                int num = 1;
                String cur="";
                while (i < n && Character.isDigit(c[i])) cur += c[i++];
                if (cur.length() > 0) num = Integer.parseInt(cur);
                for (String atom : tmp.keySet())
                    curMap.put(atom, curMap.getOrDefault(atom, 0) + tmp.get(atom) * num);
            }
            else {
                String cur="";
                cur += c[i++];
                while (i < n && Character.isLowerCase(c[i])) cur += c[i++];
                String atom = cur;
                int num = 1;
                cur="";
                while (i < n && Character.isDigit(c[i])) cur += c[i++];
                if (cur.length() != 0) num = Integer.parseInt(cur);
                curMap.put(atom, curMap.getOrDefault(atom, 0) + num);
            }
        }
        String cur = "";
        for (String atom : curMap.keySet()) {
            cur += atom;
            if (curMap.get(atom) > 1) {
                cur += curMap.get(atom);
            }
        }
        return cur;
    }
}
