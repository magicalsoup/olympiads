import java.util.Scanner;
public class oly18novp12 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), F = sc.nextInt();
        long psa[] = new long[N+1];
        for(int i=1; i<=N; i++) {
            psa[i] = sc.nextInt()*1000;
            psa[i] += psa[i-1];
        }
        System.out.println(bs(0L, psa[N], psa, F));
        sc.close();
    }
    static long bs(long lo, long hi, long psa[], int F) {
        long ans=0;
        while(lo <= hi) {
            long mid =  lo + (hi - lo) / 2;
            if(check(mid, psa, F)) {
                ans = Math.max(ans, mid);
                lo = mid+1;
            }
            else hi = mid-1;
        }
        return ans;
    }
    static boolean check(long mid, long psa[], int F) {
        long tmp[] = new long[psa.length];
        for(int i=0; i<psa.length; i++)
            tmp[i] = psa[i];
        for(int i=1; i<psa.length; i++)
            tmp[i] -= mid * i;
        long min=0x3f3f3f3f3f3fL;
        for(int i=1; i<psa.length; i++) {
            if(i > F) min = Math.min(min, tmp[i-F]);
            if (tmp[i] - min >= 0) return true;
        }
        return tmp[psa.length-1] >= 0;
    }
}
