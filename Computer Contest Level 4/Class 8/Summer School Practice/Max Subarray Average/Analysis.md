# Analysis

We can binary search for the average, then we can check if that average works.

To check that an average works, we need to derive a formula first. $` \frac{sum}{F} = Average`$, then $`sum = Average \times F`$, then $`sum - Average \times F = 0`$.

Therefore, when checking if an average works, we can subtract the average from all the elements in the array, and find if any contigous subsequence of length at least $`F`$ has a sum
greater than $`0`$. We can use a prefix sum array to check in $`O(N)`$ time.

**Time Complexity:** $`O(N \log N)`$