# Analysis

We can simply brute force our way by checking all $`X \times Y + Z`$ numbers, and seeing if any of them work. Proof is left as an exercise to the reader on why the max range is $`X \times Y + Z`$

**Time Complexity:** $`O(X \times Y + Z)`$