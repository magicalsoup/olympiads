import java.util.Scanner;
public class oly19practice29 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int X = sc.nextInt(), Y = sc.nextInt(), Z = sc.nextInt();
        for(int i=1; i <= X*Y+Z; i++) {
            if(i % X == 0 && i % Y == Z) {
                System.out.println("YES");
                return;
            }
        }
        System.out.println("NO");
        sc.close();
    }
}
