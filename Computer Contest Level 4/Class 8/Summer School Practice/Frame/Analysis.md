# Analysis

This is a very straightforwad implementation problem. Simply keep a 2D array, but add padding around the whole 2D array. Then scan in the elements of the picture, but
make sure to store it in the center, not on the padding. Then for each padding, just output the `#` character, else output the array contents.

**Time Complexity:** $`O(HW)`$