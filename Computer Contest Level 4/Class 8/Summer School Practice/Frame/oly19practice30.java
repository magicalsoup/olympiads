import java.util.Scanner;
public class oly19practice30 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int H = sc.nextInt(), W = sc.nextInt();
        char frame[][] = new char[H+3][W+3];
        for(int i=1; i<=H; i++) {
            String s = sc.next();
            for(int j=1; j<=W; j++) {
                frame[i][j] = s.charAt(j-1);
            }
        }
        for(int i=0; i<=H+1; i++) {
            for(int j=0; j<=W+1; j++) {
                if(i == 0 || j == 0 || j == W+1 || i == H+1) System.out.print("#");
                else System.out.print(frame[i][j]);
            }
            System.out.println();
        }
        sc.close();
    }
}
