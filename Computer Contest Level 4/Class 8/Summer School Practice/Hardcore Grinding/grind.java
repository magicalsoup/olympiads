import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class grind {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException {
        int N = readInt();
        int dif[] = new int[10000005];
        for(int i=0; i<N; i++) {
            dif[readInt()]++;
            dif[readInt()]--;
        }
        int max=0;
        for(int i=1; i<dif.length; i++)
            max = Math.max(dif[i] += dif[i-1], max);
        System.out.println(max);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}
