# Analysis

This problem might seem very hard at first, but its pretty simple. The key part is the consecutive alphabet subsequence part, in fact, we just need to check if $`2`$ letters have an
ascii difference of $`1`$.

Then, we can just brute-force, by keeping 2 nested for loops, where $`0 \le i \lt |S|`$, and $`i+1 \le j \lt |S|`$. Then just check if any substring in the form  $`[i, j]`$ matches
the requirements. If it does, output `Sushi is Here!`, else output `Better Luck Next Time.`.

**Time Complexity:** $`O(N^2)`$