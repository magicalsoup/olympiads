# Analysis

This is a straightforward sorting problem. We wil create a class triplet, where it will store the first round score, second round score, and the index of triplet, since we need to 
sort it and we don't want to mix up the indexes.

Then we sort the triplet array by its first round value in descending order. Then we will loop through the first $`K`$ values of the array, and compare the second round value of 
each element and storing the index of the highest score. Finally, output the index.

**Time Complexity:** $`O(N \log N + K)`$