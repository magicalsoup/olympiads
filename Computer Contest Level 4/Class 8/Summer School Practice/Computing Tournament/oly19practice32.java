import java.util.Arrays;
import java.util.Scanner;
public class oly19practice32 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), K = sc.nextInt();
        triplet contestants[] = new triplet[N];
        for(int i=0; i<N; i++)
            contestants[i] = new triplet(sc.nextInt(), sc.nextInt(), i);

        Arrays.sort(contestants);
        int max=0, id=0;
        for(int i=0; i<K; i++) {
            if(contestants[i].s > max){
                max = contestants[i].s;
                id = contestants[i].id;
            }
        }
        System.out.println(id+1);
        sc.close();
    }
    static class triplet implements Comparable<triplet>{
        int f, s, id;
        public triplet(int f, int s, int id){
            this.f=f;
            this.s=s;
            this.id=id;
        }
        @Override
        public int compareTo(triplet other){
            return other.f - f;
        }
    }
}
