# Mono Stack

## Common Problem Largest Rectangle In A Histogram
- Keep an increasing mono stack
- whenever poping an element, find $`L`$ and $`R`$, and calculate area, update answer
- Maintain a stack
- If stack is empty or value at index of stack is less than or equal to value at current index, push this into stack.
- Otherwise keep removing values from stack till value at index at top of stack is less than value at current index.
- While removing value from stack calculate area
    - if stack is empty it means that till this point value just removed has to be smallest element so area = `input[top] * i`
    - if stack is not empty then this value at index top is less than or equal to everything from stack `top + 1` till $`i`$. 
    - So area will `area = input[top] * (i - stack.peek() - 1)`;
- Finally maxArea is area if `area` is greater than `maxArea`.

```java
class Solution {
    public int largestRectangleArea(int[] heights) {
        int max_area=0; // storing max area
        int index=0;
        int N = heights.length;
        Stack<Integer> st = new Stack<>(); // stores the indexes, mono stack
        while(index < N) {
            if(st.isEmpty() || heights[index] >= heights[st.peek()]) {
                st.push(index);
                index++;
            }
            
            else {
                int pop_index = st.pop();
                int w = st.isEmpty()? index : index - st.peek() - 1;
                int h = heights[pop_index];
                int area = w*h;
                max_area = Math.max(area, max_area);
            }
            
        }
        while(!st.isEmpty()) {
            int pop_index = st.pop();
            int w = st.isEmpty()? index : index - st.peek() - 1;
            int h = heights[pop_index];
            int area = w*h;
            max_area = Math.max(area, max_area);
        }   
        return max_area;
    }
}
```