import java.util.Arrays;
import java.util.Scanner;
import java.util.Deque;
import java.util.ArrayDeque;
public class bts17p4 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), M = sc.nextInt(), J = sc.nextInt();
        int a[] = new int[N+1];
        Arrays.fill(a, 0x3f3f3f3f);
        for(int i=1; i<=M; i++) {
            int idx = sc.nextInt();
            int t = sc.nextInt();
            a[idx] = t;
        }
        Deque<pair> dq = new ArrayDeque<pair>();
        dq.add(new pair(0, 0));
        for(int i=1; i<=N; i++) {
            a[i] = Math.max(a[i], dq.peekFirst().val);
            while(!dq.isEmpty() &&  dq.peekLast().val > a[i])
                dq.pollLast();
            dq.add(new pair(i, a[i]));
            if(dq.peekFirst().idx <= i - J) dq.pollFirst();
        }
        System.out.println(dq.peekFirst().val == 0x3f3f3f3f? -1: dq.peekFirst().val);
        sc.close();
    }
    static class pair {
        int idx, val;
        public pair(int idx, int val){
            this.idx=idx;
            this.val=val;
        }
    }
}
