# Analysis

There are many approaches to this problem, which I will list below.
1. Binary Search
2. Segment Tree
3. Mono Deque
4. Dynamic Programming

I will mainly focus on the Mono Deque one, and a bit on the Binary Search.

For binary search, we can search for a time $`t`$, then use a greedy $`O(N)`$ check to see if time $`t`$ works.

**Time Complexity:** $`O(N \log_2(N))`$

For mono deque, we can keep an minimum queue, or an increasing deque. This queue will basically get the minimum between the range $`[J, I]`$. And the last element in the queue will be our answer, the proof is left to the reader as an exercise.
We can first fill the patches of mud that do not dry with infinity. Then we will add $`0`$ into the deque, as mud $`0`$ is where we start, and loop through from $`1`$ to $`N`$. Now we
query the deque, which means getting the maximum between the head element of the deque and the current element. Why? Because we need to know the time so that the patch of mud dries, we cannot simply "jump" to a mud that did not dry yet. Then we push the time into the queue while making sure the queue is increasing.
When the head element is outside the range, pop it. At the very end, when getting your answer, if the minimum time is infinity, output `-1`, else, output the answer.

**Time Complexity:** $`O(N)`$.