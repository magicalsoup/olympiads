import java.util.Scanner;
public class pacnw16c {
    public static void main(String[]args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(), k = sc.nextInt(), r = sc.nextInt();
        boolean camera[] = new boolean[n+1];
        for(int i=0; i<k; i++)
            camera[sc.nextInt()] = true;

        int left=0, right=left+r-1;
        int need=0, curr=0;
        for(int i=0; i<right; i++)
            if(camera[i]) curr++;

        while(left < n && right < n) {
            if(curr < 2) {
                int tmp = right;
                while(curr<2) {
                    while(camera[tmp]) tmp--;
                    camera[tmp]=true;
                    curr++;
                    need++;
                }
            }
            if(camera[left]) curr--;
            if(camera[right+1]) curr++;
            left++;
            right++;
        }
        System.out.println(need);
     }
}
