# Analysis

So, first sight, seems kind of hard to achieve an $`O(N)`$ algorithm, but it is possible, with something similar to difference array and sliding window. We start with the 2 pointers
the $`left`$ and $`right`$ pointers, $`right = r-1`$ and $`left = 0`$ Now we move forward on both indexes by one each time, each time we move the left index, we check if there is a camera at the left index, if there is, subtract one from the current cameras, also check if there is a camera at the right + 1 index, add one to the current cameras.
If current cameras is smaller than 2, start from the right index, and go backwards and fill in the missing cameras. Remember, its optimal to place the cameras in the furthest index in the range. 
This will require you to first initialize current cameras in the range $`[0, r-1]`$, but that will be it.

**Time Complexity:** $`O(2N)`$