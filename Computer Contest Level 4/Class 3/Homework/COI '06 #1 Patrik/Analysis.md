# Analysis

We can start solving subproblems, if someone meets a taller person than them, that means they cannot see anyone past that person. With that being said, we can use a mono stack, keeping a decreasing maximum stack.
We keep 2 values, one for the height of the number of people that this person is taller than. Now this means, whevener we pop a person out of the mono-stack, we add the number of people that the persons we pop onto the answer.
Now lets deal if the persons height is equal to the last height in the stack, if it is, that means the current person can still see more people, so we pop the stack until the height is not equal to the current height, then we add into the stack the current height plus the person that this person can see + 1.

**Time Complexity:** $`O(N)`$