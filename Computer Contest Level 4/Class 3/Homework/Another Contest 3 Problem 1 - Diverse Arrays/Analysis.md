# Analysis
What exactly is a diverse array? How about lets see what makes a non-diverse array: a subarray of size of at least $`1`$ and with at less than $`K`$ distinct integers. In short, there 
is 2 ways to approach this problem
1. Count the number of diverse arrays
2. Count the number of non-diverse arrays, then subtracting it from $`\frac{N \times (N-1)}{2}`$ (the total number of subarrays)



Here, I'll explain the second way. Now lets consider this, if an diverse array from $`[L, R]`$ is valid, then does that mean $`[L, R+1], [L, R+2], \cdots [L, N]`$ are also distinct? Now that we have this, this problem simplifies to finding $`R`$ for every $`L`$, and finding how many subarrays you can make with $`[L, R-1]`$, and subtracting the non-diverse arrays from the total. Now we reach another problem, how do we find the number of distinct numbers between a range $`[L, R]`$ in near constant time?

The solution is to use something similar to the problem [cameras](https://dmoj.ca/problem/pacnw16c) + two-pointer. We keep a frequency list, and a current counter of how many distinct integers are in the range. When we increase the $`R`$ pointer, we check if `freq[a[r]] == 0`, if it is, then that means we haven't add `freq[a[r]]` to the range yet, so we increase $`1`$ to the counter. We add one to the freqeuncy regardless, as the frequnecy array counts how many times the number appeared, not necessarily need to be distinct. Every time we increase $`L`$ pointer, we check if `freq[a[l] == 1`, if it is, we decrease one from the counter, since we have moved out of the range, so the number of distinct integers also will decrease by $`1`$, if `freq[a[l] == 1`. We also decrease one from the `freq[a[l]` regardless, as it counts the number of times a integer appeared, and we moved out of the range as well. 

For the first way, we also apply the same approach, except we update the answer when $`cnt \ge K`$.

**Time Complexity:** $`O(N)`$



