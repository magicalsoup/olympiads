import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
public class acc3p1 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args)throws IOException{
        int N = readInt(), K = readInt(); // read input
        int a[] = new int[N+2];
        for(int i=1; i<=N; i++)
            a[i] = readInt();
        int freq[] = new int[1000010];
        long ans=0;
        int cnt=0;
        for(int l=1, r=1; r<=N; r++) {
            if(freq[a[r]] == 0) {
                freq[a[r]]++;
                cnt++;
            }
            else freq[a[r]]++;
            while(cnt >= K) {
                if(freq[a[l]] == 1) {
                    freq[a[l]]--;
                    cnt--;
                }
                else freq[a[l]]--;
                l++;
            }
            ans += l-1;
        }
        System.out.println(ans);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}
