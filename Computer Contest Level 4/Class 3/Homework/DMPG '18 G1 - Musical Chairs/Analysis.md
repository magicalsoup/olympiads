# Analysis

## Subtaks 1
For the first subtask, you can use simulation to pass.

**Time Complexity:** $`O(NK)`$

## Subtask 2
Loop through the chairs twice, as there is a possbility of looping around. Keep a boolean array to keep track of which chair is empty, and mark the where the students are. If a 
student has been seated down, mark them as `-1`. Now as you loop through the chairs, if the student is standing and the chair he is standing infront is empty, sit him onto the chair. 
else if the chair is empty, get the person from the top of stack and and put him onto the chair. If a studen hasn't been seated yet, push them into the stack. The last person in the stack
is the person who is still standing up.

**Time Complexity:** $`O(N + K)`$
