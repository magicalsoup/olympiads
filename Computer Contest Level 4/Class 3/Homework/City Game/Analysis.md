# Analysis

Let us first solve a small problem first. Given an histogram, what is the largest rectangle area? We can use a Mono Stack to solve this. We keep an increasing stack,
and whenever we pop, we find the left and right most index of the possible rectangle, and get its area. Update the answer if need be. Now let's solve this problem.
We can read the city plan from top to bottom, if we see a `F`, add 1 to that histogram, if we see a `R`, set that cell of the histogram to $`0`$. Then, each time
we compress top to bottom, we calculate the max rectangle area. It takes $`O(N)`$ to calculate each histogram, and there are $`M`$ rows.

**Time Complexity:** $`O(NM)`$