import java.util.Scanner;
import java.util.Stack;
public class ccoprep16q1 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int K = sc.nextInt();
        while(K-- >0) {
            int M = sc.nextInt(), N = sc.nextInt();
            int ans=0;
            int heights[] = new int[N];
            char g[][] = new char[M][N];
            for(int i=0; i<M; i++) {
                for(int j=0; j<N; j++) {
                    g[i][j] = sc.next().charAt(0);
                    if(g[i][j] == 'F') heights[j]++;
                    else heights[j]=0;
                }
                ans = Math.max(ans, largestRectangle(heights));
            }
            System.out.println(ans*3);
        }
        sc.close();
    }
    static int largestRectangle(int height[]) {
        Stack<Integer> st = new Stack<Integer>();
        int max_area=0;
        int index=0;
        while(index < height.length) {
            if(st.isEmpty() || height[index] >= height[st.peek()])
                st.push(index++);
            else {
                int top = st.pop();
                int area = height[top] *  (st.isEmpty()? index: index - st.peek()-1);
                max_area = Math.max(max_area, area);
            }
        }
        while(!st.isEmpty()) {
            int top=st.pop();
            int area = height[top] * (st.isEmpty()? index: index - st.peek() - 1);
            max_area = Math.max(max_area, area);
        }
        return max_area;
    }
}
