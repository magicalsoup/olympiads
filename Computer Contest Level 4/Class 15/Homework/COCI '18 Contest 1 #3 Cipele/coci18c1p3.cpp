#include <bits/stdc++.h>
using namespace std;
#define INF 0x3f3f3f3f
#define v first
#define w second
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007;
const int MAXN = 1000005, MAXQ = 1000005;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
vector<int> lft, rit; int n, m;
bool check(int dif) {
    int cnt=0, tar = min(n, m);
    for(int i=0, j=0; i<n && j<m;) {
        if(abs(rit[j] - lft[i]) <= dif) {
            i++; j++; cnt++;
        }
        else if(lft[i] < rit[j]) i++;
        else j++;
    }
    return cnt == tar;
}
int main() {
    scanf("%d %d", &n, &m);
    for (int i = 0, x; i < n; i++) {
        scanf("%d", &x);
        lft.push_back(x);
    }
    for(int i=0, x; i < m; i++) {
        scanf("%d", &x);
        rit.push_back(x);
    }
    sort(lft.begin(), lft.end());
    sort(rit.begin(), rit.end());
    int lo=0, hi=1e9;
    while(lo <= hi) {
        int mid = lo+(hi-lo)/2;
        if(check(mid)) hi=mid-1;
        else lo=mid+1;
    }
    printf("%d\n", lo);
}