#include <bits/stdc++.h>
using namespace std;
#define INF 0x3f3f3f3f
#define v first
#define w second
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007;
const int MAXN = 1000005, MAXQ = 1000005;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
int BIT[22][300005], arr[300005];
int query(int idx, int x) {
    int sum=0;
    for(int i=x; i>0; i-=i&-i)
        sum += BIT[idx][i];
    return sum;
}
void update(int idx, int x, int val){
    for(int i=x; i<300005; i+=i&-i)
        BIT[idx][i] += val;
}
int N, M;
int main() {
    scanf("%d %d", &N, &M);
    for(int i=1; i<=N; i++) {
        scanf("%d", &arr[i]);
        update(arr[i], i, 1);
    }
    for(int t=1, cmd; t<=M; t++) {
        scanf("%d", &cmd);
        if(cmd == 1) {
            int a, b; scanf("%d %d", &a, &b);
            update(arr[a], a, -1);
            update(b, a, 1);
            arr[a]=b;
        }
        else {
            int l, r, c;
            scanf("%d %d %d", &l, &r, &c);
            int tot=0;
            for(int i=20; i>=0; i--) {
                if(tot + query(i, r) - query(i, l-1) >= c) {
                    printf("%d\n", i);
                    break;
                }
                tot += query(i, r) - query(i, l-1);
            }
        }
    }
}