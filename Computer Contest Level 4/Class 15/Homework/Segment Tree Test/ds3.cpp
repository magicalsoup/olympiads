#include <bits/stdc++.h>
using namespace std;
#define INF 0x3f3f3f3f
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
#define For(i, a, b) for(int i=a; i<b; i++)
#define FOR(i, a, b) for(int i=a; i<=b; i++)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007, MM = 100005;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
struct node {
    int l, r, val, gcd, cnt;
} seg[3*MM];
int N, M, a[MM];
void push_up(int idx) {
    seg[idx].val = min(seg[2*idx].val, seg[2*idx+1].val);
    seg[idx].gcd = gcd(seg[2*idx].gcd, seg[2*idx+1].gcd);
    seg[idx].cnt = 0;
    if(seg[idx].gcd == seg[2*idx].gcd) seg[idx].cnt += seg[2*idx].cnt;
    if(seg[idx].gcd == seg[2*idx+1].gcd) seg[idx].cnt += seg[2*idx+1].cnt;
}
void build(int l, int r, int idx) {
    seg[idx].l=l; seg[idx].r=r;
    if(l == r) {
        seg[idx].val = a[l];
        seg[idx].gcd = a[l];
        seg[idx].cnt = 1;
        return;
    }
    int mid = (l+r)/2;
    build(l, mid, 2*idx); build(mid+1, r, 2*idx+1);
    push_up(idx);
}
void update(int pos, int val, int idx) {
    if(seg[idx].l == pos && seg[idx].r == pos) {
        seg[idx].val = val;
        seg[idx].gcd = val;
        return;
    }
    int mid = (seg[idx].l + seg[idx].r)/2;
    if(pos <= mid) update(pos, val, 2*idx);
    else update(pos, val, 2*idx+1);
    push_up(idx);
}
int querymin(int l, int r, int idx) {
    if(seg[idx].l == l && seg[idx].r == r)
        return seg[idx].val;
    int mid = (seg[idx].l + seg[idx].r) / 2;
    if(r <= mid) return querymin(l, r, 2*idx);
    else if(l > mid) return querymin(l, r, 2*idx+1);
    return min(querymin(l, mid, 2*idx), querymin(mid+1, r, 2*idx+1));
}
int querygcd(int l, int r, int idx) {
    if(seg[idx].l == l && seg[idx].r == r)
        return seg[idx].gcd;
    int mid = (seg[idx].l + seg[idx].r) / 2;
    if(r <= mid) return querygcd(l, r, 2*idx);
    else if(l > mid) return querygcd(l, r, 2*idx+1);
    return gcd(querygcd(l, mid, 2*idx), querygcd(mid+1, r, 2*idx+1));
}
int querycnt(int l, int r, int idx, int val) {
    if(seg[idx].l == l && seg[idx].r == r) {
        return seg[idx].gcd == val? seg[idx].cnt : 0;
    }
    int mid = (seg[idx].l + seg[idx].r) / 2;
    if(r <= mid) return querycnt(l, r, 2*idx, val);
    else if(l  > mid) return querycnt(l, r, 2*idx+1, val);
    return querycnt(l, mid, 2*idx, val) + querycnt(mid+1, r, 2*idx+1, val);
}
int main() {
    scanf("%d %d", &N, &M);
    for(int i=1; i<=N; i++) scanf("%d", &a[i]);
    build(1, N, 1);
    while(M--) {
        char cmd; scanf(" %c", &cmd);
        if(cmd == 'C') {
            int x,  v; scanf("%d %d", &x, &v);
            update(x, v, 1);
        }
        if(cmd == 'M') {
            int l, r; scanf("%d %d", &l, &r);
            printf("%d\n", querymin(l, r, 1));
        }
        if(cmd == 'G') {
            int l, r; scanf("%d %d", &l, &r);
            printf("%d\n", querygcd(l, r, 1));
        }
        if(cmd == 'Q') {
            int l, r; scanf("%d %d", &l, &r);
            printf("%d\n", querycnt(l, r, 1, querygcd(l, r, 1)));
        }
    }
}
