#include <bits/stdc++.h>
using namespace std;
#define INF 0x3f3f3f3f
#define v first
#define w second
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007;
const int MAXN = 1000005, MAXQ = 1000005;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
int N; ll len[4005], freq[2002];
int main() {
    scanf("%d", &N);
    for(int i=0, x; i<N; i++) {
        scanf("%d", &x); freq[x]++;
    }
    for(int i=1; i<=2000; i++) {
        if(freq[i] > 0) {
            for(int j=i; j<=2000; j++) {
                if(i == j) len[i + j] += freq[i]/2;
                else if(freq[j] > 0) len[i + j] += min(freq[i], freq[j]);
            }
        }
    }
    ll ans1=0, ans2=0;
    for(int i=1; i<=4000; i++) {
        if(len[i] > ans1) {
            ans1 = len[i];
            ans2=1;
        }
        else if(len[i] == ans1)
            ans2++;
    }
    printf("%lld %lld\n", ans1, ans2);
}