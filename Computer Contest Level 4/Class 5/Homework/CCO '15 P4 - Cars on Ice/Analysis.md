# Analysis

There are multiple ways to solve this problem, here I'll explain a few, and go into detail about what the way I followed.

1. You can do bfs/dfs, after each move, check the next car which can be pushed out
2. You can create a graph, filter it, then use topological sort to find the order

What I did is similar to the first method. I keep a deque for each row, which tells me what cars are in each row, meaning that the start and end of the deque should be popped first,
as there is no cars blocking its way in terms of horizontal movement. Then I do the same for the vertical movement cars, which I keep in a deque. For each column deque, the start and
end of the deque should popped first, as there is no caras blocking its way in terms of vertical movement.

Thus we can write a simple check method, that checks which cars can be pushed out in both the vertical and horizontal movements. Then simply print the answer out.

**Time Complexity:** $`O(NM)`$