import java.util.Scanner;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.ArrayList;
public class cco15p4 {
    static boolean vis[][];
    static char g[][];
    static ArrayList<pair> ans = new ArrayList<>();
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), M = sc.nextInt(), tot=0;
        g = new char[N][M]; vis = new boolean[N][M];
        Deque<state> row[] = new ArrayDeque[N];
        Deque<state> col[] = new ArrayDeque[M];
        for(int i=0; i<N; i++) row[i] = new ArrayDeque<>();
        for(int j=0; j<M; j++) col[j] = new ArrayDeque<>();

        for(int i=0; i<N; i++) {
            g[i] = sc.next().toCharArray();
            for(int j=0; j<M; j++) {
                if(g[i][j] == '.') continue;
                tot++;
                row[i].add(new state(g[i][j], i, j));
                col[j].add(new state(g[i][j], i, j));
            }
        }
        while(ans.size() < tot){
            check(row, 'W', 'E', N);
            check(col ,'N', 'S', M);
        }
        for(pair i : ans)
            System.out.println("(" + i.r + "," + i.c + ")");
        sc.close();
    }
    static void check(Deque<state> dq[], char a1, char a2, int n) {
        for(int i=0; i<n; i++) {
            if(dq[i].isEmpty()) continue;
            while(!dq[i].isEmpty() && vis[dq[i].peekFirst().r][dq[i].peekFirst().c])
                dq[i].pollFirst();
            while(!dq[i].isEmpty() && vis[dq[i].peekLast().r][dq[i].peekLast().c])
                dq[i].pollLast();
            if(dq[i].isEmpty()) continue;
            while(!dq[i].isEmpty() && dq[i].peekFirst().dir == a1) {
                vis[dq[i].peekFirst().r][dq[i].peekFirst().c] = true;
                ans.add(new pair(dq[i].peekFirst().r, dq[i].peekFirst().c));
                dq[i].pollFirst();
            }
            if(dq[i].isEmpty()) continue;
            while(!dq[i].isEmpty() && dq[i].peekLast().dir == a2) {
                vis[dq[i].peekLast().r][dq[i].peekLast().c] = true;
                ans.add(new pair(dq[i].peekLast().r, dq[i].peekLast().c));
                dq[i].pollLast();
            }
        }
    }
    static class pair {
        int r, c;
        public pair(int r, int c){
            this.r=r;
            this.c=c;
        }
    }
    static class state {
        char dir; int r, c;
        public state(char dir, int r, int c){
            this.dir=dir;
            this.r=r;
            this.c=c;
        }
    }
}
