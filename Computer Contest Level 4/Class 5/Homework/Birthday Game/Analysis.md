# Analysis

This problem is finding the longest cycle in a DAG (Direct Acyclic Graph). This can be done using topological sort or dfs, I prefer the latter. We can keep a multi-use visited array,
the visited array is $`0`$ if it has not been visited before, or $`1`$, if it has been visited before, or $`2`$, if it had been checked for a cycle. That means if a $`visited[i]`$ is equal to $`1`$, that means
it was a cycle, and we can mind the length of the cycle by subtract the distance in the edges $`dist[u] - dist[v] + 1`$. If the vertex has not been visted before, we do a standard
dfs. And to find all the cycles, we dfs all vertexes, but since we mark the vertex as visited, all vertex are only considered at most twice, which will give us a complexity of 
$`O(N)`$.

**Time Complexity:** $`O(N)`$