# Analysis

There are many ways to solve this problem, as the problem just boils down to finding if there is a cycle in a DAG (Directed ACyclic Graph). One way is to dfs every vertex, to
see if there is a cycle. Another way is to do topological sort, and check if there is a cycle. Something like Floyd-Warhsall won't pass though, as $`N`$ can be as big as $`10^4`$

**Time Complexity:** $`O(NM)`$ (dfs) or $`O(N + M)`$ (topological sort).