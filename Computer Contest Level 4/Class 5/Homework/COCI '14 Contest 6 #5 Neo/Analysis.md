# Analysis

For this problem, its crucial to see that the following statement holds:

**For any given marix $`A`$, if $`A`$ is *extremely cool*, then all submatrix of $`2 \times 2`$ in $`A`$ is also cool.**

We can prove this with some mathematical reasoning.

Lets set up a matrix $`A`$ which is extremely cool


```math
\begin{bmatrix} 
a & b & c \\
d & e & f \\
g & h & i \\
\end{bmatrix}

\\
\\

\text{lets assume that the matrix } 
\begin{bmatrix}
a & b \\
d & e \\
\end{bmatrix}

\text{is cool.}
\space 
\text{Now that means } a+e \le b+d \text{, this also means that } b+f \le c+e

\\
\\

\text{Now lets add up the right side and left side, which gives us this equation: } a+e+b+f \le b+d+c+e.

\\
\\

\text{assume the above statement is still true, we can cancel out the duplicates, and reach a final equation of: } a+f \le c+d.

\\
\\
\\
\\

\text{This tells us that the matrix } 
\begin{bmatrix}
a & b & c \\
d & e & f \\
\end{bmatrix}

\text{ is extremely cool.}

\\
\\

\text{which proves the statement, and can be used to prove that matrix } A \text{ is also extremely cool, given that all its 2x2 matrices are also cool.}
```

With this proof, we now optimized our brute-force. For the second part of the brute force, we can implement a mono stack. (largest rectangle in a histogram). 
For each $`2 \times 2`$ submatrix, we will compress it into a single square, a 1 if it is a cool submatrix, or 0 if its not. Then we can do a similar fashion in the problem City Game,
and compress each row and call the function to find the largest rectangle in the histogram. A slight tweak in the code for the largest rectangle is to increase the height and width
by $`1`$ when finding the area, as we compressed the $`2 \times 2`$ matrices into $`1 \times 1`$ earlier. 

**Time Complexity:** $`O(RS)`$
