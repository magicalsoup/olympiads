# Analysis

This a pretty easy to do once you get the basic idea. We will start as from the bottom, and do a bottom up dp-approach. The transition is to find the number of triangles
that used $`a_{r,c}`$ as part of it and up to row $`r`$. That is to say, if $`a_{r,c}`$ is a `#`, then we check the dp values underneath it, specifically the triangle bases
$`dp_{r-1,c}, dp_{r-1, c-1}, dp_{r-1, c+1}`$, and we take the minimum and plus $`1`$ to it. Thus, our dp transition looks like this:

```cpp
if(a[row][col] == '#')
    dp[row][col] = min(dp[row-1][col], min(dp[row-1][col-1], d[row-1][col+1])) + 1;
```

Obvisouly, the base case is if $`a_{r, c}`$ is equal to `#`, then $`dp_{r, c} = 1`$. Then we sum up the entire grid to get our answer.

**Time Complexity:** $`O(N^2)`$