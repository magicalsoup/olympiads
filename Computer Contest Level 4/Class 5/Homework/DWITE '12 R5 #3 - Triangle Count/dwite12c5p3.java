import java.util.Scanner;
public class dwite12c5p3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        for (int test = 1; test <= 5; test++) {
            int N = sc.nextInt();
            int dp[][] = new int[N + 1][N + 1];
            char g[][] = new char[N + 1][N + 1];
            for (int i = 1; i <= N; i++) {
                String s = sc.next();
                for (int j = 1; j <= N; j++) {
                    g[i][j] = s.charAt(j - 1);
                    dp[i][j] = g[i][j] == '#' ? 1 : 0;
                }
            }
            int ans=0;
            for (int row=N-1; row>0; row--) {
                for (int col = 1; col < N; col++) {
                    if(g[row][col] == '#') 
                        dp[row][col] = Math.min(dp[row + 1][col], Math.min(dp[row + 1][col - 1], dp[row + 1][col + 1])) + 1;
                }
            }
            for(int i=1; i<=N; i++) {
                for (int j = 1; j <= N; j++) {
                    ans += dp[i][j];
                }
            }
            System.out.println(ans);
        }
    }
}