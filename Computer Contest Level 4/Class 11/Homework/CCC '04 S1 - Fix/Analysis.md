# Analysis

This is simply an implementation problem. For every word in the 3 word list, check to see if any of the other 2 words is your suffix or prefix. If such a word exists, print `YES`, else
output `No`

**Time Complexity:** $`O(NS)`$, where $`S`$ is the length of the string with the maximum length in the 3 word list.