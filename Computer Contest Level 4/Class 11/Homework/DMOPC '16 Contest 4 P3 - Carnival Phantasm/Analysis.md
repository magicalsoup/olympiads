# Analysis

This problem is asking you to implement a data structure to support the mentioned operations.

Here, I will be using an ordered_set or Treeset in java. We will keep an array of Treesets/ordered_set. 

For the `A` operation, at the flavour/$`k^{th}`$ index, we will add a new pair which consists of the distance and id of the stand.

For the `S` operation, we will remove the stand at the $`k^{th}`$ index.

For the `E` operation, we will remove the stand at all the flavours, since we don't actually know the flavour of the current stand, due to duplicates. This is possible because there
is only up to $`100`$ flavours.

For the `Q` operation, get the first element's id of your set/treeset of flavour $`k`$, if its empty, print `-1`. Here I assumed your treeset/ordered_set sorts by from least
to greatest in terms of distance of each stand.

**Time Complexity:** $`O(100 \times Q \log N)`$