import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.TreeSet;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
public class dmopc16c4p3 {
    static ArrayList<TreeSet<pair>> apples = new ArrayList<>(); // treeset at k is the stands that sell apple k
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt(), S = readInt(), dis[] = new int[N+1];
        for(int i=1; i<=N; i++)
            dis[i] = readInt();
        for(int i=0; i<=100; i++) apples.add(new TreeSet<>());
        for(int i=1; i<=S; i++) {
            int id = readInt(), flavour = readInt();
            apples.get(flavour).add(new pair(dis[id], id));
        }
        int Q = readInt();
        for(int t=0; t<Q; t++) {
            char c = next().charAt(0);
            if(c == 'A') {
                int id = readInt(), flav = readInt();
                apples.get(flav).add(new pair(dis[id], id));
            }
            if(c == 'S') {
                int id = readInt(), flav = readInt();
                apples.get(flav).remove(new pair(dis[id], id));
            }
            if(c == 'E') {
                int id = readInt(), dist = readInt();
                for(int i=0; i<100; i++)
                    apples.get(i).remove(new pair(dis[id], id));
                dis[id] = dist;
            }
            if(c == 'Q') {
                int flav = readInt();
                if(apples.get(flav).isEmpty()) System.out.println(-1);
                else {
                    int ans = apples.get(flav).first().id;
                    System.out.println(ans);
                }
            }
        }
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static class pair implements Comparable<pair>{
        int dist, id;
        public pair(int dist, int id) {
            this.dist=dist;
            this.id=id;
        }
        @Override
        public int compareTo(pair o) {
            if(dist == o.dist)
                return Integer.compare(id, o.id);
            return Integer.compare(dist, o.dist);
        }
        public int hashCode(){
            int prime=2349, MOD=1000000007;
            return (((prime%MOD)*(dist%MOD))%MOD + ((prime%MOD)*(prime%MOD)*id%MOD)%MOD) % MOD;
        }
        public boolean equals(Object o){
            return hashCode() == o.hashCode();
        }
    }
}