#include <bits/stdc++.h>
using namespace std;
#define f first
#define s second
typedef long long ll;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
const int MAXN = 1<<13;
int BIT[MAXN+5][MAXN+5];
void update(int row, int x,int val) {
	for(int j=row; j<=MAXN; j+=j&-j)
		for(int i=x; i<=MAXN; i+=i&-i)
			BIT[j][i] += val;
}
ll query(int r, int c) {
	ll sum=0;
	for(int i=r; i>0; i-=i&-i) 
		for(int j=c; j>0; j-=j&-j) 
			sum += BIT[i][j];
	return sum;		
}
int N, Q, a[MAXN+5];
int main() {
	scanf("%d", &N);
	for(int i=1; i<=N; i++) {
		scanf("%d", &a[i]);
		a[i]++;
		update(a[i], i, 1);
	}
	scanf("%d", &Q);
	while(Q--) {
		int com;
		scanf("%d", &com);
		if(com == 1) {
			int l, r, a, b;
			scanf("%d %d %d %d", &l, &r, &a, &b);
			l++; r++; a++; b++;
			ll tot = query(b, r) - query(a-1, r) - query(b, l-1) + query(a-1, l-1);
			printf("%lld\n", tot);
		}
		else {
			int x, h;
			scanf("%d %d", &x, &h);
			x++; h++;
			update(h, x, 1);
			update(a[x], x, -1);
			a[x] = h;
		}
	}
}