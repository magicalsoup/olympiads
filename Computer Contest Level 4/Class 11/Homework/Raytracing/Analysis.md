# Analysis

We can solve this problem using a 2D bit. The first dimension represents the indexes $`[l, r]`$, and the second dimesnion represents the value $`[a, b]`$, and since $`a, b \le N`$,
we can do this without having huge memory errors.

Then an element is simply defined by its value and index, and we will update it at $`\text{update}(\text{value}, \text{index}, 1)`$. Then finding the number of trees with heights
$`[a, b]`$ from indexes $`[l, r]`$ is simply querying the square $`(a, l)`$ to $`(b, r)`$. 

Make sure to keep an original array to keep track of the trees height, and make sure to subtract $`1`$ from the original square if a tree ungrows or grows to a new height.

**Time Complexity:** $`O(Q \log^2(N))`$