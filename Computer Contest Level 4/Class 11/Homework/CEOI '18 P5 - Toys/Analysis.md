# Analysis

The problem statement is somewhat confusing, but the problem itself is pretty straight forward. Given $`n`$ days, how many ways can you make $`r`$ with the prime factors of $`n`$?

For example, given the sample, $`n=12`$, the prime factorization is $`2 \times 2 \times 3`$. We can make one of the $`r`$ with the factors $`4`$ and $`3`$. Since $`4 \times 3 = 12`$,
so the total number of toys we have is $`(4-1) + (3-1) = 5`$. We subtract $`1`$ because an empty subset is also considered a set, so its like given $`a`$ of toy 1 and $`b`$ of toy 2,
if an empty set is also considered a valid set, then how many different subsets can we make? The answer is $`(a+1) \times (b+1)`$.

Now we actually understand the problem, we can begin to solve. Think, can we just take a factor of the current $`n`$, then add the sum of the previous factors and this factor together
and push it into an set, and keep repeating this for all the factors of $`n`$ and their factors as well? The answer is yes.

For example, take $`12`$ for example, we first push $`12 + \text{prev} - 1`$, which is $`11`$, since $`\text{prev} = 0`$. We will then loop through $`2`$ to $`\sqrt{12} \approx 3.4`$, for any prime factorization of the number, we only need to go to the square root of that
number. Proof is left as an exercise to the reader.

Now, $`2`$ is a factor, so we recurse with $`(6, 2, 0+2-1)`$. Namely, our recursion function is $`(\text{curN}, \text{factor}, \text{prev})`$.

Then we do the same for $`3`$ we recurse with $`(4, 3, 0+3-1)`$. And then we do the same thing with $`2`$ and $`3`$ as the $`\text{curN}`$ as well, and so on.

After pushing the values into a set, just sort the set and output the size the contents of the set.

**Time Complexity:** $`O(N \log N + \sqrt{N})`$