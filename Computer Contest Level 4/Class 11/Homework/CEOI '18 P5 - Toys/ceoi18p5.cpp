#include <bits/stdc++.h>
using namespace std;
#define f first
#define s second
#define INF 1L<<60;
#define NINF -INF
typedef long long ll;
typedef long double lb;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MM = 1000005;
vector<int> ans;
void recur(int curVal, int curMin, int sum) {
	ans.push_back(curVal + sum - 1);
	for(int i=curMin; i*i<=curVal; i++)
		if(curVal % i == 0) recur(curVal/i, i, sum+i-1);
}
int n;
int main() {
	scanf("%d", &n);
	recur(n, 2, 0);
	sort(ans.begin(), ans.end());
	int sz = unique(ans.begin(), ans.end()) - ans.begin();
	printf("%d\n", sz);
	for(int i=0; i<sz; i++)
		printf("%d ", ans[i]);
}