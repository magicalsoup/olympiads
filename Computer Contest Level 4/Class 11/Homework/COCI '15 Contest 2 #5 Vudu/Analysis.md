# Analysis

If we subtract $`P`$ from all the values of the original array, then the problem just boils down to finding the number of contigious subarrays such that the sum is greater than or equal
to $`0`$.

We use a prefix sum array to quickly get the sum of a contigious sequence, and then the problem just boils down to how many contigious subsarrays with a sum greater or equal to $`0`$, for each contigious subarrays.

For this we can use a BIT, for each PSA sum, we can assign a value to it, after we sort the values of each psa from greatest to least. If we don' assign a value, we will MLE, due
to the size of the input, also since we only care about their **relative** order, we can do that. After that, we just update the value, and query how many values before my current
value and add it to the total sum, also to increment sum by one if your current value is greater than or equal to $`0`$.

**Time Complexity:** $`O(N \log N)`$