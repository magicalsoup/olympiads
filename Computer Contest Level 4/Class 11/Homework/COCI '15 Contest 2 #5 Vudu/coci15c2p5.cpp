#include <bits/stdc++.h>
using namespace std;
#define f first
#define s second
#define NINF -(1L<<60);
typedef long long ll;
typedef long double lb;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;	
ll BIT[1000005], psa[1000005];
void update(int x, int val) {
	for(int i=x; i <= 1000001; i+=i&-i)
		BIT[i] += val;
}
ll query(int x) {
	ll sum=0;
	for(int i=x; i>0; i-=i&-i)
		sum += BIT[i];
	return sum;	
}
int N, P, c[1000005]; pll hashed[1000005];
int main() {
	scanf("%d", &N);
	for(int i=1; i<=N; i++)
		scanf("%lld", &psa[i]);
	scanf("%d", &P);
	for(int i=1; i<=N; i++) {
		psa[i] += psa[i-1] - P;
		hashed[i] = make_pair(psa[i], i);	
	}	
	hashed[0] = make_pair(-0x3f3f3f3f3f3fL, -0x3f3f3f3f3f);
	sort(hashed, hashed+N+1);
	int val=1;
	for(int i=1; i<=N; i++) {
		if(i>1 && hashed[i].f != hashed[i-1].f) val++;
		c[hashed[i].s] = val;
	}
	ll ans=0;
	for(int i=1; i<=N; i++) {
		if(psa[i] >= 0) ans++;
		ans += query(c[i]);
		update(c[i], 1);
	}
	printf("%lld\n", ans);
}