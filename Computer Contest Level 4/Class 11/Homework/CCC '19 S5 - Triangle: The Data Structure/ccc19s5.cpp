#include <bits/stdc++.h>
using namespace std;
#define f first
#define s second
#define INF 1L<<60;
#define NINF -INF
typedef long long ll;
typedef long double lb;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
int BIT[3002][3002];
void update(int r, int c, int val) {
	for(int i=r; i<3002; i+=i&-i)
		for(int j=c; j<3002; j+=j&-j)
			BIT[i][j] = max(BIT[i][j], val);
} 
int query(int r, int c){
	int ans=0;
	for(int i=r; i>0; i-=i&-i)
		for(int j=c; j>0; j-=j&-j)
			ans = max(ans, BIT[i][j]);
	return ans;		
}
int N, K, a[3002][3002]; 
ll ans=0;
int main() {
	scanf("%d %d", &N, &K);
	for(int i=1; i<=N; i++) 
		for(int j=N-i+1; j<=N; j++) 
			scanf("%d", &a[i][j]);
	int cnt=N-K+1;		
	for(int i=N; i>=N-K; i--) {
		for(int j=N; j>=cnt; j--) {
			update(i, j, a[i][j]);
		}
		cnt++;
	}
	for(int i=N-K; i>=1; i--) {
		int row=i, col=N;
		while(row<=N-K) {
			ans += query(row+K, col);
			row++; col--;
		} 
		row=i, col=N;
		while(row <=N) {
			update(row, col, a[row][col]);
			row++; col--;
		}
		cout<<endl;
	}
	int row=1, col=N;
	while(row <= N-K+1) {
		ans += query(row+K-1, col);
		row++; col--;
	}
	printf("%lld\n", ans);
}