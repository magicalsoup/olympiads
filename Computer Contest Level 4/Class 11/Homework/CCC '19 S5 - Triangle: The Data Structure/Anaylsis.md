# Analysis

We can use DP to solve the subtask easily, which gives us $`4/15`$ points Simply by `compressing` the dp for each $`k`$ up to the original $`K`$, and using the previous computed compressed
grid to quickly find the next DP state. You can either use a flip array or 2d array to accomplish this task;

```
for k in 2...K:
    for i in 1 ... N-K+1:
        for j in 1 ... i:
            dp[i][j] = max(dp[i][j], max(dp[i+1][j+1], dp[i+1][j]))
```

**Time Complexity:** $`O(N^2K)`$ or $`O(N^3)`$

To quicken our DP, we can see that we don't need to iterate through all values of $`K`$, instead we can skip over some of them. This time we can multiple $`1.5`$ and cast it to int 
to our $`k`$. This is close to $`\log K`$ speed for the first loop now, so our time complexity is around $`O(N^2 \log n)`$. Implementation is left as an exercise to the reader.

**Time Complexity:** $`O(N^2 \log K)`$ or $`O(N^2 \log N)`$

Another way to do this is with 2D BIT. This time, instead of getting the sum over a range, we can the maximum over a range. This means when we update and query, we either update 
the maximum or get the maximum. The downside to this is that we cannot *demax* a value, meaning if a value was at $`5`$, we cannot change it back to $`3`$.

Another problem is that we are use to querying to the top left corner, but the shape the triangle is normally in messes with our BIT, because many maximums overlap when we are updating,
and we cannot ever *demax* a value. Thus we can push everything to the most right, this way the triangle will be flipped over the y-axis. Now when we query, the rest of the values
are $`0`$, so we don't have to worry of maximums overlapping.

For example, given the sample, the inverted triangle can look like this in an array (the array is 1-base indexed):

|Index| 1 | 2 | 3 | 4 |
|:----|:--|:--|:--|:--|
|1    | 0 | 0 | 0 | 3 |    
|2    | 0 | 0 | 1 | 2 |
|3    | 0 | 4 | 2 | 1 |
|4    | 6 | 1 | 4 | 2 |

Now, for example, if query the triangle $`(4, 2, 1)`$ in the bottom right corner, after we update those values and query the triangle $`(2,1,2)`$ that is above it, we wont have
the $`4`$ as the maximum, instead we have the real maximum which is $`2`$.

After getting this understood, we can move on to querying the triangles, its simply getting the BIT query of the rectangle with the bottom vertex at $`(N+K, N)`$.

For example, lets do the sample (we are doing 1 indexed based for better understanding):

|Index| 1 | 2 | 3 | 4 |
|:----|:--|:--|:--|:--|
|1    | 0 | 0 | 0 | 3 |    
|2    | 0 | 0 | 1 | 2 |
|3    | 0 | 4 | 2 |`1`|
|4    | 6 | 1 |`4`|`2`|

We first update the bottom right-most triangle of size $`K`$. Then we query the maximum, which is $`(4)`$ (the updated values are highlighted)

Then we will update the diagonal (the updated values are highlighted).

|Index| 1 | 2 | 3 | 4 |
|:----|:--|:--|:--|:--|
|1    | 0 | 0 | 0 | 3 |    
|2    | 0 | 0 | 1 |`2`|
|3    | 0 | 4 |`2`| 1 |
|4    | 6 |`1`| 4 | 2 |

Then we can get a triangle along the diagonals of the updated values, namely $`(2, 2, 1), (2, 1, 4)`$, starting from top to bottom. Our maximums are now  $`(4, 2, 4)`$

Then we can update the next diagonal (the updated values are highlighted).

|Index| 1 | 2 | 3 | 4 |
|:----|:--|:--|:--|:--|
|1    | 0 | 0 | 0 |`3`|    
|2    | 0 | 0 |`1`| 2 |
|3    | 0 |`4`| 2 | 1 |
|4    |`6`| 1 | 4 | 2 |

Then we can get the maximums along the diagonal, the triangles are $`(3, 1, 2), (1, 4, 2), (4, 6, 1)`$ respectively, Our maximms are now $`(4, 2, 4, 3, 4, 6)`$

Now there is no more diagonals left to update, we can now sum up the maximums, which is $`4+2+4+3+4+6 = 23`$. And that is our answer.

**Time Complexity:** $`O(N^2 \log^2 N)`$
