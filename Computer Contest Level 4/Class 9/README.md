# Class 9
 
## Binary Indexed Tree (Fenwick Tree)
- an int array of size $`N`$
- Everything is related with the binary representation of the numbers

## How It Works?
- Each element in the array stores cumulative freqeuncy of consecutive list of boxes
- Range of boxes that is stored is related to "binary value" of the index

## Define
- $`f(x) =`$ number of marble in box $`x`$
- $`c(x) =`$ summation of number of marble in box #1 to box #$`x`$
- $`tree[x] =`$ element $`x`$ in the array

## Storage Solution
- $`Tree[16] = f(1) + f(2) + \cdots + f(16)`$
- $`Tree[12] = f(9) + f(10) + \cdots + f(12)`$
- $`Tree[6] = f(5) + f(6)`$
- $`Tree[3] = f(3)`$

## The Last 1
- A node at the index $`X`$ will store freq of boxes in the range
    - $`X - 2^r+1`$ to $`X`$
    - Where $`r`$ is the position of the last digit of $`1`$
- Example:
    - $`X = 12 (1100)_2`$
    - Node will store freq from $`9`$ to $`12`$
    - The last $`1`$ of $`12`$ is at position $`2`$ (0-indexed)
    - $`12 - 2^2 + 1 = 9 = (1001)_2`$

## Query
- Remember: Remove last bit one each time
- And find new number and add that number

## Update
- Add the value of the last set bit
- And update the new index

## Get the negative / Twos complement
- Flip the bits, add 1 to the end, and you get the negative version of the number

## Build
```java
for(int i=1; i<=n; i++) {
    BIT[i] += a[i];
    int j = i+(i&-i);
    if(j <= n) BIT[j] += BIT[i];
}
```