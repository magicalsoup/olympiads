#include <bits/stdc++.h>
using namespace std;

double merge(int array[], int l, int mid, int r){
    int temp[r - l + 1];
    int lindex = l, mindex = mid + 1, index = 0;
    double total = 0;
    while(lindex <= mid && mindex <= r){
        if(array[lindex] <= array[mindex])
            temp[index++] = array[lindex++];
        else {
            temp[index++] = array[mindex++];
            total += (mid + 1 - lindex);
        }
    }

    while(lindex <= mid)
        temp[index++] = array[lindex++];

    while(mindex <= r)
        temp[index++] = array[mindex++];

    for(lindex = l; lindex <= r; lindex++)
        array[lindex] = temp[lindex - l];

    return total;
}

double ans = 0;
void mergesort(int array[], int l, int r){
    if(l == r) return;
    int mid = (l + r) / 2;
    mergesort(array, l , mid);
    mergesort(array, mid + 1, r);
    ans += merge(array, l , mid, r);
}
int a[100001], N;
int main() {
    scanf("%d", &N);
    for(int i = 0; i < N; i++)
        scanf("%d", &a[i]);

    mergesort(a, 0, N - 1);
    printf("%.2f\n", (ans + N) / N);
    return 0;
}