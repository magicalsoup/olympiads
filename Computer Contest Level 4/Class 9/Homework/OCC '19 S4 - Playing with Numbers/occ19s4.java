import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;
public class occ19s4 {
    static TreeSet<Integer> start = new TreeSet<>();
    static TreeMap<Integer, Integer> len = new TreeMap<>();
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), Q = sc.nextInt();
        int a[] = new int[N+2];
        a[0] = Integer.MAX_VALUE; a[N+1] = Integer.MIN_VALUE;
        start.add(1);
        int pre=1;
        for(int i=1; i<=N; i++) {
            a[i] = sc.nextInt();
            if(i > 1 && a[i] < a[i-1]) {
                len.put(i-pre, len.getOrDefault(i-pre,0)+1);
                start.add(i); pre = i;
            }
        }
        start.add(N+1); len.put(N+1-pre, len.getOrDefault(N+1-pre, 0) + 1);
        System.out.println(len.lastKey());
        for(int i=1; i<=Q; i++) {
            int x = sc.nextInt(), y = sc.nextInt();
            a[x] = y;
            if(!start.contains(x) && a[x] < a[x-1]) add(x);
            else if(start.contains(x) && a[x] >= a[x-1]) remove(x);
            if(!start.contains(x+1) && a[x+1] < a[x]) add(x+1);
            else if(start.contains(x+1) && a[x+1] >= a[x]) remove(x+1);
            System.out.println(len.lastKey());
        }
        sc.close();
    }
    static void add(int x) {
        int hi = start.higher(x), lo = start.lower(x); remove2(hi-lo);
        len.put(hi - x, len.getOrDefault(hi-x, 0) + 1);
        len.put(x - lo, len.getOrDefault(x-lo,0) + 1);
        start.add(x);
    }
    static void remove(int x) {
        int hi = start.higher(x), lo = start.lower(x);
        remove2(hi-x); remove2(x-lo);
        len.put(hi-lo, len.getOrDefault(hi-lo,0) + 1);
        start.remove(x);
    }
    static void remove2(int x) {
        len.put(x, len.get(x)-1);
        if(len.get(x) == 0) len.remove(x);
    }
}
