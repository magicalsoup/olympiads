# Analysis

This problem may be the same kind of madness as this years CCC S3 in terms of implementation, beware, its ugly. 

To solve this problem, we have to understand that for each update, we are either breaking an increasing subarray into 2, merging 2 increasing subarrays, or not affecting at all.

Thus we can keep track of where the increasing subarrays starts, and put them in a treemap or something we can query in logarithmic or constant time.

When we are updating, we check to see if we are affecting any subarrays. 

```java
if(!start.contains(x) && a[x] < a[x-1]) add(x); // add a new start point
else if(start.contains(x) && a[x] >= a[x-1]) remove(x); // remove the current start
if(!start.contains(x+1) && a[x+1] < a[x]) add(x+1); // add a new start point
else if(start.contains(x+1) && a[x+1] >= a[x]) remove(x+1); // remove the current start
```

We can also put the length of each increasing subarray into a map, so we can query to see the length of a new subarray when merging or breaking. The map's key will keep track
of the length, and the value will be the length's frequency. When a length's frequency drops to 0, that means there no longer exists that length, and we remove it from the map.

Simply, the answer to the queries is the maximum key value in your map. You can use a multiset or treemap to quickly query the answer.

**Time Complexity:** $`O(Q \log N)`$