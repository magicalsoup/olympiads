# Analysis

We can use a BIT to count the number of numbers that are smaller and bigger than the current number, the answer is the sum of the minimum of the two.

**Time Complexity:** $`O(N \log N)`$