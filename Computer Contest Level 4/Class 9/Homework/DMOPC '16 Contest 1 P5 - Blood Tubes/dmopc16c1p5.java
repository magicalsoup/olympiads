import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class dmopc16c1p5 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException {
        int N = readInt();
        long BIT1[] = new long[N + 2], BIT2[] = new long[N + 2];
        long ans = 0;
        for (int i = 1; i <= N; i++) {
            int x = readInt();
            ans += Math.min(query(x, BIT1), query(N - x + 1, BIT2));
            update(x, 1, BIT1);
            update(N - x + 1, 1, BIT2);
        }
        System.out.println(ans);
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens()) 
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static void update(int x, int val, long BIT[]) {
        while(x < BIT.length) {
            BIT[x] += val;
            x += x&-x;
        }
    }
    static long query(int x, long BIT[]) {
        long sum = 0;
        while(x > 0) {
            sum += BIT[x];
            x -= (x&-x);
        }
        return sum;
    }
}