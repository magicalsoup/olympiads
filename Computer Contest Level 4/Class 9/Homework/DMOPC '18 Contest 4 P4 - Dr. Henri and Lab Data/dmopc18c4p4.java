import java.util.StringTokenizer;
import java.util.Arrays;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
public class dmopc18c4p4 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt(), Q = readInt();
        long BIT[] = new long[N+1], ans[] = new long[Q+1];
        pair a[] = new pair[N]; query q[] = new query[Q];
        for(int i=0; i<N; i++) {
            a[i] = new pair(readInt(), i+1);
            update(i+1, a[i].val, BIT);
        }
        for(int i=0; i<Q; i++) q[i] = new query(readInt(), readInt(), readInt(), i+1);
        Arrays.sort(q); Arrays.sort(a);
        int j=0;
        for(int i=0; i<Q; i++) {
            while(j < N && a[j].val < q[i].k) {
                update(a[j].idx, -2*a[j].val, BIT); j++;
            }
            ans[q[i].id] = query(q[i].l, q[i].r, BIT);
        }
        for(int i=1; i<=Q; i++) pw.println(ans[i]);
        pw.close();
    }
    static long query(int l, int r, long BIT[]) {
        return query(r, BIT) - query(l-1, BIT);
    }
    static long query(int x, long BIT[]) {
        long sum=0;
        for(int i=x; i > 0; i -= i&-i)
            sum += BIT[i];
        return sum;
    }
    static void update(int x, long val, long BIT[]) {
        for(int i=x; i < BIT.length; i += i&-i)
            BIT[i] += val;
    }
    static class query implements Comparable<query>{
        int l, r, k, id;
        public query(int l, int r, int k, int id) {
            this.l=l;
            this.r=r;
            this.k=k;
            this.id=id;
        }
        @Override
        public int compareTo(query o) {
            return k - o.k;
        }
    }
    static class pair implements Comparable<pair>{
        int val, idx;
        public pair(int val, int idx) {
            this.val=val;
            this.idx=idx;
        }
        @Override
        public int compareTo(pair o) {
            if(o.val == val)
                return idx - o.idx;
            return val - o.val;
        }
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}