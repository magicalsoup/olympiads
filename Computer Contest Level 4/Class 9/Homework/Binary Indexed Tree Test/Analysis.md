# Analysis

The first 2 operations can be easily accomplished with a normal BIT, be sure to keep the original array so you can update the correct amount to the specified index.

The last operation needs a bit of thinking, we can use another BIT, which store the frequency of each number, and thus the answer is simply the sum of all the numbers that 
are smaller than the current index, since we calculated the frequency of each number. Be sure to also update the frequency BIT when necessary.

**Time Complexity:** $`O(N \log N + M \log N)`$, or with better implementation $`O(N + M \log N)`$