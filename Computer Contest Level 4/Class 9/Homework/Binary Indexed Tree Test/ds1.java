import java.util.StringTokenizer;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
public class ds1 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt(), M = readInt();
        int a[] = new int[N+1];
        long BIT1[] = new long[N+1], BIT2[] = new long[100005];
        for(int i=1; i<=N; i++){
            a[i] = readInt();
            update(i, a[i], BIT1);
            update(a[i], 1, BIT2);
        }
        for(int i=1; i<=M; i++) {
            char com = next().charAt(0);
            if(com == 'C') {
                int x = readInt(), val = readInt();
                update(a[x], -1, BIT2);
                update(val, 1, BIT2);
                int diff = val - a[x];
                update(x, diff, BIT1);
                a[x] = val;
            }
            if(com == 'S') {
                int l = readInt(), r = readInt();
                pw.println(query(l, r, BIT1));
            }
            if(com == 'Q') {
                int v = readInt();
                pw.println(query(v, BIT2));
            }
        }
        pw.close();
    }
    static long query(int l, int r, long BIT[]) {
        return query(r, BIT) - query(l-1, BIT);
    }
    static long query(int x, long BIT[]) {
        long sum=0;
        for(; x > 0; x -= x&-x)
            sum += BIT[x];
        return sum;
    }
    static void update(int x, int val, long BIT[]) {
        for(; x < BIT.length; x += x&-x)
            BIT[x] += val;
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}