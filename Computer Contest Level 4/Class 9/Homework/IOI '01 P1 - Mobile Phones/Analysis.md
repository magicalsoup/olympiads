# Analysis

The answer is to use a 2D BIT. However, since input are 0-based, make sure add $`+1`$ to your variables. The rest is trivial.

**Time Complexity:** $`O(U \log^2(S))`$