import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
public class ioi01p1 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    static int BIT[][];
    public static void main(String[]args) throws IOException {
        readInt(); int S = readInt();
        BIT = new int[S+2][S+2];
        while(true) {
           int com = readInt();
           if(com == 3) break;
           if(com == 1) {
               int X = readInt() + 1, Y = readInt() + 1, val = readInt();
               update(X, Y, val, S);
           }
           if(com == 2) {
               int r1 = readInt() + 1, c1 = readInt() + 1, r2 = readInt() + 1, c2 = readInt() + 1;
               int tot = query(r2, c2) - query(r1-1, c2) - query(r2, c1-1) + query(r1-1, c1-1);
               pw.println(tot);
           }
        }
        pw.close();
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static void update(int x, int y, int val, int N) {
        for(int i=x; i <= N; i += i&-i)
            for(int j=y; j <= N; j += j&-j)
                BIT[i][j] += val;
    }
    static int query(int r, int c) {
        int sum=0;
        for(int i=r; i > 0; i -= i&-i)
            for (int j=c; j > 0; j -= j & -j)
                sum += BIT[i][j];
        return sum;
    }
}