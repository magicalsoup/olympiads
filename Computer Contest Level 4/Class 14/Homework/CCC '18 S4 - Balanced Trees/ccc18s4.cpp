#include <bits/stdc++.h>
using namespace std;
#define r first
#define c second
#define INF 0x3f3f3f3f;
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
unordered_map<int, ll> dp; int N;
ll solve(int n) {
    if(dp[n]) return dp[n];
    if(n == 1 || n == 2) return dp[n]=1;
    ll sum=0;
    for(int i=1; i<=sqrt(n); i++)
        sum += (n/i - n/(i+1)) * solve(i);
    for(int i=2; n/i > (int)(sqrt(n)); i++)
        sum += solve(n/i);
    return dp[n] = sum;
}
int main() {
    cin>>N;
    printf("%lld\n", solve(N));
}