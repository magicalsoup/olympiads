#include <bits/stdc++.h>
using namespace std;
#define r first
#define c second
#define INF 0x3f3f3f3f;
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
string base, funny; ll ans=1; int freq[26];
int main() {
    cin>>funny>>base;
    for(int i=0; i<funny.length(); i++)
        freq[funny[i]-'a']--;
    for(int i=0; i<base.length(); i++)
        freq[base[i]-'a']++;
    int len = base.length() - funny.length() + 1;
    for(int i=1; i<=len; i++) {
        ans = (ans % MOD * i % MOD) % MOD;
    }
    for(int i=0; i<26; i++) {
        if(freq[i] < 0) {
            printf("0\n");
            return 0;
        }
        for(int j=1; j<=freq[i]; j++)
            ans = divmod(ans, j, MOD);
    }
    printf("%lld\n", ans % MOD);
}