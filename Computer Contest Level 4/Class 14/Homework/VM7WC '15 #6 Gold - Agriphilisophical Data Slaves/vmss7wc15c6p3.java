import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.ArrayList;
public class vmss7wc15c6p3{
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    static ArrayList<ArrayList<Integer>> adj = new ArrayList<ArrayList<Integer>>();
    static int weight[], dis[];
    static boolean vis[];
    static int ans=-0x3f3f3f3f;
    public static void main(String[]args) throws IOException{
        int N = readInt();
        weight = new int[N+1];
        dis = new int[N+1];
        vis = new boolean[N+1];
        for(int i=0; i<=N; i++) adj.add(new ArrayList<>());
        for(int i=0; i<N-1; i++) {
            int u = readInt(), v = readInt();
            adj.get(u).add(v);
        }
        for(int i=1; i<=N; i++) {
            weight[i] = readInt();
            ans = Math.max(weight[i], ans);
        }
        dfs(1);
        System.out.println(ans);
    }
    static int dfs(int u) {
        int sum=weight[u];
        vis[u]=true;
        for(int v : adj.get(u)) {
            if (!vis[v]) {
                sum += dfs(v);
            }
        }
        ans = Math.max(sum, ans);
        return dis[u] = sum;
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}