#include <bits/stdc++.h>
using namespace std;
#define r first
#define c second
#define INF 0x3f3f3f3f;
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
int N, M; set<int> adj[100005];
int main() {
    cin>>N>>M;
    for(int i=0; i<M; i++) {
        int u, v; cin>>u>>v;
        adj[u].insert(v);
    }
    ll ans=0;
    for(int i=1; i<=N; i++) {
        if(adj[i].empty()) continue;
        ans += adj[i].size(); int v = *adj[i].begin();
        adj[i].erase(adj[i].begin()); 
        if(adj[i].size() > adj[v].size()) swap(adj[i], adj[v]);
        adj[v].insert(adj[i].begin(), adj[i].end());
    }
    printf("%lld\n", ans);
}