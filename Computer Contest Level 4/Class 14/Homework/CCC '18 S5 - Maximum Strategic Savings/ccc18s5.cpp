#include <bits/stdc++.h>
using namespace std;
#define INF 1L<<60;
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MAXN = 100005, MAXM = 1000005;
struct edge {
    int a, b, w; bool id;
};vector<edge> edges;
bool cmp(edge a, edge b) {
    return a.w < b.w;
}
int N, M, P, Q, pl[MAXN], cl[MAXM], pleft, cleft;
int find(int d, int par[]) {
    if(d != par[d]) par[d] = find(par[d], par);
    return par[d];
}
void init() {
    for(int i=0; i<=N; i++) pl[i]=i;
    for(int i=0; i<=M; i++) cl[i]=i;
}
bool merge(int u, int v, int par[]){
    int fu = find(u, par), fv = find(v, par);
    if(fu == fv) return false;
    par[fu] = fv;
    return true;
}
int main() {
    scanf("%d %d %d %d", &N, &M, &P, &Q);
    init(); ll mst=0, tot=0;
    for(int i=0, u, v, w; i<P; i++) {
        scanf("%d %d %d", &u, &v, &w);
        tot += 1ll * w * N;
        edges.push_back({u, v, w, false});
    }
    for(int i=0, u, v, w; i<Q; i++) {
        scanf("%d %d %d", &u, &v, &w);
        tot += 1LL * w * M;
        edges.push_back({u, v, w, true});
    }
    sort(edges.begin(), edges.end(), cmp);
    int pleft = N, cleft = M;
    for(edge e : edges) {
        int *par = e.id? pl : cl;
        if(merge(e.a, e.b, par)) {
            if(e.id) {
                pleft--;
                mst += 1LL * e.w * cleft;
            }
            else {
                cleft--;
                mst += 1LL * e.w * pleft;
            }
        }
    }
    printf("%lld\n", tot - mst);
}