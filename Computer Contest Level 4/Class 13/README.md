# Sparse Table

- Sparse Table is a dat structure taht answers static Range Minimum Query (RMQ)
- It is recognized for itsrelatively fast query and short implementation compared to other data structures
- Apllications:
    - Range minimum query(Range min / max / sum query)
    - Lowest Common Ancestor Query (LCA)

## The RMQ Problem
- The Range Minimum Query problem (RMQ for short) is the following

- Given an arrray A and two incidces $`i \le j`$, what is the smallest element out of $`A[i], A[i+1], \cdots, A[j-1], A[j]`$?

- Naive way: no preprocessing, $`O(n)`$ query
- Cache everything: $`O(n^2)`$ preprocessing, $`O(1)`$
- Segment Tree:  $`O(n \log n)`$ preprocessing, $`O(\log n)`$ query
- Spare Table: $`O(n \log n)`$ preprocessing, $`O(1)`$ query

## The Intuition
- If we precompute the answer over too many ranges, the preprocessing time will be too large.
- If we precompute the answers over too few ranges, the query time wont be $`O(1)`$
- Precompute RMQ over set of ranges such that are fewer than $`O(n^2)`$ total range, but there are enough ranges to support $`O(1)`$ query

## The Approach
- For each index $`i`$, compute RMQ for ranges staring at $`i`$ of size $`1, 2, 4, 8 ,16 \cdots 2^k`$ a long as they fit in the array.
    - Gives both large and small ranges starting at any point in the array.
    - Only $`O(log n)`$ ranges computed for each array element.
    - Total number of ranges: $`O(n \log n)`$
- Claim: any range in the array can be formed

## Precomputing the Ranges
- there are $`O(n \log n)`$ ranges to precompute
- Using dynamic programming, we can compute all of them in time $`O(n \log n)`$.
- $`st[i][j] = \min(st[i-1][j], st[i-1][j+(1<<(i-1))]`$

## Query
- query: $`\min(ST[K][L], st[K][R-2^k+1)]`$
- To answer $`RMQ_A (i, j)`$:
- Find the largest $`k`$ such that $`2^k \le j- i + 1`$.
    - With the right preprocessing, this can be done in $`O(1)`$ time, you'll figure out how in the prolem set
- The range $`[i, j]`$ can be formed as the overlap of the ranges $`[i, i+2^k-1]`$ and $`[j-2^k + 1, j]`$
- each range can be looked up in $`O(1)`$ time
- Total time: $`O(1)`$


## Lowest Common Ancestor 
- Get **Euler tour: ** and the depth
- Do sparse table

## Summary
- sparse table is a data stucture, which can efficiently answer RMQ query
- LCA problem can be converted to a RMQ prolem on the Euler tour sequence
- By using sparse table, we can get O (n log n) preprocessing and O(1) querying time for both RMQ and LCA


