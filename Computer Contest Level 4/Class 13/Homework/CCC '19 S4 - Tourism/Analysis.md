# Analysis

## First/Second Subtask
To get the first $`6`$ points, we apply a simple dp, our transition being

For each attraction $`[1,i]`$, what is the best answer? Let $`dp[i]`$ be that. 

For each day, we must take at least at number in the range: $`\Bigl[i-K, \Bigl\lceil \dfrac{i+k}{k} \Bigr\rceil  \times K \Bigr]`$

This our dp transition would be $`dp[i] = max(dp[i], dp[j] + RMQ(j+1, i))`$, where $`i-K \le j \le \Bigl\lceil \dfrac{i+k}{k} \Bigr\rceil  \times K`$, and RMQ = `Range Maximum Query`.

This is good enouh to get $`6`$ out of the $`15`$ points.

**Time Complexity:** $`O(N^2 + N \log N)`$.

## Full Solution
However, we can use the **montonos property** to prove that the dp values are always increasing. This means if we were to increase $`j`$, we should never decrease $`j`$. That means we only 
consider each state at most $`2N`$ times. Thus reducing our time complexity

**Time Complexity:** $`O(N)`$