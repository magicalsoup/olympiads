#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
int W, D, V, d[12]; set<int> s1;
int main() {
  scanf("%d %d", &W, &D);
  for(int i=0; i<D; i++) {
    scanf("%d", &d[i]);
    s1.insert(d[i]);
  }
  while(W--) {
    set<int> s2;
    for(auto e : s1) {
      for(int j=0; j<D; j++) {
        s2.insert(e + d[j]); s2.insert(e * d[j]);
      }
    }
    s1 = s2;
  }
  scanf("%d", &V);
  while(V--) {
    int x; scanf("%d", &x);
    printf("%s\n", s1.count(x)? "Y" : "N");
  }  
}