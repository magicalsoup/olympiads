#include <bits/stdc++.h>
using namespace std;
#define INF 1L<<60;
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MAXN = 5005;
struct Node {
    int id, par, depth;
} nodes[MAXN];
bool cmp(Node a, Node b) {
    return a.depth > b.depth;
}
int N, S, idx[MAXN]; bool vis[MAXN], mar[MAXN];
vector<int> adj[MAXN];
void getDepth(int u, int d) {
    nodes[u].depth = d++;
    for(int v : adj[u]) {
        if(nodes[v].depth == 0) {
            nodes[v].par = u;
            getDepth(v, d);
        }
    }
}
int getCenter(int u, int r) {
    if(r == 0) return u;
    return getCenter(nodes[idx[u]].par, r-1);
}
void mark(int u, int r) {
    if(r == -1) return;
    vis[u] = true;
    mar[u]=true;
    for(int v : adj[u]) {
        if(!mar[v]) mark(v, r - 1);
    }
}
bool check(int mid) {
    int cnt=0;
    memset(vis, false, sizeof(vis));
    for(int i=1; i<=N; i++) {
        if(!vis[nodes[i].id]) {
            memset(mar, false, sizeof(mar));
            int center = getCenter(nodes[i].id, mid);
            mark(center, mid);
            cnt++;
        }
    }
    return cnt <= S;
}
int main() {
    scanf("%d %d", &N, &S);
    for(int i=0, u, v; i<N-1; i++) {
        scanf("%d %d", &u, &v);
        adj[u].push_back(v);
        adj[v].push_back(u);
    }
    for(int i=1; i<=N; i++) {
        nodes[i].depth=0; nodes[i].id=i; nodes[i].par=1;
    }
    getDepth(1, 1);
    sort(nodes+1, nodes+N+1, cmp);
    for(int i=1; i<=N; i++) {
        idx[nodes[i].id] = i;
        // printf("node id: %d, i: %d\n", nodes[i].id+1, i+1);
    }
    int lo=0, hi=N, ans=0x3f3f3f3f;
    while(lo <= hi) {
        int mid = lo+(hi-lo)/2;
        if(check(mid)) {
            ans = min(ans, mid);
            hi = mid-1;
        }
        else lo = mid+1;
    }
    printf("%d\n", lo);
}