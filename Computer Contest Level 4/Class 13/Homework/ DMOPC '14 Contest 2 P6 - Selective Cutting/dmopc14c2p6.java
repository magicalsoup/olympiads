import java.util.StringTokenizer;
import java.util.Arrays;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class dmopc14c2p6 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    static long BIT[];
    public static void main(String[]args) throws IOException{
        int N = readInt(); pair a[] = new pair[N];
        BIT = new long[2020005];
        for(int i=0; i<N; i++)
            a[i] = new pair(readInt(), i+1);
        Arrays.sort(a);
        int Q = readInt();
        query queries[] = new query[Q]; long ans[] = new long[Q];
        for(int i=0; i<Q; i++) {    
            int u = readInt(), v = readInt(), q = readInt();
            queries[i] = new query(u+1, v+1, q, i);
        }
        Arrays.sort(queries);
        int qidx=0, aidx=0;
        while(qidx < Q) {
            if(aidx >= N || queries[qidx].q > a[aidx].h) {
                ans[queries[qidx].id] = query(queries[qidx].l, queries[qidx].r);
                qidx++;
            }
            else {
                update(a[aidx].id, a[aidx].h);
                aidx++;
            }
        }
        for(int i=0; i<Q; i++)
            System.out.println(ans[i]);
    }
    static void update(int x, int val) {
        for(int i=x; i<BIT.length; i+=i&-i)
            BIT[i] += val;
    }
    static long query(int x) {
        long sum=0;
        for(int i=x; i>0; i-=i&-i)
            sum += BIT[i];
        return sum;
    }
    static long query(int l, int r) {
        return query(r) - query(l-1);
    }
    static class pair implements Comparable<pair> {
        int h, id;
        public pair(int h, int id) {
            this.h=h;
            this.id=id;
        }

        @Override
        public int compareTo(pair o) {
            return o.h - h;
        }
    }
    static class query implements Comparable<query>{
        int l, r, q, id;
        public query(int l, int r, int q, int id) {
            this.l=l;
            this.r=r;
            this.q=q;
            this.id=id;
        }
        @Override
        public int compareTo(query o) {
            return o.q - q;
        }
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static long readLong() throws IOException {
        return Long.parseLong(next());
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}