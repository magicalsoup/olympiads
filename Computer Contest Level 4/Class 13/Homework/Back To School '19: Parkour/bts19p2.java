import java.util.Scanner;
public class bts19p2 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int X = sc.nextInt(), Y = sc.nextInt(), H = sc.nextInt(), V = sc.nextInt(), T = sc.nextInt();
        for(int i=1; i<T; i++) {
            int l=X, r=X+H-1;
            l = Math.max(l, 3*i-(Y+V-1));
            r = Math.min(r, 3*i-Y);
            if(r < l || (l>2*i || r<i)) continue;
            System.out.println("YES");
            return;
        }
        System.out.println("NO");
        sc.close();
    }
}