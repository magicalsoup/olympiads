import java.util.StringTokenizer;
import java.util.Scanner;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
public class mwc15c2p2 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt();
        int top=-1;
        pair a[] = new pair[N];
        for(int i=0; i<N; i++) {
            int h = readInt();
            int ic=0;
            while(top >= 0 && a[top].first <= h)
                top--;
            if(top >= 0) ic = i - a[top].second;
            else if(i!=0) ic=i;
            a[++top] = new pair(h, i);
            pw.print(ic + " ");
        }
        pw.close();
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static class pair{
        int first, second;
        public pair(int first, int second){
            this.first=first;
            this.second=second;
        }
    }
}