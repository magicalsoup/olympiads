import java.util.Scanner;
public class dwite07c5p2 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        for(int testcases=1; testcases<=5; testcases++) {
            int N = sc.nextInt(); int cnt=0;
            if(isPrime(N)) cnt--;
            for (int i = 2; i <= N; i++) {
                while(N%i==0){
                        cnt++;
                        N/=i;
                }
            }
            System.out.println(cnt);
        }
        sc.close();
    }
    static boolean isPrime(int x) {
        if(x <= 1) return false;
        if(x==2) return true;
        for(int i=2; i<=Math.sqrt(x); i++)
            if(x%i==0) return false;
        return true;
    }
}
