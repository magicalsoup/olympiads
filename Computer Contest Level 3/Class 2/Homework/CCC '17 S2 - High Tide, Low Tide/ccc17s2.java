import java.util.Arrays;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
public class ccc17s2 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException{
        int N = readInt();
        int tides[] = new int[N+1];
        for(int i=1; i<=N; i++)
            tides[i] = readInt();
        Arrays.sort(tides);
        if(N%2 == 0) {
            int low = N/2;
            int hi = N/2+1;
            for (int i = 1; i <= N / 2; i++) {
                System.out.print(tides[low] + " " + tides[hi] + " ");
                low--;
                hi++;
            }
        }
        else {
            int low = N/2+1;
            int hi=N/2+2;
            for(int i=1; i<=N/2; i++) {
                System.out.print(tides[low] + " " + tides[hi] + " ");
                low--;
                hi++;
            }
            System.out.print(tides[low]);
        }
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}