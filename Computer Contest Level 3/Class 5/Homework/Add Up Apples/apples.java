import java.util.*;
import java.io.*;
public class apples {
	static Scanner br;
	static PrintWriter out;
	static long ways = 0;
	public static void main(String[]args)throws IOException {
		Scanner br = new Scanner(System.in);
		int n = br.nextInt();
		solve(n, n, 1, new ArrayList<Integer>());
		System.out.println("total="+ways);
		br.close();
	}
	static void solve(int start, int n, int min, ArrayList<Integer> l){
		ArrayList<Integer> newL = new ArrayList<Integer>();
		newL.addAll(l);
		for(int i = min; i <= n / 2; i++) {
			newL.add(i);
			solve(start, n - i, i, newL);
			newL.remove(newL.size() - 1);
		}
		if(!l.isEmpty()) {
			System.out.print(start + "=");
			for(int i : l) System.out.print(i + "+");
			System.out.println(n); 
			ways++;
		}
	}  
}