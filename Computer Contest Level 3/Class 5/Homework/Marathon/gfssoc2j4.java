import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
public class gfssoc2j4 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    public static void main(String [] args) throws IOException{
        int N = readInt(), Q = readInt();
        int psa[] = new int[N+1];
        for(int i=1; i<=N; i++) {
            psa[i] = readInt();
            psa[i] += psa[i-1];
        }
        int tot = psa[N];
        for(int i=0; i<Q; i++) {
            int a= readInt(), b = readInt();
            int ans = tot - psa[b] + psa[a-1];
            pw.println(ans);
        }
        pw.close();
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}