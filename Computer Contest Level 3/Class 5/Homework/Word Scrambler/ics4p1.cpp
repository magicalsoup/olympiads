#include<bits/stdc++.h>
using namespace std;
int T; 
string s;
void recur(string s, int left, int right, vector<string> &ans){
  if(left == right) {
    ans.push_back(s);
    return;
  }
  for(int i=left; i<=right; i++) {
    swap(s[left], s[i]);
    recur(s, left+1, right, ans);
    swap(s[left], s[i]);
  }
}
int main() {
    cin>>s;
    vector<string>ans;
    recur(s,0, s.length()-1, ans);
    sort(ans.begin(), ans.end());
    for(string i : ans)
      cout<<i<<endl;
    return 0;
}