import java.util.Arrays;
import java.util.Scanner;
public class nccc2j3s1 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        long sum=0;
        int N = sc.nextInt(), a[] = new int[N+1];
        for(int i=1; i<=N; i++) {a[i] =sc.nextInt();sum+=a[i];}
        Arrays.sort(a);
        int max=a[N];
        System.out.println(Math.min(sum - max, sum/2));
        sc.close();
    }
}