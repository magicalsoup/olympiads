# Analysis
This is the official editorial on DMOJ by **xiaowuc1**.

The answer is 

```math
\displaystyle\min\left(\sum_i a_i - \max_i a_i, \left\lfloor \frac{\sum_i a_i}{2} \right \rfloor\right)
```

The left expression is an upper bound on the answer because if we consider the $`n−1`$ smallest elements, each operation has to decrease at least one of those entries.

The right expression is an upper bound on the answer since the sum of the list decreases by $`2`$ per operations and cannot be negative.

If the left expression bounds the answer, then the optimal arrangement is to always decrease the maximal element once and force the other elements to zero.

If the right expression bounds the answer, we can simultaneously decrease the smallest and largest element and repeat this until the list contains at most one $`1`$.

Make sure to use a 64-bit data type to store the answer

**Time Complexity:** $`O(N)`$