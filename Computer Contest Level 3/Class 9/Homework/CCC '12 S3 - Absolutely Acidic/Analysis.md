# Analysis

Use a Map to find frequency of each number. Then create an object to store that number and its frequency. Sort the list in descending order according to its frequency, break ties 
by comparing their values. Then loop through, keep 2 different variables, first and second. According to the rules in the problem statement, break ties and store the answers
in the 2 variables. Output the maximum answers or the only answer wherever approiate.

**Time Complexity** $`O(N \log N)`$