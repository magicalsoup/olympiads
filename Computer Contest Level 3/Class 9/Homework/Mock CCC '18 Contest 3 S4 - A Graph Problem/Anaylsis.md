# Analysis

This problem is pretty much the same thing as the array problem in the homework. Simply check if the maximum element is smaller than the sum of the rest of the elements and that if
the sum is even.

Use a 64-bit data-type to avoid overflow.

**Time Complexity:** $`O(N)`$