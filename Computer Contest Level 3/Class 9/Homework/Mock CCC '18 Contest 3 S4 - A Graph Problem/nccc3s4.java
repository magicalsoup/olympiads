import java.util.Scanner;

public class nccc3s4 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        long sum=0, max=0;
        int a[] = new int[N+1];
        for(int i=1; i<=N; i++) {
            a[i] = sc.nextInt();
            sum += a[i];
            max = Math.max(max, a[i]);
        }
        if(sum % 2 == 0 && max <= sum - max) System.out.println("YES");
        else System.out.println("NO");
    }
}