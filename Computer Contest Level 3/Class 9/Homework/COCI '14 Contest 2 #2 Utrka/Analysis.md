# Analysis

Simply use a HashMap to keep track the occurrence of each name, find the name with a odd number of occurrence, that is our answer.

**Time Complexity:** $`O(N \log N)`$