import java.util.StringTokenizer;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.PrintWriter;
public class dmopc17c5p2 {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    public static void main(String[]args) throws IOException {
        String s = next();
        int pre[][] = new int[2][s.length()+1];
        for(int i=1; i<=s.length(); i++) {
            if(s.charAt(i-1) == '0') pre[0][i]++;
            if(s.charAt(i-1) == '1') pre[1][i]++;
            pre[0][i] += pre[0][i-1];
            pre[1][i] += pre[1][i-1];
        }
        int Q = readInt();
        for(int i=0; i<Q; i++) {
            int x = readInt(), y = readInt(), z = readInt();
            int idx = lower_bound(y, pre[z], x, s.length()+1, x);
            pw.println(idx == s.length()+1? -1: idx);
        }
        pw.close();
    }
    static int lower_bound(long key, int arr[], int low, int high, int start) {
        while(low < high){
            final int mid = low + (high - low) / 2;
            if(key > arr[mid] - arr[start-1])
                low = mid+1;
            else
                high = mid;
        }
        return low;
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
}