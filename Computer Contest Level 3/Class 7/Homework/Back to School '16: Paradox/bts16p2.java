import java.util.*;
public class bts16p2 {
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        LinkedList<Boolean> st = new LinkedList<>();
        int C = sc.nextInt();
        while(C-- >0) {
            int c = sc.nextInt();
            if(c == 1) {
                boolean exp = sc.nextBoolean();
                if(st.contains(exp)) System.out.println(false);
                else {
                    System.out.println(true);
                    st.add(exp);
                }
            }
            if(c == 2) {
                boolean exp = sc.nextBoolean();
                if(!st.contains(exp)) System.out.println(false);
                else {
                    System.out.println(true);
                    st.remove(exp);
                }
            }
            if(c == 3) {
                boolean exp = sc.nextBoolean();
                if(!st.contains(exp)) System.out.println(-1);
                else System.out.println(st.indexOf(exp));
            }
            if(c == 4) {
                if(st.isEmpty()) System.out.println();
                else if(st.size() == 1) System.out.println(st.get(0));
                else System.out.println("false true");
            }
        }
        sc.close();
    }
}