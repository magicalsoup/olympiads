import java.util.Collections;
import java.util.Scanner;
import java.util.ArrayList;
public class vmss7wc16c4p2 {
    static ArrayList<ArrayList<Character>> lib = new ArrayList<ArrayList<Character>>();
    static ArrayList<String> ans = new ArrayList<String>();
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), L = sc.nextInt();
        for(int i=0; i<N; i++) lib.add(new ArrayList<>());
        for(int i=0; i<N; i++) {
            int M = sc.nextInt();
            for(int j=0; j<M; j++)
                lib.get(i).add(sc.next().charAt(0));
        }
        for(Character c : lib.get(0))
            recur(c+"", 1, L, N);
        Collections.sort(ans);
        for(String i : ans)
            System.out.println(i);
        sc.close();
    }
    static void recur(String s, int idx, int L, int N) {
        if(s.length() > L) return;
        if(idx == N) {
            ans.add(s);
            return;
        }

        recur(s, idx+1, L, N);
        for(Character c : lib.get(idx))
            recur(s + c, idx+1, L, N);
    }
}