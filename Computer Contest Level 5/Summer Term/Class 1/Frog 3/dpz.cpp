#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MM = 2e5+5;
int N, q[MM], front=0, lst=0; ll C, dp[MM], h[MM];
double slope(int j, int k) {
    return (double)(dp[k] + h[k]*h[k] - dp[j]-h[j]*h[j])/(2.0*(h[k]-h[j]));
}
int main() {
    cin >> N >> C;
    for(int i=1; i<=N; i++) cin >> h[i];
    memset(dp, 0x3f, sizeof dp);
    dp[1] = 0; q[lst++] = 1;
    for(int i=2; i<=N; i++) {
        while(front+1 < lst && slope(q[front], q[front+1]) <= h[i]) front++;
        dp[i] = dp[q[front]] + (h[i] - h[q[front]])*(h[i] - h[q[front]]) + C;
        while(front+1 < lst && slope(q[lst-2], q[lst-1]) >=  slope(q[lst-1], i)) lst--;
        q[lst++] = i;
    }
    cout << dp[N] << endl;
}
