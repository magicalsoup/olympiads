## Anaylsis

### First approach: Naive Way:

State: $`dp[i]`$ minimum cost to reach stone $`i`$

Transition: $`dp[i] = \min\{dp[j] + (h[i]-h[j])^2 + C\} \text{ for all } j < i`$

**Time Complexity:** $`n`$ states and $`O(n)`$ to calculuate all the states so total complexity is $`O(n^2)`$


### Optimization using Monotonus Property
How to optimize transition? (Here we optimize transition because the state is already a linear state and cannot be optimized further)

If we have a dp in the form: $`dp[i] = \min/\max\{dp[j] + w(j, i)\}`$
Here, the $`j`$ which gives $`dp[i]`$ best answer, $`j`$ is called the **decision point** 


Then does it optimize the monotonous property/quadrangle inequality? 

$`w(j, i) + w(j+1, i+1) \le w(j+1, i) + w(j, i+1)`$ (if it was $`\max`$, then it would be $`\ge`$)

Plug the equation in the problem into the inequality:

$`(h_i - h_j)^2 + (h_{i+1} - h_{j+1})^2 \le (h_i - h_{j+1})^2 + (h_{i+1} - h_j)^2`$

$`-2h_ih_j - 2h_{i+1}h_{j+1} \le -2h_ih_{j+1} - 2h_{i+1}h_j`$

$`0 \le h_ih_j + h_{i+1}h_{j+1} - h_ih_{j+1} - h_{i+1}h_j`$

$`0 \le h_{i+1}(h_{j+1} -h_j) - h_i(h_{j+1} - h_j)`$

$`0 \le (h_{i+1} - h_i) \times (h_{j+1} - h_j)`$

And since $`h_1 \lt h_2 \le h_3 \cdots \lt h_n`$

Therefore the inequality is always true. 

**What does this inequality tell us?**

It tells us that $`dp[i+1]`$ decision point $`\ge`$ $`dp[i]`$'s **decision point**. Basically, the **decision point** keeps increasing.

**Trick:** Simply by keeping track of the last **decision point**, we can cheese some problems using this trick, this is commongly known as `fake cht (convex hull trick)`

### Further Optimization using Convex Hull Trick

If $`j \lt k`$, and if by using `dp[j]` to calculate `dp[i]` is not as good as `dp[k]` to calculate `dp[i]`, then $`j`$ will never be used anymore

Since $`j \lt k, h_j \lt h_k`$

$`dp[j] + (h_i - h_j)^2 \ge dp[k] + (h_i -h_k)^2`$

$`dp[j] - 2h_i h_j + h_j^2 \ge dp[k] + 2h_ih_k + h_k^2`$

$`2h_i(h_k-h_j) \ge (dp[k] + h_k^2) - (dp[j] + h_j^2)`$

$`h_i \ge \dfrac{(dp[k]+h_k^2) - (dp[j] + h_j^2)}{2(h_k-h_j)} = slope(j, k)`$

This inequality means that if $`slope(j, k) \le h[i]`$, $`j`$ is worse than $`k`$ and it wont be used anymore. <br>
This also means if $`slope(j, k) > h[i]`$, $`j`$ is better than $`k`$ <br>
If $`j`$ is worse as a decision point to calculate `dp[i]`, $`j`$ won't be used to calculuate `dp[i+1], dp[i+2] ... `

This inequaltiy can also be interpreted as a slope function, and here we have an introduction to **convex hull trick**.

To main this hull, we can use a monotonic deque.

```
while dq has at least two elements && slope(dq[0], dq[1]) <= h[i]) :
    pop the front of the deque

decision_point = dq[0] 
dp[i] = dp[decision_point] + (h[i] - h[decision_point])^2 + C // here we made a O(1) transition


// lets say we have the deque{j, k, l, ... ,a, b} and we want to push i into the deque
// here lets make the assumption slope(a, b) >= slope(b, i), and that b is useless
// for any constant h, if slope(b, i) <= h, b is worse than i as a decision point (as discuessed before), we dont use b
// if slope(a, b) >= slope(b, i) > h, then slope (a, b) > h, and a is better than b as decision point
// therefore, if this inequality is satisfed, than b is useless either way (either worse than i or worse than a)

while dq has at least two elements && slope(dq[2nd last], dp[last]) >= slope(dq[last], i) :
    pop the end of the deque

push i into the deque
```

**Time Complexity**  $`O(n)`$





