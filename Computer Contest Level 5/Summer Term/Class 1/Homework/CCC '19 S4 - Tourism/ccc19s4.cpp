#include <bits/stdc++.h>
using namespace std;
#define x first
#define y second
#define INF 0x3f3f3f3f;
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;

const int MAXN = 1000005, LOG=20;
int N, K, a[MAXN], st[LOG+5][MAXN], day[MAXN]; ll dp[MAXN], dp2[MAXN];
void build() {
    for(int i=0; i<=N; i++)
        st[0][i] = a[i];
    for(int i=1; i<=LOG; i++) {
        for(int j=1; j+(1<<i)-1<=N; j++)
            st[i][j] = max(st[i-1][j], st[i-1][j+(1<<(i-1))]);
    }
}
ll query(int l, int r) {
    int k = log2(r - l + 1);
    return max(st[k][l], st[k][r - (1<<k) + 1]);
}

int main() {
    scanf("%d %d", &N, &K);
    for(int i=1; i<=N; i++) {
        scanf("%d", &a[i]);
        day[i] = (i+K-1)/K;
    }
    build(); dp[0]=0;
    for (int i=1, j=0; i <= N; i++) {
        if(j < i-K) j = i-K;
        while(day[j] == day[j+1] && dp[j] + query(j+1, i) <= dp[j+1]+query(j+2, i)) j++;
        dp[i] = max(dp[i], dp[j] + query(j+1, i));
    }
    reverse(a+1, a+N+1);
    build(); dp2[0] = 0;
    for(int i=1, j=0; i<=N; i++) {
        if(j < i-K) j = i-K;
        while(day[j] == day[j+1] && dp2[j] + query(j+1, i) <= dp2[j+1] + query(j+2, i))j++;
        dp2[i] = max(dp2[i], dp2[j] + query(j+1, i));
    }
    printf("%lld\n", max(dp[N], dp2[N]));
}