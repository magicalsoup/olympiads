# Analysis

## First/Second Subtask
To get the first $`6`$ points, we apply a simple dp, our transition being

For each attraction $`[1,i]`$, what is the best answer? Let $`dp[i]`$ be that. 

For each day, we must take at least at number in the range: $`\Bigl[i-K, (\Bigl\lceil \dfrac {i}{k} \Bigr\rceil - 1)  \times K \Bigr]`$

The reason for that range is this, for the lower bound, we derive it from this: $`i - j \le K \implies j \ge i - K`$.

For the upper bound, we have to base our current state from all the possible attractions we could've went before today, so we take the previous day ($`\Bigl\lceil \dfrac{i}{K} \Bigr\rceil - 1`$) and multiple it by the number of attractions we can visit per day $`(K)`$ to get how many attractions we could've visited before today. 

This our dp transition would be $`dp[i] = \max(dp[i], dp[j] + RMQ(j+1, i))`$, where $`i-K \le j \le (\Bigl\lceil \dfrac {i}{k} \Bigr\rceil - 1)  \times K`$, and RMQ = `Range Maximum Query`.

This is good enouh to get $`6`$ out of the $`15`$ points.

**Time Complexity:** $`O(N^2 + N \log N)`$.

## Full Solution

### Straight-forward
We can break our dp state down:

For each day, if we maintain a prefix max and suffix max for the attractions that can be visited on that day, we can break down our RMQ function:

It then becomes $`dp[i] = \max(dp[i], dp[j] + \max(suf[j+1], pre[i]))`$, where the suffix array is from the previous day and prefix array is from the current day.

Then our dp becomes $`dp[i] = \max(dp[j] + suf[j+1], dp[j] + pre[i])`$. We can use 2 additional dynamic arrays to maintain this pair of values.

For each "day" range, we can maintain another 2 arrays to keep track of the maximum of the pair of values, then propagate each array so that the best answer is in one cell. Then when we move to another day, we can directly access that propagated cell to make our transition $`O(1)`$. Since there are only $`\Bigl\lceil \dfrac{N}{K} \Bigr\rceil`$ such sections, and there are $`K`$ days, this propagation takes $`O(N)`$ total time.

**Time Complexity:** $`O(N)`$


### Sketchy Solution: 
However, we can use the **montonos property** to prove that the dp values are always increasing. This means if we were to increase $`j`$, we should never decrease $`j`$. That means we only 
consider each state at most $`2N`$ times. Thus reducing our time complexity

Note: for this approach, you also have to do it backwards a second time and take the best answer due to our transition.

**Time Complexity:** $`O(N)`$
