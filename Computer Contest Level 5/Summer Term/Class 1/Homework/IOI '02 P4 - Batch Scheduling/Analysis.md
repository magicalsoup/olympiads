## Analysis

Some deal, we first do the naive way. Now this question is a bit confusing so the naive way may have been a bit hard to come up wth.

Let dp[i] be the minimum time to complete the jobs from $`i`$ to $`N`$. 


Now $`dp[i] = \min\{dp[j] + (S + sumT(i, j)) \times sumF(i, N)\}`$.

The transition works like this, since a previous batch affects all **later** batches, we have to add the time for the current batch to all the later batches.

As for the cost of a single task, the math works out like this:

$`= (S + T_i + T_{i+1} + \cdots + T_k) \times F_i + (S + T_i + T_{i+1} + \cdots + T_k) \times F_{i+1} + \cdots + (S + T_i + T_{i+1} + \cdots + T_k) \times F_{i+k}`$

$`= (S + T_i + T_{i+1} + \cdots + T_k) \times (F_i + F_{i+1} + \cdots F_{i+k})`$

$`= (S + sumT(i, k)) \times sumF(i, k)`$

Now we use the online judge to see if it satisfies the monotonos property, and it does.

Now we set up the equation to determine the slope of our function.

$`dp[j] + (S + t_i - t_j) \times f_i \le dp[k] + (S + t_i - t_k) \times f_i \text{ for all } k \lt j`$ (Notice this because we loop backwards due to our transition)

$`dp[j] - dp[k] \le t_if_i - t_kf_i`$

$`\dfrac{dp[j] - dp[k]}{t_j - t_k} \le f_i = slope(j, k)`$


You can now maintain this with a monotonous deque and we have optimized our transition.

**Time Complexity** $`O(n)`$