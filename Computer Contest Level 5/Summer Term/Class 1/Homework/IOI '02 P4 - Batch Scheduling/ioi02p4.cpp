#include <bits/stdc++.h>
using namespace std;
int N, S, t[10005], f[10005], dp[10005];
double slope(int j, int k) {
    return (dp[j] - dp[k])/(double)(t[j] - t[k]);
}
int main() {
    cin >> N >> S;
    for(int i=1; i<=N; i++) cin >> t[i] >> f[i];
    for(int i=N; i>0; i--) {
        t[i] += t[i+1]; f[i] += f[i+1];
    }
    memset(dp, 0x3f, sizeof dp); dp[N+1] = 0;
    // dp[i] is the total min cost from i to N
    deque<int> dq; dq.push_back(N+1);
    for(int i=N; i>0; i--) {

        while(dq.size() >= 2 && slope(dq[0], dq[1]) <= f[i]) dq.pop_front();
        int pt = dq[0];
        dp[i] = dp[pt] + (S + t[i] - t[pt]) * f[i];
        while(dq.size() >= 2 && slope(dq[(int)dq.size()-2], dq.back()) >= slope(dq.back(), i)) dq.pop_back();
        dq.push_back(i);
    }
    cout << dp[1] << endl;
}
/**

dp[j] + (S + t[i] - t[j]) * f[i] <= dp[k] + (S + t[i] - t[k]) *f[i]
dp[j] + Sf[i] + t[i]f[i] - t[j]f[i] <= dp[k] + Sf[i] +t[i]f[i] - t[k]f[i]
dp[j] - dp[k] <= t[j]f[i] - t[k]f[i]
(dp[j] - dp[k])  <= f[i](t[j] - t[k])
(dp[j] - dp[k]) / (t[j] - t[k]) <= f[i]
**/