## Analysis

We just need to realize a simple fact, if the probability of you winning is a certain percent $`p`$, then on the next turn the probablity of the opponent winning is $`1 - p`$. 

With this in mind, we can use state compressing and bfs to solve this problem. 

First we compress the grid into a single mask (base 3), for example a grid (`0` is used for empty spaces (`_`), `1` is used for `J` nd `2` is used for `D`):

```
JJ_D
DDDJ
```

Becomes `11022221`.

Now we just bfs, for each state we go to the 4 different directions, recursively find that probability and add that to a list.
At the end of each recursive function, we sort the list by highest probability and sum as many as we can ($`\min(\text{size of probabilities}, \text{ the player's error factor}))`$. And return that probablity.

The hard part of this problem is the implementation.

**Time Complexity:** $`(3^{RC} \times 8)`$


