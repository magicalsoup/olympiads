#include <bits/stdc++.h>
using namespace std;
#define boost() cin.tie(0); cin.sync_with_stdio(0)
#define debug(...) " [" << #__VA_ARGS__ ": " << (__VA_ARGS__) << "]"
typedef long long ll;
const int MM = 1e5+5;
int N, K, par[205][MM]; ll psa[MM]; ll dp[2][MM];
// psa[i] <= psa[N] + (dp[pre][j] - dp[pre][k]) / (psa[j]-psa[k])
double slope(int j, int k, int id) {
    if(psa[j] == psa[k]) return -1.0e20;
    return psa[N] - (dp[id][j] - dp[id][k]) / (double)(psa[j] - psa[k]);
}
int main() {
    cin >> N >> K;
    for(int i=1; i<=N; i++) { cin >> psa[i]; psa[i] += psa[i-1];}
    int pre=0, cur=1;
    for(int k=1; k<=K; k++) {
        memset(dp[cur], 0, sizeof dp[cur]);
        deque<int> dq; dq.clear(); dq.push_back(0);
        for(int i=1; i<=N; i++) {
            while(dq.size() >= 2 && slope(dq[0], dq[1], pre) <= psa[i]) dq.pop_front();
            int pt = dq[0];
            dp[cur][i] = dp[pre][pt] + (psa[i] - psa[pt])*(psa[N] - psa[i]);
            par[k][i] = pt;
            //cout << pt << " " << dp[cur][i] << endl;
            while(dq.size() >= 2 && slope(dq[(int)dq.size()-2], dq.back(), pre) >= slope(dq.back(), i, pre)) dq.pop_back();
            dq.push_back(i);
        }
        cur ^=1; pre ^= 1;
    }
    ll ans = -1, idx=0;
    for(int i=1; i<=N; i++) {
        if(dp[pre][i] > ans) {
            ans = dp[pre][i];
            idx = i;
        }
    }
    cout << ans << endl;
    for(int i=K; i>0; i--) {
        cout << idx << " ";
        idx = par[i][idx];
    }
    cout << endl;
}
/**
dp[i] = max{dp[pre][j] + (psa[i] - psa[j]) * (psa[N] - psa[i])
dp[pre][j] + (psa[i] - psa[j])*(psa[N] - psa[i]) <= dp[pre][k] + (psa[i] - psa[k])*(psa[N] - psa[i])
dp[pre][j] - dp[pre][k] <= (psa[i] - psa[k])*(psa[N] - psa[i]) - (psa[i] - psa[j])*(psa[N] - psa[i])
dp[pre][j] - dp[pre][k] <= (psa[N] - psa[i])(psa[i] - psa[k] - psa[i] + psa[j])
dp[pre][j] - dp[pre][k] <= (psa[N] - psa[i])(psa[j] - psa[k])
(dp[pre][j] - dp[pre][k])/(psa[j]-psa[k]) <= psa[N] - psa[i]
(dp[pre][j] - dp[pre][k])/(psa[j]-psa[k]) - psa[N] <= -psa[i]
psa[N] - (dp[pre][j] - dp[pre][k])/(psa[j]-psa[k]) >= psa[i]
**/