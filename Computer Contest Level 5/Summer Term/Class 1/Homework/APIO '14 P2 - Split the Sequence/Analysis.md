## Analysis

First approach: naive way

Let `dp[k][i]` be the maximum sum on the $`k^{th}`$ split spliting at index $`i`$.

Then $`dp[k][i] = max\{dp[k][i], dp[k-1][j] + sum(j, i) \times sum(i, N)\} \text{ for all } j \lt i`$

Here this is only good for the first 4-5 subtasks. 

The first optimization we need to make is in terms of memory, as the dp table only depends on the previous row, we can use a "flip array" to save a dimension of memory. Implementation is left as an exercise to the reader.

In addition we can use a parent array to output the indexes of the splits, simplying updating them when updating your dp. Implementation is left as an exercise to the reader.


Now we can start to optimize the dp. As any similar convex hull trick problem, we first test out the monotonous property by examining if the decision point for each $`i`$ is always increasing.
It turns out it is for this problem. Implementation is left as an exercise to the reader.

Now lets determine the inequality for which $`j`$ is worse than $`k`$ as a decision point if $`j \lt k`$. Here we do not care about the 1 first dimension as it does not affect this inequality.
Assume we are using the previous row for the dp in this inequality. In addition, we are using a prefix sum array to quickly get the sum of a range.

$`dp[j] + (psa[i] - psa[j])(psa[N] - psa[i]) \le dp[k] + (psa[i] - psa[k])(psa[N] - psa[i])`$

$`dp[j] + psa[i]psa[N] - psa[i]^2 - psa[j]psa[N] + psa[i]psa[j] \le dp[k] + psa[i]psa[N] - psa[i]^2 -psa[k]psa[N] + psa[i]psa[k]`$

$`dp[j] - psa[j]psa[N] + psa[i]psa[j] \le dp[k] - psa[k]psa[N] + psa[i]psa[k]`$

$`dp[j] + psa[j](psa[i] - psa[N]) \le dp[k] + psa[k](psa[i] - psa[N])`$

$`dp[j] - dp[k] \le (psa[k] - psa[j])(psa[i] - psa[N])`$

$`\dfrac{dp[j] - dp[k]}{psa[k] - psa[j]} \le psa[i] - psa[N]`$

$`psa[N] + \dfrac{dp[j] - dp[k]}{psa[k] - psa[j]} \le psa[i] = slope(j, k)`$

Now we can use this inequality to maintain our monotonous deque and solve the problem. Keep in mind that since $`0`$'s may be present in the array, `psa[k]` could equal `psa[j]` in which case you would want to return a really big negative value.

**Time Complexity:** $`O(nk)`$


