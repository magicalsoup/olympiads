## Analysis

Here we follow the same approach as frog 3.

First we do the naive way:

The problem is simply asking for to split the array into subarrays and apply a function to the sum of those subarrays.

Thus we by using a prefix sum array, we can easily derive the state and transition.

State: $`dp[i]`$ = maximum sum for the array from $`[1, i]`$

Transition: $`dp[i] = \max\{dp[j] + f(sum(j, i))\} \text{ for all } j \lt i`$ where $`sum(j, i)`$ is the sum of the array from $`[j, i]`$ and $`f(x)`$ is the function described in the problem statement.

And now, we can prove that it satisfies the monotonous property described in frog 3. And for this question we only need to keep track of the last used index, no further optimization is needed.


**Time Complexity:** $`O(n)`$ but is more close to $`O(n \log n)`$