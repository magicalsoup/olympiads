#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MM = 2e6+5;
int N, L; ll dp[MM], psa[MM], x[MM], y[MM];
double slope(int j, int k) {
    return (dp[j] - dp[k] + y[j]*y[j] - y[k]*y[k]) / (double)(y[j] - y[k]);
}
int main() {
    cin >> N >> L; y[0] = 1 + L;
    for(int i=1; i<=N; i++) {
        cin >> psa[i]; psa[i] += psa[i-1];
        x[i] = psa[i] + i; y[i] = psa[i] + i + 1 + L;
    }
    deque<int> dq; dq.push_back(0);
    for(int i=1; i<=N; i++) {
        while(dq.size() >= 2 && slope(dq[0], dq[1]) <= 2LL*x[i]) dq.pop_front();
        int pt = dq[0];
        dp[i] = dp[pt] + (x[i] - y[pt])*(x[i] - y[pt]);
        //cout << pt << " " << dp[i] << endl;
        while(dq.size() >= 2 && slope(dq[(int)dq.size()-2], dq.back()) >= slope(dq.back(), i)) dq.pop_back();
        dq.push_back(i);
    }
    cout << dp[N] << endl;
}
/**
dp[i] = min({dp[j] + (x-L)^2}); for all j < i
dp[j] + (x[i] - y[j])^2 <= dp[k] + (x[i] - y[k])^2
dp[j] - dp[k] <= -(x[i]^2 - 2x[i]y[j] + y[j]^2) + (x[i]^2 - 2x[i]y[k] + y[k]^2)
dp[j] - dp[k] <= -x[i]^2 + 2x[i]y[j] - y[j]^2 + x[i]^2 - 2x[i]y[k] + y[k]^2
dp[j] - dp[k] <= 2x[i]y[j] - y[j]^2 - 2x[i]y[k] + y[k]^2
dp[j] - dp[k] <= y[j](2x[i] - y[j]) - y[k](2x[i] - y[k])
dp[j] - dp[k] + y[j]^2 - y[k]^2 <= 2x[i](y[j] - y[k])
(dp[j] - dp[k] + y[j]^2 - y[k]^2) / (y[j] - y[k])  <= 2x[i]
**/