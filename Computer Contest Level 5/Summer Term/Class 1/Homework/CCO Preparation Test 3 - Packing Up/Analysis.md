## Analysis

To make our lives easier later on, we are going to define two more functions.

Let $`X[i] = psa[i] + i`$ and $`Y[i] = psa[i] + i + 1 + L`$.

This way, when we do our transition, our function would be $`(X[i] - Y[i])^2`$. Here, there is a $`+1`$ in `Y[i]`'s because how prefix sum arrays are inclusive. For a
summation in a range there it would be $`psa[r] - psa[l-1]`$, then the indexes would be $`r-(l-1)`$ which would be and such we need to subtract another $`1`$ to get $`r -l`$ when we subtract `y[i]` from `x[i]`
Also, due to this, we define $`Y[0] = 1 + L`$. (Why this is true is left as an exercise to the reader)

So first approach: naive way

Let `dp[i]` be the minimum cost for the first $`i`$ books

Then $`dp[i] = \min\{dp[j] + (X[i] - Y[j])^2\} \text{ for all } j \lt i`$

**Time Complexity:** $`O(n^2)`$

To improve this, we need to check if it satisfies the monotonous property. In fact, we do not need to prove it ourselves, simply let the judge check. We keep track of the last optimal index used for each `dp[i]` and check if it is always increasing.
implementation is left as an exercise to the reader.

It turns out the function does indeed satisfiy the monotonous property, now lets make an inequality which shows that if $`j \lt k`$, $`j`$ is worse than $`k`$ as a **decision point** (explained in frog 3).

$`dp[j] + (X[i] - Y[j])^2 \le dp[k] + (X[i] - Y[k])^2`$

$`dp[j] + X[i]^2 - 2X[i]Y[j] + Y[j]^2 \le dp[k] + X[i]^2 - 2X[i]Y[k] + Y[k]^2`$

$`dp[j] + Y[j]^2 - dp[k] - Y[k]^2 \le 2X[i](Y[j] -Y[k])`$

$`\dfrac{dp[j] + Y[j]^2 - dp[k] - Y[k]^2}{Y[j] - Y[k]} \le 2X[i] = slope(j, k)`$

Now we can use this function to maintain our montonous deque. Implementation is left as an exercise to the reader.

**Time Complexity:** $`O(n)`$
