#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MM = 1e6+5;
ll a, b, c, psa[MM], dp[MM]; int n;
ll cost(ll x) {
    return a*x*x + b*x + c;
}
int main() {
    cin >> n;
    cin >> a >> b >> c;
    for(int i=1; i<=n; i++) {
        cin >> psa[i]; psa[i] += psa[i-1];
    }
    int lst = 0; memset(dp, -0x3f, sizeof dp); dp[0] = 0;
    for(int i=1; i<=n; i++) {
        for(int j=lst; j<i; j++) {
            ll cur = cost(psa[i] - psa[j]) + dp[j];
            if(cur > dp[i]) {
                dp[i] = cur;
                lst = j;
            }
        }
    }
    cout << dp[n] << endl;
}