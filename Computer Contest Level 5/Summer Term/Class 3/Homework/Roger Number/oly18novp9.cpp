#include <bits/stdc++.h>
using namespace std;
int dp[11][2][2][2][10]; string k;
int fun(int pos, bool limit, bool lead, bool roger, int pre) {
    int &ret = dp[pos][limit][lead][roger][pre];
    if(ret != -1) return ret;
    if(pos == k.length()) {
        if(roger) return ret = 1;
        return ret = 0;
    }
    int bound = limit? k[pos]-'0': 9; ret = 0;
    for(int i=0; i<=bound; i++) {
        bool condition = roger && abs(i-pre) >= 2;
        if(pos == 0 || lead) condition = true;
        ret += fun(pos+1, limit && i == k[pos]-'0', lead && i == 0, condition, i);
    }
    return ret;
}
int solve(int x) {
    memset(dp, -1, sizeof dp); k = to_string(x);
    return fun(0, 1, 1, 1,0);
}
int main() {
    int l, r; cin >> l >> r;
    cout << solve(r) - solve(l-1) << endl;
}