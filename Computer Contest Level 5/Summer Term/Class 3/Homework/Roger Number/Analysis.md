## Analysis

Another straightforward dp problem. Here we can think of the first condition (the single digits) as if I had leading 0s. Then we can 
combine the first and second conditions. 

Our dp state would be: `dp[pos][limit][leading 0][is roger?][previous digit]`

**Time Complexity:** $`O(\log_{10}(R(L-1)) \times 2 \times 2 \times 2 \times 2)`$