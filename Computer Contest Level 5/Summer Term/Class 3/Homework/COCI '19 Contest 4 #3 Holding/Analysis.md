## Analysis

We need to treat this problem as a knap-sack problem. 

Here our dp state is `dp[number of items][cost]`. The crucial observation here is to note that instead of thinking to swap numbers in and out of the range, think about putting numbers into that range, of course this would also include the numbers already inside that range.

Consider we inserting the elements from left to right. Then the cost to insert an number is $`|j - (L + i-1)|`$. Where $`j`$ is position of the number to be inserted in, $`i`$ is the $`i^{th}`$ item to be inserted starting from $`L`$. 

Then our transition is $`dp[i][k] = \min(dp[i][k], dp[i-1][k-cost] + a[j])`$. Note that $`i`$'s range is $`[1, R-L+1]`$.

To prevent from choosing the same number twice or using values that aren't accessible, we need to loop backwards for $`i`$. 

**Time Complexity** $`O(NK(R-L+1))`$