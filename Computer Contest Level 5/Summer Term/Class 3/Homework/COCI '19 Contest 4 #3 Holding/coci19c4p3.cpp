#include <bits/stdc++.h>
using namespace std;
int l, r, n, K, a[10005], dp[101][10005];
int main() {
    cin >> n >> l >> r >> K;
    for(int i=1; i<=n; i++) cin >> a[i];
    memset(dp, 0x3f, sizeof dp); dp[0][0] = 0;
    for(int j=1; j<=n; j++) {
        for(int item = r-l+1; item>0; item--) {
            int cost = abs(j-(l+item-1));
            for(int k=cost; k<=K; k++) {
                dp[item][k] = min(dp[item][k], dp[item-1][k-cost] + a[j]);
            }
        }
    }
    int ans = INT_MAX;
    for(int i=0; i<=K; i++) ans = min(ans, dp[r-l+1][i]);
    cout << ans << endl;
}