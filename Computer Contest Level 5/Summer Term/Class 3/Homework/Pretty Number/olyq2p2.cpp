#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll dp[19][2520][(1<<8)];
int digit[20];

ll fun(int pos, bool limit, int mask, int rem) {
    if(!limit && dp[pos][rem][mask] != -1) return dp[pos][rem][mask];
    if(pos == 0) {
        for(int i=2; i<=9; i++)
            if((mask & 1<<(i-2)) && rem % i != 0) return 0;
        return 1;
    }
    int bound = limit? digit[pos] : 9; ll ret = 0;
    for(int i=0; i<=bound; i++) {
        int nmask = i>=2? mask | 1<<(i-2) : mask;
        ret += fun(pos-1, limit && i == digit[pos],  nmask, (rem*10 + i) % 2520);
    }
    if(!limit) dp[pos][rem][mask] = ret;
    return ret;
}
ll solve(ll x) {
    int id = 0;
    for(; x; x/=10) digit[++id] = x%10;
    return fun(id, 1, 0, 0);
}
void test_case() {
    ll l, r; cin >> l >> r;
    cout << solve(r) - solve(l-1) << endl;
}
int main() {
    memset(dp, -1, sizeof dp);
    int t; cin >> t;
    while(t--) test_case();
}