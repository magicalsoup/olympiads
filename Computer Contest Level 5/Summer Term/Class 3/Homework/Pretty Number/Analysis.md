## Analysis

Another digit dp problem. Here we do a bit of optimization with the states. 

A naive state would be `dp[pos][limit][remainder][digits used]`. This would be `dp[N][2][gcd(1,2,3...10)][(1<<10)]`.

Let $`N`$ be the number we are using for our dp. Also, $`\gcd(1, 2, 3, \cdots , 10) = 2520`$.

First, we do not care about a number being divisible by 1 or 0, since those do not need to be checked. Hence we can reduce our last state to just $`2^8`$ instead of $`2^{10}`$ states. Secondly, we actually do not need the limit as a state, we can instead manually recalculate dp whenever limit is true. Since this only requires $`\log_{10}(N)`$ manual recalculations (since its only when the digits match up with the digit of $`N`$), this will pass in time. 

The rest transition should be straight forward dp. 

In addition, to memoize, we do not need to reset the dp everytime we calculate a query. However, in order to do this, we must loop through $`N`$ backwards, as if we looped forwards, then the dp would not recongize if whether the first position (and any position) is a hundreds digit, a tenth digit, a thousandths digit, etc.

**Time Complexity:** $`O(T(\log_{10}(N) \times 2520 \times 2^8))`$