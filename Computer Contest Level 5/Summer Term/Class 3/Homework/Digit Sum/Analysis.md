# Analysis

A very straight forward digit dp problem. Simply keep track of the sum of digits $`\bmod \space D`$. And if that sum is 0, then it satisfies our condition.

**Time Complexity:** $`O(\log_{10}(K) \times D \times 2)`$