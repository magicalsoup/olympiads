#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MM = 1e4+5;
const ll mod = 1e9+7;
ll dp[MM][2][102]; string k; int D;
void add_self(ll &a, ll b) {
    a += b;
    if(a >= mod) a -= mod;
}
ll fun(int pos, bool limit, int sum) {
    ll &ret = dp[pos][limit][sum];
    if(ret != -1) return ret;
    if(pos == k.length()) {
        if(sum == 0) return ret = 1;
        else return ret = 0;
    }
    int bound = limit? k[pos]-'0' : 9; ret = 0;
    for(int i=0; i<=bound; i++) {
        add_self(ret, fun(pos+1, limit && i == k[pos]-'0', (sum + i) % D));
    }
    return ret;
}
int main() {
    memset(dp, -1, sizeof dp);
    cin >> k >> D;
    ll ans = fun(0, 1, 0);
    cout << ans-1 << endl;
}