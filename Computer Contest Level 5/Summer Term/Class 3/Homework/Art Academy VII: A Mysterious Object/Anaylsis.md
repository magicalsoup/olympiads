# Analysis

Another standard digit dp problem. 

Here our state is `dp[pos][limit][if a one appeared][number of zeroes (upto 2 only)]`

Base case is simply if we reached the end and the number zeroes is only 1, then we return 1.

**Time Complexity:** $`O(log_{10}(N) \times 2 \times 2 \times 3)`$
