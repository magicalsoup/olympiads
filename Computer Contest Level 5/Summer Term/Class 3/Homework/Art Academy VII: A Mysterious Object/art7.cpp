#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MM = 1e5+5;
const ll mod = 1e9+7;
ll dp[MM][2][2][3]; string k;
void add_self(ll &a, ll b) {
    a += b;
    if(a >= mod) a -= mod;
}
ll fun(int pos, bool limit, bool one, int cnt) {
    ll &ret = dp[pos][limit][one][cnt];
    if(ret != -1) return ret;
    if(pos == k.length()) {
        if(cnt == 1) return ret = 1;
        return ret = 0;
    }
    int bound = limit? k[pos] - '0' : 9; ret = 0;
    for(int i=0; i<=bound; i++) {
        int zero = cnt;
        if(one && i == 0) zero++;
        if(zero > 2) zero = 2;
        add_self(ret, fun(pos+1, limit && i == k[pos]-'0', one || (i == 1), zero));

    }
    return ret;
}
int main() {
    memset(dp, -1, sizeof dp);
    cin >> k;
    cout << fun(0, 1, 0, 0) << endl;
}