#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
struct Edge {int v; ll c1, c2;};
const int MM = 2e5+5, LOG = 18;
int anc[MM][LOG + 2], depth[MM], N, psa[MM]; vector<Edge> adj[MM];
ll ans = 0;
int lca(int u, int v) {
    if(depth[u] < depth[v]) swap(u, v);
    for(int i=LOG; i>=0; i--) {
        if(depth[anc[u][i]] >= depth[v]) u = anc[u][i];
    }
    if(u == v) return u;
    for(int i=LOG; i>=0; i--) {
        if(anc[u][i] != anc[v][i])
            u = anc[u][i], v = anc[v][i];
    }
    return anc[u][0];
}
void dfs(int u, int par) {
    anc[u][0] = par;
    for(int i=1; i<=LOG; i++) anc[u][i] = anc[anc[u][i-1]][i-1];
    for(auto e : adj[u]) {
        if(e.v == par) continue;
        depth[e.v] = depth[u] + 1;
        dfs(e.v, u);
    }
}
void collect(int u, int par) {
    for(auto e : adj[u]) {
        if(e.v == par) continue;
        collect(e.v, u);
        ans += min(e.c1*psa[e.v], e.c2);
        psa[u] += psa[e.v];
    }
}
int main() {
    cin >> N;
    for(int i=0; i<N-1; i++) {
        int u, v, c1, c2;
        cin >> u >> v >> c1 >> c2;
        adj[u].push_back({v, c1, c2});
        adj[v].push_back({u, c1, c2});
    }
    dfs(1, 0);
    for(int i=2; i<=N; i++) { psa[i-1]++; psa[i]++; psa[lca(i-1, i)]-=2;}
    collect(1, 0);
    cout << ans << endl;
}