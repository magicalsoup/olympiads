## Analysis

For this problem, we count the "contribution" of each edge towards the total sum of the path, and do not treat it as an directed graph.

Here we use lca (lowest common ancestor) and a prefix sum array to find the contribution of each edge.

For each $`i`$ from $`2 \le i \le N`$ (note that $`1 \le i \lt N`$ also works with a bit of tweaking), we update a path from those two consecutive numbers. That means:

```cpp
int u = i-1, v = i;
psa[u]++;
psa[v]++;
psa[lca(u, v)]-=2;
```

Then for we can do a second dfs to collect each of the values (i.e `psa[u] += psa[v]` for all $`v`$ that is connected to $`u`$).

Then we just decided to either buy the two way or one way path.

`ans += min(c2[v], c1[v]*psa[v])`

**Time Complexity:** $`O(N \log N)`$