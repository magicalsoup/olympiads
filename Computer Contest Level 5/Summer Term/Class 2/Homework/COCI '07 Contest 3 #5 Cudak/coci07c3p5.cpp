#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int S, n, a[16], b[16]; ll ans = LONG_LONG_MAX, dp[16][2][2][136]; // powers of 10
string A, B;
ll fun(int pos, bool lmt, bool rmt, int sum, ll res) { // position, lower limit, upper limit, sum, the resulting number
    if(sum > S) return 0;
    ll &ret = dp[pos][lmt][rmt][sum];
    if(ret != -1) return ret;
    if(pos == n) {
        if(sum == S) {
            ans = min(ans, res);
            return 1;
        }
        else return 0;
    }
    int lo = lmt? a[pos] : 0, hi = rmt? b[pos]: 9; ret = 0;
    for(int i=lo; i<=hi; i++) {
        ret += fun(pos+1, lmt && i==a[pos], rmt && i==b[pos], sum + i, res*10 + i);
    }
    return ret;
}
int main() {
    cin >> A >> B >> S; n = B.size(); memset(dp, -1, sizeof dp);
    for(int i=0; i<n; i++) b[i] = B[i] - '0';
    for(int i=n-1, j=A.size()-1; j>=0; i--, j--) a[i] = A[j] - '0';
    cout << fun(0, 1, 1, 0, 0) << endl;
    cout << ans << endl;
    return 0;
}