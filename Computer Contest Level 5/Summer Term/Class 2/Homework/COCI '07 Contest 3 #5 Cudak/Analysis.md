## Analysis

Another straightforward digit dp problem. This time we keep track of both the right and left bounds.

Here, we keep track (in base 10) the result of our number, and the current sum of our digits.

State is `dp[cur pos][left bound][right bound][sum of digits]`$

Base case is when we reach the end and our sum is equal to $`S`$, then we return $`1`$ and update our answer for the smallest number that satisfies that.

Transition is also standard.

**Time Complexity:** $`O(\log_{10}(N) \times 4 \times S)`$
