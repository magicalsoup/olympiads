#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll dp[19][2][10][10][2][2]; string s;
ll fun(int pos, bool limit, int pre, int ppre, bool zero, bool pzero) {
    ll&ret = dp[pos][limit][pre][ppre][zero][pzero];
    if(pos == s.length()) return ret = 1;
    if(ret != 0) return ret;
    int bound = limit? s[pos] -'0' : 9;
    for(int i=0; i<=bound; i++) {
        if((zero || i != pre) && (pzero || i != ppre))
            ret += fun(pos+1, limit && i == s[pos]-'0', i, pre, zero&&i==0, zero);
    }
    return ret;
}
ll solve(ll x) {
    memset(dp, 0, sizeof dp);
    s = to_string(x);
    return fun(0, 1, 0, 0, 1, 1);
}
int main() {
    ll l, r; cin >> l >> r;
    memset(dp, 0, sizeof dp);
    cout << solve(r) - solve(l-1) << endl;
}