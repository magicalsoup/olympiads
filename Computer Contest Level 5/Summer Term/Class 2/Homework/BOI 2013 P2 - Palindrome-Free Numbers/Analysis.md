## Analysis

Another standard digit dp problem (the general case of digit dp is explained in this week's readme file).

The condition this time is if our current digit is not equal to the previous one and the previous of the previous one, and this is true for all the digits of the number, then the number is not a palindrome. (Proof as left as an exercise to the reader, hint: this condition covers both odd and even cases of palindromes).

We also have to take in leading 0s into account, as leading 0s do not contribute to the above condition (eg 0002000) is not a palindrome.

Thus our dp state is `dp[cur pos][limit][zero][prev zero][digit][pre digit]`.

The base case is as long we reach the end of the number, then we return 1 (we will only add the non palindrome number states)

Transition is outlined in general case of digit dp.

**Time Complexity:** $`O(\log_{10}(N) \times 2^3 \times 10^2)`$