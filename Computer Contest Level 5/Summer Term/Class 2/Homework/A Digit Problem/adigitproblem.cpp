#include <bits/stdc++.h>
using namespace std;
const int mod = 1e9+7;
int N, expect, dp[1002][1<<10][2][2]; string s;
int fun(int pos, int mask, bool limit, bool zero) {
    int &ret = dp[pos][mask][limit][zero];
    if(ret != -1) return ret;
    if(pos == s.length()) return ret = ((mask & expect) == expect);
    int v = s[pos]-'0', bound = limit? v : 9; ret = 0;
    for(int i=0; i<=bound; i++) {
        int nmask = mask | (1<< i);
        if(zero && i == 0) nmask = mask;
        ret += fun(pos+1, nmask, limit && i==v, zero && i == 0);
        ret %= mod;
    }
    return ret;
}
int main() {
    cin >> N; memset(dp, -1, sizeof dp);
    for(int i=1, x; i <= N; i++) {
        cin >> x; expect |= 1 << x;
    }
    cin >> s;
    cout << fun(0, 0, 1, 1) + (expect == 1) << endl;
}