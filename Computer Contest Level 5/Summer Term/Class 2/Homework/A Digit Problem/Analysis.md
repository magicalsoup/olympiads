## Analysis

Another standard digit dp problem. 

Here our we use a mask to indicate which digits have been used (the position of the set bit indicates whether a digit has been used).
Then we just check if our required mask is equal to our number's mask (Base case).

Here we also have to take care of leading 0s (they dont count towards if a digit has been used).

Our dp state is `dp[cur pos][mask][limit][leading zero]`

Transition is standard.

Note: Don't forget to mod your answer by $`10^9 + 7`$

**Time Complexity:** $`O(\log_{10}(K) \times 2^{10} \times 4)`$
