## Analysis

A rather simple interval dp problem (should not be 20 points)

Our state is $`dp[l][r][c]`$, if I can make the string in the range $`[l, r]`$ with the variable $`c`$.

Then our transition is simple. Since it is only possible to change a single variable into a terminal, we have our base case of:

```cpp
if (l==r && rule1[u-'A'][v-'a']) // if its possible from rule1 to change our variable into a terminal
    dp[l][r][u] = true
```

Then for the other rule, we can check all the intervals

```cpp
// let the pair p be the two varialbes my current variable can become
for(int k=l; k<r; k++) {
    if(dp[l][k][p.first] && dp[k+1][r][p.second])
        dp[l][r][current_variable] = true
}
```

We can simply repeat this dp for every query.


**Time Complexity:** $`O(W(R1 + R2)VT)`$
