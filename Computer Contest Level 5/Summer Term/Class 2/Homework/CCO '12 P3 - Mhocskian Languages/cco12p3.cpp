#include <bits/stdc++.h>
using namespace std;
// dp[l][r][c] => if its possible by to make the string from l to r i with variable c
int V, T, R1, R2, W; // # of variables, terminals, V -> T, V->V1V2, of words to check
int dp[31][31][129]; // using ascii codes --> 0 for unsolved, 1 for false, 2 for true
char var[27], term[27];
map<char, vector<pair<char, char>>> vv; // variable --> variable
bool vt[26][26]; // variable --> terminal
int solve(int l, int r, char variable, string &s) {
    if(dp[l][r][variable] != 0) return dp[l][r][variable]; // if already solved, return dp
    if(l == r) { // if im at a single position and variable is equal to my string at that position
        int res = 1;
        if(s[l] == variable) res = 2;
        if(variable <= 'Z' && s[l] >= 'a' && vt[variable-'A'][s[l]-'a']) res = 2;
        return dp[l][r][variable] = res;
    }
    if(vv.count(variable)) { // if there is a rule using this variable
        for(int k=l; k<r; k++) {
            for (pair<char, char> c : vv[variable]) {
                if(solve(l, k, c.first, s) == 2 && solve(k+1, r, c.second, s) == 2) dp[l][r][variable] = 2;
            }
        }
    }
    if(dp[l][r][variable] == 0) dp[l][r][variable] = 1; // if it still hasn't been assigned a value, that it is not possible
    //cout << l << " " << r << " " << dp[l][r][variable] << endl;
    return dp[l][r][variable]; // return dp
}
int main() {
    cin >> V >> T;
    for(int i=1; i<=V; i++) cin >> var[i];
    for(int i=1; i<=T; i++) cin >> term[i];
    cin >> R1;
    for(int i=1; i<=R1; i++) {
        char a, b; cin >> a >> b;
        vt[a-'A'][b-'a'] = true;
    }
    cin >> R2;
    for(int i=1; i<=R2; i++) {
        char a, b, c; cin >> a >> b >> c;
        vv[a].push_back(make_pair(b, c));
    }
    cin >> W;
    while(W--) {
        string word; cin >> word; memset(dp, 0, sizeof dp);
        int res = solve(0, word.size()-1, var[1], word);
        assert(res != 0);
        cout << res - 1 << endl;
    }
    return 0;
}