## Analysis

First lets derive a naive solution:

let `dp[r][c][v]` be the number of ways to reach cell $`(r, c)`$ with product $`v`$. <br>
Then our transition is `dp[r][c+1][v*a[i][j]] += dp[r][c][v]` and `dp[r-1][c][v*a[i][j]] += dp[r][c][v]`. <br>
Our base case would be `dp[1][1][a[1][1]] = 1`;

Note that we should watch out memory when implementing the product and the dp array. For the product, if its over $`N`$, then just set it to $`N`$. In addition, a flip array should be used on the rows to conserve memory.

Our answer would then be in $`dp[R][S][N]`$. (this is the general form, memory optimizations would change the cell your final answer would be in).

**Time Complexity:** $`O(RSN)`$

To optimize, we change our state a little bit. 

Let `dp[r][c][k]` be the number of ways to reach cell $`(r, c)`$ with $`k`$ be the largest integer we can multply in the remaining part of the path such the product is $\lt N`$. 

The motivation to change this is that the only possible values of $`k`$ are $`\lfloor \dfrac{N-1}{i} \rfloor`$ for all $`(1 \le i \le N)`$. 
Thus the number of **distinct** integers of $`k`$ is only $`\sqrt{N}`$. 

Base case would be: `dp[1][1][(N-1)/a[1][1]] = 1`. <br>
Our transition would now be `dp[r][c][k/a[i][j]] += dp[r][c-1][k] + dp[r-1][c][k]`

Note that more memory optimization should be used here, you should use relative order for the dp array to conserve memory. Lastly, make sure not to forget to mod by $`10^9 + 7`$, and becareful of intermediate values. (watch out for overflow)

Our answer than would be in $`dp[R][S][0]`$ (Again, this is the general form, memory optimizations would change the cell your final answer would be in).

**Time Complexity:** $`O(RS\sqrt{N})`$



