#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> pii;
#define debug(...) " [" << #__VA_ARGS__ ": " << (__VA_ARGS__) << "]"
const ll mod = 1e9 + 7;
int R, S, N, a[305][305]; ll dp[2][305][2005];
vector<int> useful; int inv[1000005];
void add_self(ll &a, ll b) {
    a += b;
    if(a >= mod) a -= mod;
}
int pre = 0, cur = 1;
int main() {
    cin >> R >> S >> N; N--;
    for(int i=1; i<=R; i++) for(int j=1; j<=S; j++) cin >> a[i][j];
    for(int i=1; i<=N; i++) useful.push_back(N/i);

    sort(useful.begin(), useful.end(), greater<int>());
    useful.erase(unique(useful.begin(), useful.end()), useful.end());
    useful.insert(useful.begin(), 0);
    for(int i=0; i<useful.size(); i++) inv[useful[i]] = i;

    dp[pre][1][inv[N/a[1][1]]] = 1;
    for(int i=1; i<=R; i++) {
        memset(dp[cur], 0, sizeof dp[cur]);
        for(int j=1; j<=S; j++) {
            for(int k=0; k<useful.size(); k++) {
                if(dp[pre][j][k] == 0) continue;
                //cout << debug(w[k]) << debug(k) << debug(cnt) << endl;
                //cout << debug(dp[pre][j][id]) << debug(i) << debug(j) << debug(id) << debug(k) << endl;
                if(i != R) {
                    //cout << debug(inv[k/a[i+1][j]]) << debug(k/a[i+1][j]) << debug(a[i+1][j]) << endl;
                    add_self(dp[cur][j][inv[useful[k]/a[i+1][j]]], dp[pre][j][k]);
                }
                if(j != S) {
                    //cout << debug(inv[k/a[i][j+1]]) << debug(k/a[i][j+1]) << debug(a[i][j+1]) << endl;
                    add_self(dp[pre][j+1][inv[useful[k]/a[i][j+1]]], dp[pre][j][k]);
                }
               // cout << endl;
            }
        }
        swap(pre, cur);
    }
    cout << dp[cur][S][0] << endl;
}