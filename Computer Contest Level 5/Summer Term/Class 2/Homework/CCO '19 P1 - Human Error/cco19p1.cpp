#include<bits/stdc++.h>
using namespace std;
int R, C, err[2], mask, power[14], tot; double dp[2][1594323];
int dir[4][2] = {{1,0}, {0,1}, {-1,0}, {0,-1}};
void decode(int decoded[14], int mask) {
    int p = R*C - 1;
    for(int i=R-1; i>=0; i--)
        for(int j=C-1; j>=0; j--)
            decoded[p--] = mask % 3, mask /= 3;
}
double fun(int p, int mask) { // player, mask
    if(dp[p][mask] >= 0) return dp[p][mask];
    vector<double> prob;
    int decoded[14]; decode(decoded, mask);
    for(int i=0; i<R; i++) {
        for(int j=0; j<C; j++) {
            int cur = i*C + j;
            if(decoded[cur] != p + 1) continue;
            for(int k=0; k<4; k++) {
                int nr = i + dir[k][0], nc = j + dir[k][1], nxt = nr*C + nc;
                if(nr < 0 || nc < 0 || nr >= R || nc >= C || decoded[nxt] == 0) continue;
                int nmask = mask - decoded[cur]*power[tot-cur-1] - decoded[nxt]*power[tot-nxt-1] + decoded[cur]*power[tot-nxt-1];
                prob.push_back(1 - fun(1-p, nmask));
            }
        }
    }
    sort(prob.begin(), prob.end(), greater<double>());
    double ret = 0.0; int sz = min((int)prob.size(), err[p]);
    if(sz == 0) return dp[p][mask] = 0.0;
    for(int i=0; i<sz; i++) ret += prob[i];
    return dp[p][mask] = ret/sz;
}
int main() {
    cin >> R >> C; tot = R*C; for(int i=0; i<1594323; i++) dp[0][i] = dp[1][i] = -1.0;
    power[0] = 1; for(int i=1 ;i<tot; i++) power[i] = power[i-1]*3;
    for(int i=0; i<R; i++) {
        string s; cin >> s;
        for(int j=0; j<C; j++) {
            int val = s[j] == 'J'? 1 : (s[j] == 'D' ? 2 : 0);
            mask = mask*3 + val;
        }
    }
    cin >> err[0] >> err[1];
    double ans = fun(0, mask);
    printf("%.3f\n", ans);
}
