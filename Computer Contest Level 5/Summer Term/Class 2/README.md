## Class 2
- Digit dp

### What is digit dp?

We treat a number as a string.

Normally, our digit dp is solved using recursion, and our function looks like this:

```cpp
// ... represents other parameters
int fun(int cur_pos, bool limit, ...) {
    int &ret = dp[cur_pos][limit][...] 
    if(ret != ...) return ret; // if we have calculated this answer before
    if(cur_pos == end) return ...; // if we reached our bound (base case)
    ret = 0; 
    int bound = limit? num[cur_pos] : 9; // what are the digits I can place in this position
                                         // The idea is that if we have already a digit smaller than our bound, then we can place any   
                                         // digit from 1-9 in our current position
    for(int digit=0; digit<=bound; digit++) {
        ret += fun(cur_pos + 1, limit && digit == num[cur_pos], ...) // add up answers
    }
    return ret;
}
```

Then, for ranged $`[a, b]`$ problems, we can just call the function twice, so it would be $`fun(b) - fun(a-1)`$.

More information can be found in this [codeforces blog](https://codeforces.com/blog/entry/53960).




