## Class 4


### Aliens trick / WQS Binary Search

This trick allows us to optimize the number of states of a dp solution.

In very few words, this optimization involves by giving a **restriction constant** to a function (the dp transition) such that we can binary search for the second state. 

For example, lets say you are given an array $`A`$ of $`N`$ positive integers, split it into exact $`K`$ segments, each segment value is the sum of all numbers in this segment `s[i]`    

You need to minimize the sum $`a \times s[i]^2`$, where $`a`$ is a random constant 

This can be done in $`O(NK)`$ by using convex hull trick & montonous deque. 

However, how can we continue to optimize this? Transition is already $`O(1)`$, this means we must optimize state.

Here, we add a restriction constant and remove the $`K`$ state. Here we rephrase the problem into minimize the sum of $`a \times s[i]^2 - C`$, where $`C`$ is our restriction constant. Then our final dp answer would be $`dp[N] + K \times C`$  (since we removed $`C`$ for $`K`$ segments we need to add it back)

Now, we notice that as $`C`$ increases, spliting into more segments will increase the total sum, and the same vice versa. This means that depending on the value $`C`$, $`K`$ also changes in relation to $`C`$, and this is also monotonous. Since its monotonic, this means we can binary search a value $`C`$ such that we have exactly $`K`$ segments.

Now our total time complexity is $`O(N \log S)`$, where $`S`$ is somewhat close to $`K`$ in value.

### Theory
This is just an example, so we would now be talking about the theory behind this trick. 

Lets say you are given a function $`f(x)`$, $`x`$ is in the range $`[1, K]`$. You know 2 things about this function:

1. $`f(x)`$ is a convex function
2. You only know the minimum value of $`f(x)`$ and the $`x`$ that gives this minimum value

Then, if we were to shift our function to the right for example. Then we have $`f(x) - p \times x`$ for $`p=0, p=1, p=2 \cdots`$. Now the interesting tthing is that the shifted function, we still have the following 2 properties of the original function. Thus we also know its minimum value and the x that gives that minimum value. Hence we just need to keep shifting our function once for a such $`p`$ that the minimum value's x-coordinate is exactly $`K`$, then we have our answer which is $`f(k) + p \times K`$.

Here, the function would be synonymous to our dp function. Since our dp gives as the minimum value and can also gives the $`p`$ that gives that function. 