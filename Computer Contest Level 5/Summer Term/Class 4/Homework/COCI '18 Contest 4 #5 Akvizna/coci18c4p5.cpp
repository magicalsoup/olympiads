#include <bits/stdc++.h>
using namespace std;
const int MM = 1e6+5;
typedef long double ld;
ld dp[MM]; int N, K, pre=0, cur=1, cnt[MM];
ld slope(int p, int q){
    return (ld)(dp[q] - dp[p])/(q-p);
}
bool check(ld cost) {
    deque<int> dq; dq.push_back(0);
    for(int i=1; i<=N; i++) {
        while(dq.size()>=2 && slope(dq[0], dq[1])>= (ld)1.0/i) dq.pop_front();
        int j = dq[0];
        dp[i] = dp[j] + (ld)(i-j)/i - cost;
        cnt[i] = cnt[j] + 1;
        while(dq.size()>= 2 && slope(dq[(int)dq.size()-2], dq.back()) <= slope(dq.back(),i)) dq.pop_back();
        dq.push_back(i);
    }
    return cnt[N] > K;
}
int main() {
    cin >> N >> K;
    ld l = 0.0, r = 1;
    for(int t=0; t<40; t++) {
        ld mid = (l + r) / 2;
        if(check(mid)) l = mid;
        else r = mid;
    }
    printf("%.9Lf\n", dp[N] + l*K);
}