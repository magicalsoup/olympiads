## Analysis

Note in all these approaches, to optimize memory, you should use a flip array.

### Naive Approach
First we approach the naive dp.

We define our dp state `dp[i][j]`, best answer for the first $`i`$ rounds if we knocked out $`j`$ people.

Then our transition is very easy: 

$`dp[i][j] = \max(dp[i][j], dp[i-1][k] + \dfrac{(j-k)}{j}) \text{ for all } k \in [0, j-1]`$

**Time Complexity:** $`O(N^2K)`$

### Optimized Approach Using CHT

We can then apply cht as we can prove that our function is monotonous:

$`f(i, j) =  \dfrac{i-j}{i}`$

$`f(i, j) + f(i+1, j+1) \ge f(i+1, j) + f(i, j+1)`$

$`\dfrac{i-j}{i} + \dfrac{i-j}{i+1} \ge \dfrac{i+1-j}{i+1} + \dfrac{i-j-1}{i}`$

$`\dfrac{i-j}{i} - \dfrac{i-j-1}{i} \ge \dfrac{i+1-j}{i+1} - \dfrac{i-j}{i+1}`$

$`\dfrac{1}{i} \ge \dfrac{1}{i+1}`$

$`i+1 \ge i`$

$`1 \ge 0`$

We can then derive our slope function:

$`dp[j] + \dfrac{i-j}{i} \le dp[k] + \dfrac{i-k}{i}`$ 

$`dp[j] - dp[k] \le \dfrac{k - j}{i}`$

$`\dfrac{(dp[j] - dp[k])}{k-j} \le \dfrac{1}{i} = slope(j, k)`$

**Time Complexity:**  $`O(NK)`$

### Optimized Approach Using WQS Binary Search / Aliens Trick

We can then use the aliens trick (explained in this weeks README) to further optimize our CHT.

**Time Complexity:** $`O(C \log N)`$, where $`C`$ is somewhat close to $`K`$ in value.
