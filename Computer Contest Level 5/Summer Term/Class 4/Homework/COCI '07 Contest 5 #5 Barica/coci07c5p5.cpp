#include <bits/stdc++.h>
using namespace std;
const int MM = 3e5+5;
struct Plant {
    int x, y, f;
};
int N, K, dp[MM], par[MM], order[MM], best_x[MM], best_y[MM]; // best answer to the ith plant
Plant plants[MM];
bool possible(Plant a, Plant b) { // if its possible to jump from plant a to plant b
    return (b.x > a.x && a.y == b.y) || (b.y > a.y && a.x == b.x);
}
bool cmp(int a, int b) {
    return plants[a].x == plants[b].x? plants[a].y < plants[b].y : plants[a].x < plants[b].x;
}
int main() {
    cin >> N >> K;
    for(int i=1; i<=N; i++) {
        cin >> plants[i].x >> plants[i].y >> plants[i].f;
        order[i] = i; // we use order so we dont have to do annoying indexing issues
    }
    memset(dp, -0x3f, sizeof dp);
    //dp[0] = -0x3f3f3f3f;
    sort(order+1, order+N+1, cmp);
    for(int i=1; i<=N; i++) {

        int cur = order[i];
        if(cur == 1) dp[cur] = plants[1].f; // if its the first plant then its default

        int &same_x = best_x[plants[cur].x]; // get the best index for the x at this coordinate
        int &same_y = best_y[plants[cur].y]; // get the best index for the y at this coordinate

        if(dp[same_x] - K >= 0 && dp[same_x] + plants[cur].f - K > dp[cur]) { //if better
            dp[cur] = dp[same_x] + plants[cur].f - K;
            par[cur] = same_x;
        }

        if(dp[same_y] - K >= 0 && dp[same_y] + plants[cur].f - K > dp[cur]) { // if better
            dp[cur] = dp[same_y] + plants[cur].f - K;
            par[cur] = same_y;
        }

        if(dp[cur] > dp[same_x]) same_x = cur; // update if its better
        if(dp[cur] > dp[same_y]) same_y = cur; // update if its better
    }

    cout << dp[N] << endl; deque<int> idx;
    for(int i=N; i!=0; i=par[i]) idx.push_front(i);
    cout << idx.size() << endl;
    for(int j : idx) cout << plants[j].x << " " << plants[j].y << " " << endl;
}