## Analysis

First try the naive solution to get a feel for the problem. Time complexity should be around $`O(N^2)`$

To optimize from the naive solution, we could do the following. 

First, we assume you have sorted by $`x`$.

Then, instead of building on previous dp values, we keep track the best dp value for each $`x`$ coordinate, and $`y`$ coordinate. This is possible as the two different types of "jumps" are independent of each other. (This can be accomplished by using a map or an array).

Then, our dp transition is very simple and becomes an $`O(1)`$ transition. 
```cpp
if(dp[pre] - K >= 0) { // if the jump is possible
    // j is just the previous best dp answer we are using to calculate i
    dp[i] = max(dp[j] + plants[i].f - K, dp[i]).
}
```

**Time Complexity:** $`O(N)`$.

