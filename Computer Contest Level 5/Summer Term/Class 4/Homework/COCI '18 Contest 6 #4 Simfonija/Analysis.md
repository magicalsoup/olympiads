## Analysis

We can apply a greedy approach to this problem. 

We greedily assume that we will instantly make $`K`$ values to be exactly the same in both $`A`$ and $`B`$. Therefore we just need to optimize the rest $`N-K`$ values. 

Now for each possible contiguous segment of length $`N-K`$, we will try to optimize the difference between the two. Consider the differnece between $`A`$ and $`B`$, we store each of those values in a new array $`C`$.

Now lets consider, if we were to add $`x`$ such that one value (lets call it $`y`$) in $`C`$ becomes $`0`$, then the left side values will be equal be "negative" (since each element was less than $`y`$) and the right side values will be "positive" following the same principle as the left side. 

This would mean that for a specific segment, if we were to take the middle element, chose it to become 0, then the total sum of the left and right side values will be minimum as possible. (Proof is left to the reader as an exercise). Note that you do have to consider the case where the length is even. 

The total difference sum for each segment is simply the right side subtract the left side (try to figure out this equation, the reason why we do not have the middle element in this equation is because it cancels itself out in the calculations).

**Time Complexity:** $`O(N \log N)`$