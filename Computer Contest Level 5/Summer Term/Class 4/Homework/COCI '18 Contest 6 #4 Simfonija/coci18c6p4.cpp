#include <bits/stdc++.h>
using namespace std;
const int MM = 1e5+5;
typedef long long ll;
int N, K, dif[MM], a[MM]; ll psa[MM];
ll range(int l, int r) {
    return r - l + 1;
}
int main() {
    cin >> N >> K;
    for(int i=1; i<=N; i++) cin >> a[i];
    for(int i=1, x; i<=N; i++) {
        cin >> x;
        dif[i] = a[i] - x;
    }
    sort(dif+1, dif+N+1);
    for(int i=1; i<=N; i++) {
        psa[i] = dif[i] + psa[i-1];
    }
    ll ans = LONG_LONG_MAX;
    int len = N-K;
    for(int l=1; l<=N-len+1; l++) {
        int r = l + len - 1;
        int mid = (l + r) / 2;
        assert(r-l+1 == len);
        ll v = dif[mid];
        ll lhs = psa[mid-1] - psa[l-1];
        ll rhs = psa[r] - psa[mid];
        ll tot = rhs - lhs;
        if(len % 2 == 0) tot -= v;
        ans = min(ans, tot);
    }
    cout << ans << endl;

}