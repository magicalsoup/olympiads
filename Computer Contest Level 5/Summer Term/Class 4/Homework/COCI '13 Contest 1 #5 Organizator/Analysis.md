## Anaylsis

First you should try the naive solution to get a feel for the problem, the expected time complexity should be $`O(N \sqrt N)`$.

For for solution, we first keep a simple frequency array of each of the numbers that appears. Then we consider each possible size of team, for $`i`$ in the range $`[1, 2,000,000]`$.

Then we can loop through each $`j`$ which is a multiple of $`i`$, and add up the frequency of each of the $`j`$ that appeared. 

If our sum is greater than $`1`$, then we want to take the maximum of our previous answer and $`sum \times i`$. 

Note that as $`i`$ grows bigger, the amount of multiples in the range $`[i, 2,000,000]`$ would rapidly decrease. This results in a time complexity much closer to $`O(N \log N)`$.

**Time Complexity:** $`O(N \log N)`$

