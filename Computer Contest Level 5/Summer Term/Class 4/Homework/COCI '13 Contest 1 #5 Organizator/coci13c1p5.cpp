#include <bits/stdc++.h>
using namespace std;
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
const int MM = 2e6;

int N, freq[MM+5];
int main() {
    boost();
    cin >> N;
    for(int i=1, x; i<=N; i++) {
        cin >> x; freq[x]++;
    }
    ll ans = 0;
    for(int i=1; i<=MM; i++) { // if i take this as the number for each team
        ll cnt = 0;
        for(int j=i; j<=MM; j+=i) cnt += freq[j];
        if(cnt > 1) ans = max(ans, 1LL*cnt*i);
    }
    cout << ans << endl;
}