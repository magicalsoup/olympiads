#include <bits/stdc++.h>
using namespace std;
#define endl '\n'
#define boost() cin.tie(0); cin.sync_with_stdio(0)
#define For(i, a, b) for(int i=a; i<b; i++)
#define FOR(i, a, b) for(int i=a; i<=b; i++)
#define Rev(i, a, b) for(int i=a; i>b; i--)
#define REV(i, a, b) for(int i=a; i>=b; i--)
#define debug(...) " [" << #__VA_ARGS__ ": " << (__VA_ARGS__) << "]"
typedef long long ll;
typedef long double ld;
typedef int64_t ll64;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, int> pli;
typedef pair<ll, ll> pll;
typedef pair<char, int> pci;
typedef pair<int, double> pid;
typedef unsigned long long ull;
const ll LLINF = 0x3f3f3f3f3f3f3f3fL;
const int INF = 0x3f3f3f3f;
const ll MOD = 1e9 + 7;
const ll PMOD = 1000000000000037;
const int MAXN = 2e5 + 5; // TODO change this for each problem
const ll BASE = 131;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
mt19937 rng;
int randint(int a, int b){ return uniform_int_distribution<int>(a, b)(rng);}
const int MM = 2e4+5;
// dp[i][k] be the maximal sum with the first i elements and k subarrays
// dp[i][k] = max(dp[i-1][k], dp[i-1][k-1] + a[i]);
// bests[i][k] be the best dp answer for the first i elements and k subarrays
int N, K, dp[2][MM], best[2][MM], psa[MM], pre=0, cur=1;
int main() {
    cin >> N >> K;
    for(int i=1; i<=N; i++) {
        cin >> psa[i];
        psa[i] += psa[i-1];
    }
    memset(dp, -0x3f, sizeof dp); memset(best, -0x3f, sizeof best);
    dp[0][0] = best[0][0] = 0;
    for(int i=1; i<=N; i++) {
        dp[cur][0] = 0;
        for(int k=1; k<=min(i, K); k++)
            dp[cur][k] = max(dp[pre][k], best[pre][k-1] + psa[i]);
        for(int k=0; k<=min(i, K); k++)
            best[cur][k] = max(best[pre][k], dp[cur][k] - psa[i]);
        cur ^= 1; pre ^= 1;
    }
    cout << dp[pre][K] << endl;
    return 0;
}