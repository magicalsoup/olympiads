## Computer Contest Level 5

## Topics
- dp optimization 1D1D dp
- advanced topics (graph, DP)
- advanced data structures (BBST, treap splay, link cut tree)
- mixed practice
- square root (Mo's algorithm)

## Summer Term
Focus on DP
- 1D1D DP convex hull trick, knuth optimization, divide & conquer
- Tree's Dp


