## Analysis


### Brute Force Approach
Since the value of $`K`$ is small we can brute-force this problem.

We first pre-process by running dijkstra's algorithm from each store and the starting node (node $`0`$). This should be done
in $`O(KN \log M)`$ time.

Then we run a permutation on the indexes of the stores, and just brute-force to find the optimal sequence of stores. Then we can easily use a precomputed distances to get the distance from one store to the next.

**Time Complexity:** $`O(KN \log M + K!K)`$


### DP Approach

We can also use bitmask dp to solve this. We still precompute, but this time we have our dp state as dp[i][mask], where mask represents which stores we have already visisted starting at store $`i`$. 

**Time Complexity:** $`O(KN \log M + 2^K \times K)`$
