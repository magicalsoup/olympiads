#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

// idea is to do dijkstra 10 times from each store, and then go through all the permutations to find the optimal sequence of stores to visit
// time complexity should be
// pre-processing is O(KN Log M)
// Calculation is O(K!K)

const int MAXN = 1e5+5;
ll dis[11][MAXN]; int N, M, K; vector<pair<int, ll>> adj[MAXN]; // node, weight
pii stores[11]; // first is the node of the store, the second is the index
void dijkstra(int store, int u) { // the store index, the starting node
    priority_queue<pair<ll, int>, vector<pair<ll, int>>, greater<pair<ll, int>>> pq;
    memset(dis[store], 0x3f, sizeof dis[store]); // fill the distance array with INF
    dis[store][u] = 0;
    pq.push({0LL, u}); // weight, node
    while(!pq.empty()) {
        ll w = pq.top().first;
        int cur = pq.top().second;
        pq.pop();
        if(w > dis[store][cur]) continue;

        for(auto e : adj[cur]) {
            int v = e.first; ll w0 = e.second;
            if(dis[store][v] > dis[store][cur] + w0) {
                dis[store][v] = dis[store][cur] + w0;
                pq.push({dis[store][v], v});
            }
        }
    }
}
int main() {
    cin >> N >> M;
    for(int i=1; i<=M; i++) {
        int u, v; ll w; cin >> u >> v >> w;
        adj[u].push_back({v, w});
        adj[v].push_back({u, w});
    }
    cin >> K;
    dijkstra(0, 0); // we treat the starting node as also a "store"
    for(int i=0; i<K; i++) {
        int u; cin >> u; int idx = i+1;
        stores[i] = {u, idx};
        dijkstra(idx, u); // run dijkstra
    }

    sort(stores, stores+K);
    ll ans = LONG_LONG_MAX;
    do {
       int pre = 0; ll cur = 0;
       for(int i=0; i<K; i++) {
           int idx = stores[i].second, v = stores[i].first;
           cur = cur + dis[pre][v];
           pre = idx;
       }
       cur = cur + dis[pre][0]; // cause we going back home
       ans = min(ans, cur);

    } while(next_permutation(stores, stores+K));
    cout << ans << endl;
}

