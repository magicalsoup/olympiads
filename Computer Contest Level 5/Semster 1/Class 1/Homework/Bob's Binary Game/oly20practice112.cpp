#include<bits/stdc++.h>

using namespace std;
typedef long long ll;

const int MM = 305;

int n, k, c[(1<<8)+1], v[(1<<8)+1]; char s[MM]; ll dp[MM][MM][(1<<8)+1], mx[2];
int main() {
    memset(dp, -0x3f, sizeof dp);
    scanf("%d %d %s", &n, &k, s+1);

    for(int i=1; i<=n; i++) dp[i][i][s[i]-'0'] = 0;
    for(int i=0; i<1<<k; i++) {
        scanf("%d %d", &c[i], &v[i]);
    }
    for(int len=1; len<n; len++) {
        for(int l=1; l+len <=n; l++) {
            int r = l + len, rem = len%(k-1); // how many bits after ALL merging is done
            if(rem == 0) rem = k-1;
            for(int m=r; m>l; m -=(k-1)) {
                for(int p=0; p<(1<<rem); p++) {
                    dp[l][r][p<<1|0] = max(dp[l][r][p<<1|0], dp[l][m-1][p] + dp[m][r][0]);
                    dp[l][r][p<<1|1] = max(dp[l][r][p<<1|1], dp[l][m-1][p] + dp[m][r][1]);
                }
            }
            if(rem == k-1) {
                mx[0] = mx[1] = -1e18;
                for(int p=0; p<(1<<k); p++) {
                    mx[c[p]] = max(mx[c[p]], dp[l][r][p] + v[p]);
                }
                dp[l][r][0] = mx[0];
                dp[l][r][1] = mx[1];
            }
        }
    }
    ll ans = -1e18;
    for(int p=0; p<(1<<k); p++) ans = max(ans, dp[1][n][p]);
    cout << ans << endl;
}