## Analysis

This problem is very hard (in my opinion).

First, we can easily deduce that this problem is an interval problem, but with an extra state; define our state as `dp[l][r][final state]`.

The final state is what the sequence $`S`$ would look like at the end of all the "merging".  Since all the values of $`v_i`$ are positive, this means that if we could merge something, we should always keep merging. Therefore the final sequence must be a string with a length from $`[1, 2^{k-1}]`$. 

So for our dp, are transition is 2 cases:

```cpp
dp[l][r][p<<1|0] = max(dp[l][r][p<<1|0], dp[l][m-1][p] + dp[m][r][0]);
dp[l][r][p<<1|1] = max(dp[l][r][p<<1|1], dp[l][m-1][p] + dp[m][r][1]);
```

The first transition is if we make the sequence from $`[m, r]`$ and turn it into 0 (in the final sequence), and the second transition is if we make the same sequence but turn it into 1. 

This runs in $`O(N^3 \times 2^{k})`$.

To speed it up, we have to look at our transition.

Before, we had to loop through all the midpoints so it added a $`O(n)`$ complexity to our overall complexity. However, we realize we do not have to loop through all the midpoints. We can only make a sequence of length $`t`$ into a single bit by using a rule if only $`t`$ is exactly length $`k`$. So the only values of $`m`$ that do this is given by the equation $`c(k-1) + 1`$, where $`c`$ is an integer. 

To show you, we give you an example:

$`k = 3, N = 7`$

At the end, we can make 2 bits.

first we can "compress" 2 sequences of length $`k`$. So we have $`1 + 1 + 7 \bmod 3 = 4`$ bits left. Then we can compress one more sequence so we get $`1 + 4 \bmod 3 = 2`$.

If you use variables, you will get the equation above through derivation.

Then our midpoint finding transition is $`O(\frac{N}{K})`$ instead. Giving our final complexity of:

**Time Complexity:** $`O(N^2 \times 2^{k} \times \frac{N}{K})`$

