#include <bits/stdc++.h>
using namespace std;

// idea: sort the diagonals by distance between the vertices
// then we check and update along the way
// we first have an array nxt[i] that points to the next vertex from vertex i
// then we check the 3 sides of the triangle, current vertex (a), middle vertex (b = nxt[a]), end vertex (c)
// if b does not point to c, that means the diagonals intersected and we output "incorrect triangulation"
// if the 3 sides of the triangle do not match, we output "incorrect colouring"
// after each diagonal, also we update our pointer array and our side array (cause we updated the pointer)

typedef pair<int, int> pii;
const int MAXN = 2e5+5;
struct Diagonal {
    int u, v, c; // from (vertex), to (vertex) color
};
int N, sides[MAXN]; string s; // the color of the sides of the triangle
int nxt[MAXN];
vector<Diagonal> dia;

bool cmp(Diagonal a, Diagonal b) { // sort by distance between the vertices of the diagonal
    return (a.v - a.u + N)%N < (b.v - b.u + N)%N;
}
int main() {
    int t; cin >> t; // subtask number
    cin >> N;
    cin >> s;

    for(int i=1; i<s.size(); i++) { // convert to integers and store it in an array
        sides[i] = s[i-1] - '0';
        nxt[i] = i+1;
    }
    nxt[N] = 1; sides[N] = s[N-1] - '0';

    for(int i=1; i<=N-3; i++) {
        int u, v, c; cin >> u >> v >> c;
        dia.push_back({u, v, c});
        dia.push_back({v, u, c});

    }
    sort(dia.begin(), dia.end(), cmp);

    for(int i=0; i<N-2; i++) {
        int a = dia[i].u, b = nxt[a], c = dia[i].v;
        if(nxt[b] != c) {
            cout << "neispravna triangulacija" << endl;
            return 0;
        }
        int va = sides[a], vb = sides[b], vc = dia[i].c;
        if(!(va != vb && vb != vc && vc != va)) {
            cout << "neispravno bojenje" << endl;
            return 0;
        }
        nxt[a] = c; sides[a] = vc;
    }
    cout << "tocno" << endl;
    return 0;
}