## Analysis

we sort the diagonals by distance between the vertices, then we check and update along the way.

The sorting function is given like this, we assume $`u`$ to be the starting vertex of a diagonal, and $`v`$ to be the ending one.

```cpp
bool cmp(Diagonal a, Diagonal b) { // sort by distance between the vertices of the diagonal
    return (a.v - a.u + N)%N < (b.v - b.u + N)%N;
}
```

We first have an array `nxt[i]` that points to the next vertex from vertex $`i`$. Then we check the 3 sides of the triangle, current vertex ($`a`$), middle vertex ($`b`$ = `nxt[a]`), end vertex ($`c`$).

If $`b`$ does not point to $`c`$, that means the diagonals intersected and we output "incorrect triangulation"

If the 3 sides of the triangle do not match, we output "incorrect colouring". After each diagonal, also we update our pointer array and our side array (cause we updated the pointer).

**Time Complexity:** $`O(N \log N)`$
