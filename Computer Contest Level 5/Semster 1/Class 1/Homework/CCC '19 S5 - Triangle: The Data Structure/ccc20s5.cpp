#include <bits/stdc++.h>
using namespace std;
#define INF INT_MAX
#define NINF -INF
#define mp make_pair
#define endl '\n'
#define boost() cin.tie(0); cin.sync_with_stdio(0)
#define For(i, a, b) for(int i=a; i<b; i++)
#define FOR(i, a, b) for(int i=a; i<=b; i++)
#define Rev(i, a, b) for(int i=a; i>b; i--)
#define REV(i, a, b) for(int i=a; i>=b; i--)
#define debug(...) " [" << #__VA_ARGS__ ": " << (__VA_ARGS__) << "]"
typedef long long ll;
typedef long double ld;
typedef int64_t ll64;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
typedef pair<char, int> pci;
typedef unsigned long long ull;
const ull MOD = 1e9 + 7;
const int MAXN = 1e5 + 5;
const ull BASE = 131;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
mt19937 rng;
int N, K, BIT[3005][3005], a[3005][3005];
void upd(int r, int c, int x) {
    for(int i=r; i<3005; i+=i&-i)
        for(int j=c; j<3005; j+=j&-j)
            BIT[i][j] = max(BIT[i][j], x);
}
int qry(int r, int c) {
    int mx=0;
    for(int i=r; i>0; i-=i&-i)
        for(int j=c; j>0; j-=j&-j)
            mx = max(mx, BIT[i][j]);
    return mx;
}
int main() {
    boost();
    cin >> N >> K; ll ans = 0;
    for(int i=1; i<=N; i++) {
        for(int j=N-i+1; j<=N; j++) {
            cin >> a[i][j];
        }
    }
    int cnt = N - K + 1;
    for(int i=N; i>=N-K; i--) {
        for(int j=N; j>=cnt; j--) {
            upd(i, j, a[i][j]);
        }
        cnt++;
    }
    for(int i=N-K; i>=1; i--) {
        int row = i, col = N;
        while(row <= N - K) {
            ans += qry(row + K, col);
            row++; col--;
        }
        row = i; col = N;
        while(row <= N) {
            upd(row, col, a[row][col]);
            row++; col--;
        }
    }
    int row = 1, col = N;
    while(row <= N - K + 1) {
        ans += qry(row+K-1, col);
        row++; col--;
    }
    cout << ans << endl;
}