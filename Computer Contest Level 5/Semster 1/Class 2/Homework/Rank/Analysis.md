## Analysis

Pretty straight forward problem. 

First build the directed graph by making a connection from $`a`$ to $`b`$ if team $`a`$ beats team $`b`$.

Then simply run depth first search from every node and check to see if the starting node forms a cycle. If it does increase your counter by 1.

**Time Complexity:** $`O(N(N + K))`$