#include<bits/stdc++.h>
using namespace std;
int vis[25], par[25], N, K;
bool nodes[25]; // nodes that ares sus
vector<int> adj[25];
void dfs(int u, int p) {
    if(vis[u] == 2) return;
    if(vis[u] == 1) {
        int cur = p; nodes[u] = true;
        while(cur != u) {
            nodes[cur] = true;
            cur = par[cur];
        }
        return;
    }
    vis[u] = 1; par[u] = p;
    for(int v : adj[u]) {
        if(vis[v] == p) continue;
        dfs(v, u);
    }
    vis[u] = 2;
}
int main() {
    cin >> N >> K;
    for(int i=0; i<K; i++) {
        int a, b, va, vb; cin >> a >> b >> va >> vb;
        if(va > vb) adj[a].push_back(b);
        else adj[b].push_back(a);
    }
    for(int i=1; i<=N; i++) {
        memset(vis, 0, sizeof vis);
        memset(par, 0, sizeof par);
        dfs(i, i);
    }
    int cnt =0 ;
    for(int i=1; i<=N; i++) {
        cnt += nodes[i];
    }
    cout << cnt << endl;
}