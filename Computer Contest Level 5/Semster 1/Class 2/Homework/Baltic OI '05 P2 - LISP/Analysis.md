## Analysis

This problem isnt really that hard. First be sure to read the input carefully, the input spans several lines (for larger values of $`N`$), you cannont just read in one line of strings.

Next, for a bracket sequence (with only one type) to be balanced, it has to have the following:
- First we let each `(` be equal to $`1`$, and each `)` to be equal to $`-1`$. 
- Then the sum of the brackets at any point must be $`\ge 0`$
- And the sum of the entire sequence must be equal to $`0`$

With this in mind, for each `]` except the last one, we treat at as `)`, and we will just let the last one balance the entire sequence. 

**Note:** Fast I/O is recommened due to the sheer size of input.

**Time Complexity:** $`O(N)`$