#include <bits/stdc++.h>
using namespace std;
// the idea is that for a bracket sequence to be balanced (of only one type), we must consider the following:
// let each ( be 1 and each ) be -1, then at any point of the sequence, the sum of these brackets must never be below 0, and the sum of the entire sequence
// must be equal to 0
// we can just be greediy and let the lst bracket carry the weight of all the others
const int MM = 1e7+5;
int N, M, suf[MM], ssa[MM]; char s[MM]; vector<int> ans;
int main() {
    scanf("%d %d", &N, &M); char c;
    for(int i=1; i<=N; i++){
        scanf(" %c", &c);
        s[i] = c;
    }
    int lst = -1, ed = 0; // the last index of ]
    for(int i=N; i>=0; i--) {
        if(s[i] == ']') {
            lst = i;
            break;
        }
        if(s[i] == ')') ed++;
        if(s[i] == '(') ed--;
    }
    int cur_sum = 0, ans = 0;
    for(int i=1; i<=N; i++) {
        if(s[i] == ')') cur_sum--;
        if(s[i] == '(') cur_sum++;
        if(cur_sum < 0) {
            printf("0\n");
            return 0;
        }
        if(s[i] == ']') {
            if(i != lst) {
                cur_sum--;
                if(cur_sum < 0) {
                    printf("0\n");
                    return 0;
                }
            }
            else {
                ans = cur_sum - ed;
                if(ans < 0) {
                    printf("0\n");
                    return 0;
                }
            }
        }
    }
    printf("1\n");
    for(int i=1; i<M; i++) printf("1\n");
    cout << ans << endl;
}