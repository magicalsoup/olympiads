#include <bits/stdc++.h>
using namespace std;
const int MM = 1e6+5;
typedef long long ll;
struct Edge {int v, nxt; } edge[2*MM];
int N, M, cnt, bcc, dfn[MM], low[MM], head[MM], sz[MM]; bool in[MM]; stack<int> st;
void add_edge(int u, int v) {
    edge[cnt].v = v; edge[cnt].nxt = head[u]; head[u] = cnt++;
}
void tarjan(int u, int p) { // cur node, parent
    dfn[u] = low[u] = ++cnt;
    st.push(u); in[u] = true;
    for(int i=head[u]; i!=-1; i=edge[i].nxt) {
        int v = edge[i].v;
        if(v == p) continue;
        if(dfn[v] == 0) {
            tarjan(v, u);
            low[u] = min(low[u], low[v]);
        }
        else if(in[u]){ // if in stack
            low[u] = min(low[u], dfn[v]);
        }
    }
    if(low[u] == dfn[u]) { // if it is root node
        int bcc_sz = 0, x = 0;
        do {
            x = st.top(); st.pop(); bcc_sz++; in[x] = false;
        } while(x != u);
        sz[++bcc] = bcc_sz;
    }
}
int main() {
    cin >> N >> M;
    memset(head, -1, sizeof head);
    for(int i=1, u, v; i<=M; i++) {
        cin >> u >> v;
        add_edge(u, v);
        add_edge(v, u);
    }
    cnt = 0; tarjan(1, -1);
    ll ans = 0;
    for(int i=1; i<=bcc; i++) ans = ans + (ll)(sz[i])*(N-sz[i]);
    printf("%.5f\n", ans/(double)((ll)N*(N-1)));
}