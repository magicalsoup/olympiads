## Analysis

In this problem are are finding bridges in a graph. First we compress the graff into its bi-connected components using tarjan's algorithm. 

The number of pairs of towns that can be disconnected by a single road is simply the sum of the number of pairs of nodes in different bi-connected components. To calculate that, it is equal to the size of the current bi-connected component multiplied by the size of the other bi-connected componenets. But since each pair can be counted twice, we have to divide this number by 2.

Lastly, for the total number of pairs of towns is simply $`\frac{N \times (N-1)}{2}`$. 

However, for this problem we might encounter a memory issue using tradtional adjacency list. Therefore we will implement a chinese adjacency list. It is implemented as the following:

```cpp
// initially the head[] array is filled with -1s
// ind is initially 0 and keeps track of the "edge number"
// The edge array should have twice the size of the number of edges if it is bidirectional
void add_edge(int u, int v) { // this makes a directional edge from u to v
    edge[cnt].v = v; edge[cnt].nxt = head[u]; head[u] = ind++; 
}

void traverse() {
    for(int i=head[u]; i!=-1; i=edge[i].nxt) {
        // do something
    }
}
```

**Time Complexity:** $`O(N + M)`$