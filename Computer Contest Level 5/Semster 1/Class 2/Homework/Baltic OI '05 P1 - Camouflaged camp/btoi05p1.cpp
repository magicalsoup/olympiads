#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> pii;
#define endl '\n'
#define boost() cin.tie(0); cin.sync_with_stdio(0)

const int MM = 1005;
int R, C, L, W, H, psa[MM][MM], ret[MM][MM];

int get_sum(int r1, int c1, int r2, int c2) {
    return psa[r2][c2] - psa[r1-1][c2] - psa[r2][c1-1] + psa[r1-1][c1-1];
}
bool check(int &r, int &c, int &l, int &w, int &flag, int &cmp) { // location, size, arrangement flag, altitude flag
    int sumA = get_sum(r, c, r + l - 1, c + w - 1);
    int r_b = r + (flag == 1)*l, c_b = c + (flag == 0)*w;
    int sumB = get_sum(r_b, c_b, r_b + l - 1, c_b + w - 1);
    // cout << r << " " << c << " " << r_b << " " << c_b << " " << sumA << " " << sumB << endl;
    if(cmp == 0) return sumA < sumB;
    return sumA >= sumB;
}
int main() {
    boost();
    cin >> R >> C;
    for(int i=1; i<=R; i++) {
        for(int j=1; j<=C; j++) {
            cin >> psa[i][j];
            psa[i][j] = psa[i][j] + psa[i-1][j] + psa[i][j-1] - psa[i-1][j-1];
        }
    }
    cin >> L >> W;
    cin >> H;

    for(int t=0; t<H; t++) {
        int r, c, l, w, flag, cmp;
        cin >> r >> c >> l >> w >> flag >> cmp;
        int dr = flag == 1? l : 0, dc = flag == 0? w : 0;
        for(int i=r; i+dr+l-1<=R; i++) {
            for(int j=c; j+dc+w-1<=C; j++) {
                if(check(i, j, l, w, flag, cmp)) ret[i-r+1][j-c+1]++;
            }
        }
    }

    // sanity checks

    // cout << get_sum(3, 2, 3, 4) << endl; // psa is correct


    // check(3, 1);
    int mx = 0; pii ans = {1, 1};
    for(int i=1; i<=R; i++) {
        for(int j=1; j<=C; j++) {
            //cout << ret[i][j] << " ";
            if(ret[i][j] > mx) {
                mx = ret[i][j];
                ans = {i, j};
            }
        }
        //cout << endl;
    }
    cout << ans.first << " " << ans.second << endl;
    return 0;
}