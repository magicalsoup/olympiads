## Analysis

We can brute force this problem. First we can construct a 2D prefix sum array to get the sum of an rectangle in $`O(1)`$ time.

Then we can loop through all the **possible** starting locations of the camp, and if it satisfies one characteristic, we increment the value of that location on the grid by $`1`$.

Key word here is possible, we do not necessarily have to loop through all the values from $`1`$ to $`R`$ and $`1`$ to $`C`$. Since it is impossible to construct 2 rectangle at the very bottom left corner of the grid and there are many other index values where it is impossible to construct the rectangles. The range for possible rectangles is up to the reader to implement as an exercise.

**Time Complexity:** $`O(RC H \div K)`$, where $`K`$ is a fairly small constant.

**Note:** We assume the data is probably generated randomly, so $`K`$ would reduce our time complexity significantly. (The larger the rectangle(s) in the characteristics, the faster our program is)