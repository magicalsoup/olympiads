#include <bits/stdc++.h>
using namespace std;
#define INF INT_MAX
#define NINF -INF
#define endl '\n'
#define boost() cin.tie(0); cin.sync_with_stdio(0)
#define For(i, a, b) for(int i=a; i<b; i++)
#define FOR(i, a, b) for(int i=a; i<=b; i++)
#define Rev(i, a, b) for(int i=a; i>b; i--)
#define REV(i, a, b) for(int i=a; i>=b; i--)
#define debug(...) " [" << #__VA_ARGS__ ": " << (__VA_ARGS__) << "]"
typedef long long ll;
typedef long double ld;
typedef int64_t ll64;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, int> pli;
typedef pair<ll, ll> pll;
typedef pair<char, int> pci;
typedef pair<int, double> pid;
typedef unsigned long long ull;
const ll MOD = 1e9 + 7;
const ll PMOD = 1000000000000037;
const int MAXN = 2e5 + 5; // TODO change this for each problem
const ll BASE = 131;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
inline void add_self(ll &a, ll b) { a += b; if(a >= MOD) a -= MOD;}
inline void sub_self(ll &a, ll b) { a -= b; if(a < 0) a += MOD;}
mt19937 rng;
int randint(int a, int b){ return uniform_int_distribution<int>(a, b)(rng);}
const int MM = 1005;
int n, m, k; ll dp[MM];
// dp[i][k] best answer for first i items with k dollars
// use knapsack trick
int p[MM], s[MM], q[MM], t[MM], d[MM], a[MM];
int main(){
    cin >> n >> m >> k;
    for(int i=1; i<=n; i++) cin >> p[i] >> s[i];
    for(int i=1; i<=m; i++) cin >> q[i] >> t[i] >> d[i] >> a[i];

    for(int i = 1; i <= m; i++) {
        ll v = 1LL * s[t[i]] * q[i];
        int limit = min(a[i], k / d[i]); // how many deals i can have with k money
        for(int amn = 1; limit > 0; amn *=2) {
            amn = min(amn, limit);
            //if(p[t[i]] * q[i] * amn < d[i]*amn) // if withouth using the deal is less than using the deal
             //   continue;
            limit -= amn;
            for (int j = k; j >= d[i]*amn; j--) { // k dollars, to the deal money multiply by the times i use this deal
                //ll v = 1LL * s[t[i]] * q[i] * amn; // the satisfaction value of the product, multiply by the deal (forces you to buy q[i] of that item) multiply by the amount of times i used this deal
                dp[j] = max(dp[j], dp[j - d[i]*amn] + v * amn);
            }
        }
    }
    for(int i = 1; i <= n; i++) {
        for(int j = p[i] ; j <= k; j++) {
            dp[j] = max(dp[j], dp[j-p[i]] + s[i]);
        }
    }
    cout << dp[k] << endl;
}