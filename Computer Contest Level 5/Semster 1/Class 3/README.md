## Class 3

### Strongly Connected Components (SCC) & Tarjan's Algorithm

Definition: in a strongly connected component, all the nodes has a path that returns to them. (i.e forms a cycle)

### Tarjan's Algorithm
- determining whether a node is the root of a SCC
    - Root node is the first node of an SCC visited first when you DFS (depth first search)
    - Technically any node of the SCC can be the root node (depending on which you visit first in your dfs)
    - To find the root: 
        - we first assign a dfn (depth first number --> order number that they are discovered/visited)
        - we also assign a lowlink value. If a node can reach another node with a lower dfn value, then our lowlink value is that nodes dfn value.
- After compressing, the graph becomes a DAG (direct acyclic graph)
    - Compressing is to make a SCC into one node


#### Bi-Conected Component (BCC)

Definition: From node A to node B you have 2 simple paths that do not share any common edges.

#### Bridge
Definition: If we remove this **edge**, then the graph is no longer connected. Similarly, if $`u \rightarrow v`$ is an bridge, then if we break it, $`u`$ has no path that can lead to $`v`$ (and vice versa)

How To check: If child $`v`$'s lowlink value is greater than the parent $`u`$'s dfn number, then $`u \rightarrow v`$ is a bridge

#### Articulation Point

Definition: If we remove this **node**, the graph is no longer connected. 

How to check: If a node is part of a bridge and is not the root node/starting node, then it is an articulation point.

### Tarjan's Algorithm

```cpp
// this works for both directed and undirected graphs
int cnt = 0; 
void tarjan(int u, int p) { // cur node, parent
    dfn[u] = low[u] = ++cnt;
    st.push(u); in[u] = true;
    for(int v : adj[u]) {
        if(v == p) continue;
        if(dfn[v] == 0) {
            tarjan(v, u);
            low[u] = min(low[u], low[v]);
        }
        else if(in[u]){ // if in stack
            low[u] = min(low[u], dfn[v]);
        }
    }
    if(low[u] == dfn[u]) { // if it is root node
        
        // this is where you do stuff to the connected component
        int x = 0;
        do {
            x = st.top(); st.pop(); in[x] = false;
        } while(x != u);
    }
}
```


### Chinese Adjacency List

```cpp
// initially the head[] array is filled with -1s
// ind is initially 0 and keeps track of the "edge number"
// The edge array should have twice the size of the number of edges if it is bidirectional
void add_edge(int u, int v) { // this makes a directional edge from u to v
    edge[cnt].v = v; edge[cnt].nxt = head[u]; head[u] = ind++; 
}

void traverse() {
    for(int i=head[u]; i!=-1; i=edge[i].nxt) {
        // do something
    }
}
```